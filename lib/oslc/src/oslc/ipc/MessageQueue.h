/*
 * MessageQueue.h
 *
 *  Created on: Jun 28, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_IPC_MESSAGEQUEUE_H_
#define SRC_OSLC_IPC_MESSAGEQUEUE_H_

#include <queue>
#include <map>
#include <memory>

#include <mutex>
#include <thread>

#include <oslc/lul/Log.h>
#include <oslc/lul/Time.h>

#include <oslc/ipc/IOBase.h>
#include <oslc/lul/Serializer.h>


#include <oslc/lul/PriorityQueue.h>

namespace oslc {
namespace ipc {

typedef oslc::lul::MilliSec TimeMS;

template <class T>
struct Message
{
	// message types
	enum Type
	{
		None = 0x0,
		Data = 0x1,
		Ctrl = 0x2,
		Abort= 0x4,
		PingReq = 0x8,
		PingReply = 0x10
	};

	static const int InvalidVal = -1;
	static const int AnyAddress = -2;

	Message(Type type = None):
		src(InvalidVal), dest(InvalidVal), id(InvalidVal), flags(type)
	{}
	Message(const T &data):
	  src(InvalidVal), dest(InvalidVal), id(InvalidVal), flags(Data), data(data)
	{}

	//
	// Serialize data function
	//
	template<class Serializer>
	void serialize(Serializer &ser, const unsigned int version)
	{
		// first read/write flags
		ser & flags;
	    ser & src & dest & id;
		//& time;
	    //if(ser.isStoring())
	    ser & time;
		// if we should read data
		if(flags & Data)
		{
		  ser & data;
		}
	}

	void setData(const T &data)
	{
		flags |= Data;
		this->data = data;
	}
	const T &getData() { return data; }
	T &editData()
	{
		flags |= Data;
		return data;
	}

	// data
	int src;
	int dest;
	int id;
	unsigned int flags;
	TimeMS time;
protected:
	T data;
};

// if this is used data will be queued for delivery by background thread instead of sent immediately
//#define QUEUE_OUT_DATA

// TODO: refactor to MessageQueue client and MessageQueue server

template <class T, template<class U> class IO >
class MessageQueue
{
public:
	typedef Message<T> MessageType;
	typedef IO<MessageType> IOType;

	static_assert(
		        std::is_base_of< IOBase< MessageType >, IO< MessageType > >::value,
		        "IOType must be a descendant of IOBase"
		    );

	struct Client
	{
		typedef std::shared_ptr<Client> Ptr;
		enum State { Available, RequestPending };

		// data
		int id;
		TimeMS rttTime;
		TimeMS totalWorkTime;
		unsigned int completedCount;
		unsigned int pendingReqCount;
		State state;
		oslc::lul::TimePointHiRes timeStamp;
	};

	typedef typename Client::Ptr ClientPtr;

	typedef std::map<int, ClientPtr> ClientList;

	// sort queue according to priority -> amount of pending requests
	static bool queueCompare(const ClientPtr &left, const ClientPtr &right)
	{
		return left->pendingReqCount > right->pendingReqCount;
	}

	typedef oslc::lul::PriorityQueue<ClientPtr,bool (*)(const ClientPtr &left, const ClientPtr &right)> ClientPriorityList;

	MessageQueue(unsigned int buffer_size, bool isServer) :
		m_mio(buffer_size), m_active(false), m_isServer(isServer), m_totalPending(0), m_totalSent(0), m_clientPriorityList(0,queueCompare)
	{

	}

	~MessageQueue()
	{
      if(m_active)
    	  stop();
	}

	size_t queueInSize()
	{
		std::lock_guard<std::mutex> autoGuard(m_lockIn);

		return m_dataIn.size();
	}

	size_t queueOutSize()
	{
		std::lock_guard<std::mutex> autoGuard(m_lockIn);

#ifdef QUEUE_OUT_DATA
		return m_dataOut.size();
#else
		return 0;
#endif
	}

	void enqueueIn(MessageType &msg)
	{
		std::lock_guard<std::mutex> autoGuard(m_lockIn);
		m_dataIn.emplace(msg);
	}

	void dequeueIn(MessageType &msg)
	{
		std::lock_guard<std::mutex> autoGuard(m_lockIn);
		msg = m_dataIn.front();
		m_dataIn.pop();
	}

	void enqueueOut(MessageType &msg)
	{
		std::lock_guard<std::mutex> autoGuard(m_lockIn);

		if(m_isServer)
		{
		  // check if dest is any
		  handleMessageRouting(msg);
		  // update client list
		  updateClientList(msg,true);
		}
#ifdef QUEUE_OUT_DATA
		m_dataOut.emplace(msg);
#else
		m_mio.tryWriteData(msg);
#endif
	}

	bool tryEnqueueOut(MessageType &msg, TimeMS maxWait=0)
	{
		// wait for queue to be ready to send
		bool ready = shouldEnqueue();
		while(!ready && maxWait>0)
		{
			maxWait -= oslc::lul::WallTime::sleepMilli(1);
			ready = shouldEnqueue();
		}
		// if ready..
		if(ready)
		  enqueueOut(msg);
		// return success or failure
		return ready;
	}

	unsigned int pendingMessages()
	{
		return m_totalPending;
	}

	unsigned int totalMessagesSent()
	{
		return m_totalSent;
	}

	bool shouldEnqueue(unsigned int maxPending = 0)
	{
		unsigned int PendingLimit = (maxPending) ? maxPending : MaxPendingPerClient * m_clients.size();
		return m_totalPending < PendingLimit;
	}

	void handleMessageRouting(MessageType &msg)
	{
		if(msg.dest==MessageType::AnyAddress)
		{
			msg.dest = m_clientPriorityList.front()->id;
		}
	}

	void updateClientList(const MessageType &msg, bool isReq)
	{
		if(isReq)
		{
		  // if request lookup destination
		  ClientPtr clientPtr = m_clients[msg.dest];
		  clientPtr->state = Client::RequestPending;
		  clientPtr->pendingReqCount++;
		  clientPtr->timeStamp = oslc::lul::WallTime::now();
		  m_totalPending++;
		  m_totalSent++;
		}
		else
		{
		  // if reply lookup source
		  ClientPtr clientPtr = m_clients[msg.src];
		  clientPtr->rttTime = oslc::lul::WallTime::diffMilli(clientPtr->timeStamp);
		  clientPtr->totalWorkTime += clientPtr->rttTime;
		  clientPtr->state = Client::Available;
		  clientPtr->pendingReqCount--;
		  clientPtr->completedCount++;
		  m_totalPending--;
		}
		m_clientPriorityList.update();

	}

#ifdef QUEUE_OUT_DATA
	void dequeueOut(MessageType &msg)
	{
		std::lock_guard<std::mutex> autoGuard(m_lockOut);
		//MessageType msg = m_dataOut.front();
		msg = m_dataOut.front();
		m_dataOut.pop();
		//return msg.data;
	}
#endif

	void start()
	{
	  // call derived to help build client list
	  if(m_isServer)
	  {
	    onPopulateClientList();
	    for(auto pair: m_clients)
	    	m_clientPriorityList.enqueue(pair.second);
	  }
	  // set active flag
	  m_active = true;
	  m_thread = std::thread(&MessageQueue::run,this);
	}

	void stop()
	{
		m_active = false;
		if(m_thread.joinable() &&  (m_thread.get_id() != std::this_thread::get_id()))
			m_thread.join();
	}

	bool isActive()
	{
		return m_active;
	}

	void sendPing(typename MessageType::Type type)
	{
		MessageType msg(type);
		enqueueOut(msg);
	}

	void sendAbort(int id = -1)//bool all=true)
	{
		MessageType msg(MessageType::Abort);

		if(id<0)
		{
		  for(auto pair: this->m_clients)
		  {
			  msg.dest = pair.second->id;
			  enqueueOut(msg);
		  }
		}
	}
	//
	// manage queue in/out
	//
	void run()
	{
		LOG_INFO("Thread begin: "<<std::this_thread::get_id());
        while(m_active)
        {
    		//TODO: consider send policy - instead of sending all first, send x and then try receive ?
        	// while we have messages to send out
        	//while(m_dataIn.size())
        	// send them out
#ifdef QUEUE_OUT_DATA
        	while(queueOutSize())
        	{
        		//MessageType &dataOut = dequeueOut();
        		MessageType dataOut;
        		dequeueOut(dataOut);
        		m_mio.tryWriteData(dataOut);
        	}
#endif
        	// collect answers
        	MessageType dataIn;
        	while(m_mio.tryReadData(dataIn))
        	{
        		// update stats
        	    if(m_isServer)
        	      updateClientList(dataIn,false);

				handleMessage(dataIn);

        	}
        	// yield rest of time slot
        	//std::this_thread::yield();
        	// TODO: make this a configuration parameter or set to blocking wait on IO read?
#define QREADER_SLEEP_TIME_MILLI 10
  	        oslc::lul::WallTime::sleepMilli(QREADER_SLEEP_TIME_MILLI);
        }
		LOG_INFO("Thread end: "<<std::this_thread::get_id());
	}

	void handleMessage(MessageType& msg)
	{
		switch (msg.flags) {
		case MessageType::Data:
			enqueueIn(msg);
			break;
		case MessageType::Ctrl:
			if (onCtrlMessage(msg))
				// onCtrl returns true for further user msg processing
				enqueueIn(msg);
			break;
		case MessageType::PingReq:
			sendPing(MessageType::PingReply);
			break;
		case MessageType::Abort:
			stop();
			break;
		}
	}

	virtual void onPopulateClientList() = 0;
	// onCtrl returns true for further user msg processing
    virtual bool onCtrlMessage(const MessageType &msg) { return true; }
    //virtual void onBroadcastMessage(const MessageType &msg) = 0;

protected:
#ifdef QUEUE_OUT_DATA
	std::queue<MessageType> m_dataOut;
#endif
	std::queue<MessageType> m_dataIn;
	//MessageIO<MessageType> m_mio;
	std::thread m_thread;
	IOType m_mio;
	bool m_active;
	bool m_isServer;
	const unsigned int MaxPendingPerClient = 2;
	unsigned int  m_totalPending;
	unsigned int  m_totalSent;
	std::mutex m_lockIn;
	//std::mutex m_lockOut;
	ClientList m_clients;
	ClientPriorityList m_clientPriorityList;
};

} /* namespace ipc */
} /* namespace oslc */

#endif /* SRC_OSLC_IPC_MESSAGEQUEUE_H_ */
