/*
 * IOBase.h
 *
 *  Created on: Jun 29, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_IPC_IOBASE_H_
#define SRC_OSLC_IPC_IOBASE_H_

namespace oslc {
namespace ipc {

template <class T>
class IOBase
{
public:
	bool tryReadData(T &de)
	{
		return false;
	}

	bool tryWriteData(T &de)
	{
		return false;
	}
};

} /* namespace ipc */
} /* namespace oslc */

#endif /* SRC_OSLC_IPC_IOBASE_H_ */
