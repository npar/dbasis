/*
 * MPI.h
 *
 *  Created on: Jun 5, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_MPI_MPI_H_
#define SRC_OSLC_MPI_MPI_H_

#include<mpi.h>

#include <stdexcept>

#include <string>

#include <oslc/lul/Log.h>

namespace oslc {
namespace mpi {

class exception: public std::runtime_error
{
public:
	exception(std::string msg, int errorCode): std::runtime_error(msg + "=" + std::to_string(errorCode)) {}
};

#define MPI_TRY_EXEC_FUNC(func)\
{\
int res = func;\
if(res!=MPI_SUCCESS) \
	throw exception(#func,res);\
}

#define MPI_TRY_EXEC_FUNC_NO_THROW(func)\
{\
int res = func;\
if(res!=MPI_SUCCESS) \
   LOG_ERROR(#func<<"="<<res);\
}

class MPI
{
public:
	class Comm
	{
	public:
		Comm(MPI_Comm comm = MPI_COMM_WORLD): m_comm(comm)
	    {

	    }
		int size()
		{
			int numtasks;
			// get number of tasks
			MPI_TRY_EXEC_FUNC(MPI_Comm_size(m_comm,&numtasks));
			return numtasks;
		}
		int rank()
		{
			// get my rank
			int rank;
			MPI_TRY_EXEC_FUNC(MPI_Comm_rank(m_comm,&rank));
			return rank;
		}
		void send(const void *buf, int count, MPI_Datatype datatype, int dest,int tag)
		{
			MPI_TRY_EXEC_FUNC(MPI_Send(buf,count,datatype,dest,tag,m_comm));
		}

		void iSend(const void *buf, int count, MPI_Datatype datatype, int dest,int tag, MPI_Request *request)
		{
			MPI_TRY_EXEC_FUNC(MPI_Isend(buf,count,datatype,dest,tag,m_comm,request));
		}

		void receive(void *buf, int count, MPI_Datatype datatype, int source = MPI_ANY_SOURCE, int tag = MPI_ANY_TAG, MPI_Status *status = MPI_STATUS_IGNORE )
		{
			MPI_TRY_EXEC_FUNC(MPI_Recv(buf,count,datatype,source,tag,m_comm,status));
		}

		void iReceive(void *buf, int count, MPI_Datatype datatype, MPI_Request *request, int source = MPI_ANY_SOURCE, int tag = MPI_ANY_TAG)
		{
			MPI_TRY_EXEC_FUNC(MPI_Irecv(buf,count,datatype,source,tag,m_comm,request));
		}

		bool iProbe(int source = MPI_ANY_SOURCE, int tag = MPI_ANY_TAG, MPI_Status *status = MPI_STATUS_IGNORE)
		{
			int flag; // message is available?
			MPI_TRY_EXEC_FUNC(MPI_Iprobe(source,tag,m_comm,&flag,status));
			return flag==1;
		}

		void abort(int errorcode)
		{
			MPI_TRY_EXEC_FUNC(MPI_Abort(m_comm,errorcode));
		}

		void getCount(MPI_Status *status, MPI_Datatype datatype, int *count)
		{
			MPI_TRY_EXEC_FUNC(MPI_Get_count(status, datatype, count));
		}

	private:
	    // data
		MPI_Comm m_comm;
	};

	enum ThreadSupport { Single = MPI_THREAD_SINGLE, Funneled = MPI_THREAD_FUNNELED, Serialized = MPI_THREAD_SERIALIZED, Multiple = MPI_THREAD_MULTIPLE };

	MPI()
    {
		//init(0,0);
    }
	MPI(ThreadSupport ts, int argc=0, char **argv=0)
    {
		if(argc>0)
			initThread(&argc,&argv,ts);
		else
			initThread(0,0,ts);
    }
	~MPI()
	{
		// avoid calling an exception in destructor
		MPI_TRY_EXEC_FUNC_NO_THROW(MPI_Finalize());
	}
	// interface
	int initThread(int *argc, char ***argv,int required)
	{
		int provided;
		MPI_TRY_EXEC_FUNC(MPI_Init_thread(argc,argv,required,&provided));
		if(required!=provided)
			LOG_WARN("MPI_Init_thread level requested!=provided "<<provided<<"!="<<required);
		return provided;
	}

	void init(int *argc, char ***argv)
	{
		MPI_TRY_EXEC_FUNC(MPI_Init(argc,argv));
	}

	std::string getHostName()
	{
		char hostName[MPI_MAX_PROCESSOR_NAME];
		int len;
		MPI_TRY_EXEC_FUNC(MPI_Get_processor_name(hostName, &len));
		return std::string(hostName);
	}

	double wallClockTime()
	{
		return MPI_Wtime();
	}
};

// debug helper
// inspired by: https://www.open-mpi.org/faq/?category=debugging
inline void waitForDebugger(bool interactiveMode=true)
{
	char hostname[256];
	gethostname(hostname, sizeof(hostname));
	printf("PID %d on %s ready for attach\n", getpid(), hostname);
	fflush(stdout);
	if(interactiveMode)
	  std::getchar();
	else
	// no terminal input so just loop and wait for debugger to set value*
	// * gdb example: set val i = 1
	{
	  int i=0;
	  while (0 == i)
		sleep(3);
	}
}

} /* namespace mpi */
} /* namespace oslc */

#endif /* SRC_OSLC_MPI_MPI_H_ */
