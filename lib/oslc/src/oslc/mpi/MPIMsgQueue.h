/*
 * MPIMsgQueue.h
 *
 *  Created on: Jun 29, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_MPI_MPIMSGQUEUE_H_
#define SRC_OSLC_MPI_MPIMSGQUEUE_H_

#include<oslc/ipc/MessageQueue.h>
#include<oslc/mpi/MPI.h>

#include<oslc/lul/Serializer.h>
#include <oslc/lul/MemBuffer.h>

#include <oslc/lul/Time.h>



namespace oslc {
namespace mpi {

template<class T>
class MPIIO: public ipc::IOBase<T>
{
public:
	MPIIO(unsigned int buffer_size): m_memStrmIn(buffer_size), m_memStrmOut(buffer_size) {}

	bool tryReadData(T &msg)
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		// specify where we want the messages from
		int tag = 0; //m_comm.rank(); //MPI_ANY_SOURCE MPI_ANY_TAG
		int rank = m_comm.rank();
		// if not server (rank==0) then listen to server only
		int src = (rank!=0) ? 0 :  MPI_ANY_SOURCE;
		MPI_Status status;
		// first check if message is available so we don't block
		if(m_comm.iProbe(src,tag,&status)==false)
			return false;
		int readCount;
		m_comm.getCount(&status,MPI_CHAR,&readCount);
		LOG_INFO(m_comm.rank()<<" received from "<<status.MPI_SOURCE<<" "<<readCount<<" bytes");
		// check if we need to resize our buffer
        if(readCount>m_memStrmIn.getMemBuff().buffSize())
          m_memStrmIn.getMemBuff().resize(false,readCount);
		// read data from mpi layer
		//LOG_INFO(m_comm.rank()<<" calling receive from "<<src);
		m_comm.receive(m_memStrmIn.getMemBuff().buff(),m_memStrmIn.getMemBuff().buffSize(),MPI_CHAR,src,tag,&status); //MPI_ANY_TAG
		if(status.MPI_ERROR)
		  LOG_ERROR(m_comm.rank()<<" received data from "<<status.MPI_SOURCE<<" with tag "<<status.MPI_TAG<<" error = "<<status.MPI_ERROR);

		// serialize data
		m_memStrmIn.getMemBuff().resetReadPos();
		oslc::lul::IBinarySerializer<oslc::lul::IOMemStream> serI(m_memStrmIn);
		serI>>msg;
		if(rank!=msg.dest)
		{
		  LOG_ERROR(m_comm.rank()<<" received data from "<<msg.src<<" with dest "<<msg.dest<<" with tag "<<status.MPI_TAG);
		}
		else
		{
		  LOG_DEBUG(m_comm.rank()<<" received data from "<<msg.src<<" with dest "<<msg.dest<<" with tag "<<status.MPI_TAG);
		}
		// prepare var fields
		//msg.src = status.MPI_SOURCE;
		//msg.time = oslc::lul::_::stdch::now();
		return true;
	}

	bool tryWriteData(T &msg)
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		// write data to mpi layer
		//MPI_Status status;
		int tag = 0;
		int src = m_comm.rank();
		m_memStrmOut.getMemBuff().resetWritePos();
		oslc::lul::OBinarySerializer<oslc::lul::IOMemStream> serI(m_memStrmOut);
		// prepare var fields
		msg.src = src;
		//msg.time = oslc::lul::_::stdch::now();
		// serialize data
		serI<<msg;
		int msgSize = m_memStrmOut.getMemBuff().usedSpace();
		LOG_INFO(src<<" calling send to "<<msg.dest<<" with "<<msgSize<<" bytes");
		m_comm.send(m_memStrmOut.getMemBuff().buff(),msgSize,MPI_CHAR,msg.dest,tag);
		return true;
	}

	MPI::Comm &getComm() { return m_comm; }
private:
	// data
    MPI::Comm m_comm;
    oslc::lul::IOMemStream m_memStrmIn;
    oslc::lul::IOMemStream m_memStrmOut;
    // serialize calls to mpi
	std::mutex m_lock;

};

template<class T>
class MPIMsgQueue: public ipc::MessageQueue<T,MPIIO>
{
public:
	typedef ipc::MessageQueue<T,MPIIO> MessageQueueType;
	using ipc::MessageQueue<T,MPIIO>::MessageQueue;

	virtual void onPopulateClientList()
	{
		int size  = MessageQueueType::m_mio.getComm().size();
		for(int i = 1; i<size;i++)
		{
			// create client and get ref to it
			typename MessageQueueType::ClientPtr clientPtr = std::make_shared<typename MessageQueueType::Client>();
			clientPtr->id = i;
			MessageQueueType::m_clients[i] = clientPtr;
		}
	}

	//MPIMsgQueue();
	//~MPIMsgQueue();
};

} /* namespace mpi */
} /* namespace oslc */

#endif /* SRC_OSLC_MPI_MPIMSGQUEUE_H_ */
