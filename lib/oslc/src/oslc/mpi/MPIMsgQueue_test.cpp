/*
 * MPIMsgQueue_test.cpp
 *
 *  Created on: Jun 29, 2018
 *      Author: admin
 */

#include<oslc/lul/TestHarness.h>

#include<oslc/lul/RandomNumGen.h>


#include <oslc/mpi/MPIMsgQueue.h>

namespace oslc {
namespace mpi {


REGISTER_TEST("MPIMsgQueue","test 1")
{
  using namespace oslc::lul;
  //MPI mpi(MPI::ThreadSupport::Multiple);
  MPI mpi(MPI::ThreadSupport::Serialized);
  MPI::Comm comm;
  int size = comm.size();
  int rank = comm.rank();
  std::string hostName = mpi.getHostName();
  LOG_INFO("host name="<<hostName<<" Size="<<size<<" Rank="<<rank);


  const int MsgsPerRank = 2;

  const int N = size;

  IntRNG<int> rng(1,N-1);



  if(rank==0)
  {

	  waitForDebugger();
	  MPIMsgQueue<int> msgQ(2,true);
	  msgQ.start();

	  std::vector<int> sendVec(N,MsgsPerRank);
	  int totalMsgCount = (N - 1) * MsgsPerRank;
	  LOG_INFO("total message count: "<<totalMsgCount);
	  for(int i=0;i<totalMsgCount;i++)
	  {
		// find an available slot to send
		int index;
		do
		{
		  index = rng.rand();
		  //int trySendCount = --; // dec and test -> save how many times we had to look again if we want to report that later...
		} while(sendVec[index]--<=0);

		ipc::Message<int> msg(index);
		msg.dest = index;
	    msgQ.enqueueOut(msg);
	  }
      LOG_INFO("Send complete");
	  int replyCount = 0;
	  std::vector<int> replyVec(N,0);
	  while(replyCount < totalMsgCount)
	  {
		  // wait for replies
		  if(msgQ.queueInSize())
		  {
			  //int data = msgQ.dequeueIn();
			  //int data;
			  //msgQ.dequeueIn(data);
			  ipc::Message<int> msg;
			  msgQ.dequeueIn(msg);
			  replyCount++;
			  int msgData = msg.getData();
			  LOG_INFO(rank << " Read data="<<msgData<<" From "<<msg.src<<" ReplyCount "<<replyCount);
			  replyVec[msgData]++;
			  LOG_IF(MsgLog::Error,"bad reply "<<msgData<<"="<<replyVec[msgData],replyVec[msgData]>MsgsPerRank);
		  }
		  else
			  std::this_thread::yield();

	  }

	  //std:for_each(replyVec.begin(),replyVec.end(),[](int val)
	  int errCount = 0;
	  for(int i=1;i<N;i++)
	  {
		if(replyVec[i]!=MsgsPerRank)
		{
			LOG_TEST_ERR("replyVec for "<<i<<"="<<replyVec[i]);
			errCount++;
		}
	  }
	  TEST_EXP(errCount==0);
	  //if
	  //qThread.stop
      LOG_INFO("Send/Receive complete");
      msgQ.sendAbort();
      //std::this_thread::sleep(100);
      std::this_thread::sleep_for (std::chrono::seconds(1));
      LOG_INFO("Stopping queue...");
      msgQ.stop();
      //std::this_thread::sleep_for(std::chrono::seconds(3));
      //LOG_INFO("Wait for join");
      //qThread.join();
	  //std::cout<<txtFX::green<<"* MsgQueue Test Passed *"<<txtFX::reset<<std::endl;
      // abort mpi environment
      LOG_INFO("Abort process");
      //comm.abort(0);
  }
  else
  {
	  MPIMsgQueue<int> msgQ(2,false);
	  msgQ.start();

	  while(msgQ.isActive())
	  {
		if(msgQ.queueInSize())
		{
			//int data = msgQ.dequeueIn();
			//int data;
			//msgQ.dequeueIn(data);
			ipc::Message<int> msg;
			msgQ.dequeueIn(msg);
	        LOG_INFO(rank << " Read data="<<msg.getData()<<" From "<<msg.src);
	        msg.dest = msg.src;
			// make positive
	        //msg.data = msg.data * -1;
	        LOG_INFO(rank << " Reply data="<<msg.getData()<<" To "<<msg.dest);
	        msgQ.enqueueOut(msg);
	        LOG_INFO(rank << " queueOutSize "<< msgQ.queueOutSize());
		}
		else
		  std::this_thread::yield();
	  }
      LOG_INFO(rank << " Stopping .... ");
      // must call join or detach to avoid exception
      //qThread.join();
      msgQ.stop();
  }


}

REGISTER_TEST("MPIMsgQueue","test 2")
{
  using namespace oslc::lul;
  //MPI mpi(MPI::ThreadSupport::Multiple);
  MPI mpi(MPI::ThreadSupport::Serialized);
  MPI::Comm comm;
  int size = comm.size();
  int rank = comm.rank();
  std::string hostName = mpi.getHostName();
  LOG_INFO("host name="<<hostName<<" Size="<<size<<" Rank="<<rank);


  const int MsgsPerRank = 10;

  const int N = size;

  //IntRNG<int> rng(1,N-1);



  if(rank==0)
  {

	  waitForDebugger();
	  MPIMsgQueue<int> msgQ(1024*10,true);
	  msgQ.start();

	  std::vector<int> sendVec(N,MsgsPerRank);
	  int totalMsgCount = (N - 1) * MsgsPerRank;
	  LOG_INFO("total message count: "<<totalMsgCount);
	  for(int i=0;i<totalMsgCount;i++)
	  {
		ipc::Message<int> msg(i);
		msg.dest = ipc::Message<int>::AnyAddress;
	    while(!msgQ.tryEnqueueOut(msg,100))
	    {
	      LOG_WARN("Waiting for queue...");
	    }
	  }
//	  int i=0;
//	  while(i++<totalMsgCount)
//	  {
//		// wait for queue to be ready to send
//		while(!msgQ.shouldEnqueue())
//	      //std::this_thread::sleep_for (std::chrono::milliseconds(1));
//		  std::this_thread::yield();
//		// send message
//		ipc::Message<int> msg(i);
//		msg.dest = ipc::Message<int>::AnyAddress;
//	    msgQ.enqueueOut(msg);
//	    // next message
//	  }
      LOG_INFO("Send complete");
	  int replyCount = 0;
	  std::vector<int> replyVec(N,0);
	  while(replyCount < totalMsgCount)
	  {
		  // wait for replies
		  if(msgQ.queueInSize())
		  {
			  //int data = msgQ.dequeueIn();
			  //int data;
			  //msgQ.dequeueIn(data);
			  ipc::Message<int> msg;
			  msgQ.dequeueIn(msg);
			  replyCount++;
			  int msgData = msg.getData();
			  LOG_INFO(rank << " Read data="<<msgData<<" From "<<msg.src<<" ReplyCount "<<replyCount);
			  replyVec[msgData]++;
			  //LOG_IF(MsgLog::Error,"bad reply "<<msgData<<"="<<replyVec[msgData],replyVec[msgData]>MsgsPerRank);
		  }
		  else
			  std::this_thread::yield();

	  }

	  //std:for_each(replyVec.begin(),replyVec.end(),[](int val)
	  //int errCount = 0;
	  LOG_INFO("Reply vector=");
	  for(int i=1;i<N;i++)
	  {
//		if(replyVec[i]!=MsgsPerRank)
//		{
//			LOG_TEST_ERR("replyVec for "<<i<<"="<<replyVec[i]);
//			errCount++;
//		}
		std::cout<<i<<"="<<replyVec[i]<<std::endl;
	  }
	  //std::cout<<std::endl;
	  TEST_EXP(replyCount==totalMsgCount);
	  //if
	  //qThread.stop
      LOG_INFO("Send/Receive complete");
      msgQ.sendAbort();
      //std::this_thread::sleep(100);
      std::this_thread::sleep_for (std::chrono::seconds(1));
      LOG_INFO("Stopping queue...");
      msgQ.stop();
      //std::this_thread::sleep_for(std::chrono::seconds(3));
      //LOG_INFO("Wait for join");
      //qThread.join();
	  //std::cout<<txtFX::green<<"* MsgQueue Test Passed *"<<txtFX::reset<<std::endl;
      // abort mpi environment
      LOG_INFO("Abort process");
      //comm.abort(0);
  }
  else
  {
	  IntRNG<int> rng(1,100);
	  int delayMilli = (rank%4==1) ? 250 : 0;//rng.rand();//rank*30;

	  MPIMsgQueue<int> msgQ(1024*10,false);
	  msgQ.start();

	  while(msgQ.isActive())
	  {
		if(msgQ.queueInSize())
		{
			//int data = msgQ.dequeueIn();
			//int data;
			//msgQ.dequeueIn(data);
			ipc::Message<int> msg;
			msgQ.dequeueIn(msg);
	        LOG_INFO(rank << " Read data="<<msg.getData()<<" From "<<msg.src);
			// delay thread
	        std::this_thread::sleep_for(std::chrono::milliseconds(delayMilli));
	        msg.editData() = msg.dest;
	        msg.dest = msg.src;
	        LOG_INFO(rank << " Reply data="<<msg.getData()<<" To "<<msg.dest);
	        msgQ.enqueueOut(msg);
	        LOG_INFO(rank << " queueOutSize "<< msgQ.queueOutSize());
		}
		else
		  std::this_thread::yield();
	  }
      LOG_INFO(rank << " Stopping .... ");
      // must call join or detach to avoid exception
      //qThread.join();
      msgQ.stop();
  }

}


} /* namespace mpi */
} /* namespace oslc */
