/*
 * MPI_test.cpp
 *
 *  Created on: Jun 5, 2018
 *      Author: admin
 */

#include <oslc/mpi/MPI.h>

#include <oslc/lul/TestHarness.h>
#include <oslc/lul/MemBuffer.h>
#include <oslc/lul/Serializer.h>

// !!! main must be defined in global name space !!!
GENERATE_TEST_MAIN


namespace oslc {
namespace mpi {

//MPI mpi(MPI::ThreadSupport::Single);


REGISTER_TEST("MPI","test 1")
{
  LOG_WARNING("*** Remember to run this test using mpirun command ***");
  LOG_WARNING("*** Example: mpirun -np 2 Debug/mpiTest            ***");
  LOG_WARNING("***                                                ***");

  MPI mpi(MPI::ThreadSupport::Single);
  MPI::Comm comm;
  int size = comm.size();
  int rank = comm.rank();
  std::string hostName = mpi.getHostName();
  LOG_INFO("host name="<<hostName<<" Size="<<size<<" Rank="<<rank);

  const int N = 10;
  if(rank==0)
  {
	char buff[N] = {'a','b','c','d','e','1','2','3','4',0};
	for(int i=0;i<N;i++)
	  for(int j=1;j<size;j++)
        comm.send(&buff[i],1,MPI_CHAR,j,i);
  }
  else
  {
	while(true)
	{
	  char buff;
      //comm.receive(&buff,1,MPI_CHAR);
	  MPI_Status status;
      comm.receive(&buff,1,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,&status);
      std::cout<<rank<<" Received: "<<buff<<" | From: "<<status.MPI_SOURCE<<":"<<status.MPI_TAG<<std::endl;
      if(buff==0)
    	  break;
	}
  }

}


struct SerDataTest
{
  int x;
  float y;
  double z;
  //
  //
  //
  template<class Serializer>
  void serialize(Serializer &ser, const unsigned int version)
  {
	 ser & x;
	 ser & y & z;
  }
};


REGISTER_TEST("MPI","test 2")
{
  using namespace oslc::lul;
  MPI mpi(MPI::ThreadSupport::Single);
  MPI::Comm comm;
  int size = comm.size();
  int rank = comm.rank();
  std::string hostName = mpi.getHostName();
  LOG_INFO("host name="<<hostName<<" Size="<<size<<" Rank="<<rank);

  const SerDataTest TestData = {1,1.2,3.1 };

  if(rank==0)
  {
	  const int N = 10;
	  //char *buff = new char[N];
	  //MemBuffer sbuf(buff, buff + N);
	  MemBuffer memBuff(N);

	  //OSerializer<MemBuffer> ser(sbuf,20);
	  std::ostream strmX(&memBuff);
	  //OSerializer<std::stringstream> ser(strmX,20);
	  OSerializer<std::ostream,SerializationType::Binary> ser(strmX,20);
	  SerDataTest sdt = TestData;
	  LOG_INFO("Send data="<<sdt.x<<":"<<sdt.y<<":"<<sdt.z);
	  ser<<sdt;
	  //std::string data = strmX.str();
      //comm.send(data.c_str(),data.size(),MPI_CHAR,1,1);
      //comm.send(buff,N,MPI_CHAR,1,1);
      comm.send(memBuff.buff(),memBuff.usedSpace(),MPI_CHAR,1,1);
      LOG_INFO("Send complete: "<<memBuff.usedSpace()<<" bytes");
  }
  else
  {
	  const int N = 100;
//	  char *buff = new char[N];
//	  MemBuffer sbuf(buff, buff + N);
      //comm.receive(&buff,1,MPI_CHAR);
	  MemBuffer memBuff(N);
	  MPI_Status status;
      comm.receive(memBuff.buff(),memBuff.buffSize(),MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,&status);
      LOG_INFO("Receive complete");
      int readCount;
      comm.getCount(&status,MPI_CHAR,&readCount);
      //LOG_INFO(comm.rank()<<" received from "<<src<<" "<<readCount<<" bytes");

      LOG_INFO(rank<<" Received data from: "<<status.MPI_SOURCE<<":"<<status.MPI_TAG<<" "<<readCount<<" bytes");
      std::istream inStrm(&memBuff);
	  ISerializer<std::istream,SerializationType::Binary> ser(inStrm);
	  SerDataTest sdt = {0,0,0};
	  ser>>sdt;
	  LOG_INFO("Receive data="<<sdt.x<<":"<<sdt.y<<":"<<sdt.z);
	  TEST_EXP(sdt.x==TestData.x and sdt.y==TestData.y and sdt.z==TestData.z);
  }

}

} /* namespace mpi */
} /* namespace oslc */
