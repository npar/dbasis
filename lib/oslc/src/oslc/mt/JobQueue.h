/*
 * JobQueue.h
 *
 *  Created on: Oct 11, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_MT_JOBQUEUE_H_
#define SRC_OSLC_MT_JOBQUEUE_H_

#include<functional>
#include<atomic>

#include<oslc/lul/Log.h>
#include<oslc/mt/Queue.h>

namespace oslc {
namespace mt {

template<class FType>
using JobQueueFunc = FType;

template<class FType>
using JobQueueFuncSPtr = std::shared_ptr < JobQueueFunc<FType> >;

template<class FType>
using JobQueueBase = mt::Queue< JobQueueFuncSPtr<FType> >;


template<class FType>
class JobQueue: public JobQueueBase<FType>
{
public:

	typedef JobQueueBase<FType> QueueType;

	typedef JobQueueFunc<FType> Job;

	typedef JobQueueFuncSPtr<FType> JobSPtr;

	JobQueue(std::size_t numOfThreads)
	{
		m_activeJobCounter = 0;
		m_shouldRun = true;
		init(numOfThreads);
	}

	void init(std::size_t numOfThreads)
	{
		m_threads.resize(numOfThreads);
		for(int i=0;i<m_threads.size();i++)
		  m_threads[i] = std::thread(std::bind(&JobQueue::jobHandler,this));
	}

	// interface
	// enqueue a job copy (uses job copy constructor to create a new copy of job!)
	void enqueue(const Job &job)
	{
		JobSPtr jobPtr = std::make_shared<Job>(job);
		QueueType::enqueue(jobPtr);
	}

	// enqueue a shared job pointer (uses an existing job shared pointer)
	void enqueue(const JobSPtr &jobPtr)
	{
		QueueType::enqueue(jobPtr);
	}

	void jobHandler()
	{
    	JobSPtr jobPtr;
        while(m_shouldRun)
        {
        	if(QueueType::dequeue(jobPtr))
        	{
        		m_activeJobCounter++;
        		try
        		{
        		//jobPtr->operator()();
        		  (*jobPtr)();
        		}
        		catch(...)
				{
        		  LOG_ERROR("job threw unhandled exception");
				}
      		    m_activeJobCounter--;
        	}
        }
	}

	size_t activeJobCount() { return m_activeJobCounter; }

	void waitForQueue()
	{
	   while(!QueueType::empty())
		   std::this_thread::yield();
	}

	void stopThreads()
	{
		m_shouldRun = false;
		QueueType::signalAllWaiting();
		for(int i=0;i<m_threads.size();i++)
			if(m_threads[i].joinable())
			   m_threads[i].join();
	}

	void waitForJobsToComplete()
	{
	  // first wait for queue to be empty
	  waitForQueue();
	  // now stop the threads
	  stopThreads();
	}

	~JobQueue()
	{
		// if any threads might be running try to stop them
		if(m_shouldRun)
		  stopThreads();
	}
private:
	// data
    std::vector<std::thread> m_threads;
    bool m_shouldRun;
	std::atomic<size_t> m_activeJobCounter;
};

} /* namespace mt */
} /* namespace oslc */

#endif /* SRC_OSLC_MT_JOBQUEUE_H_ */
