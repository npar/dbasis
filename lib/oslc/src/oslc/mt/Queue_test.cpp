/*
 * Queue_test.cpp
 *
 *  Created on: Oct 10, 2018
 *      Author: admin
 */

#include <oslc/lul/TestHarness.h>

#include <oslc/mt/Queue.h>


namespace oslc {
namespace mt {

REGISTER_TEST("MTQUEUE","test 1")
{
  mt::Queue<int> queue;

  const int N = 10;

  for(int i=0;i<N;i++)
    queue.enqueue(i);

  int res = queue.dequeue();

  if(queue.dequeue(res,3))
	  LOG_TEST_MSG("dequeue called successfully");
  else
	  LOG_TEST_MSG("dequeue called unsuccessfully");

}


} /* namespace mt */
} /* namespace oslc */
