/*
 * MTSafeObject.h
 *
 *  Created on: Oct 13, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_MT_MTSAFEOBJECT_H_
#define SRC_OSLC_MT_MTSAFEOBJECT_H_

#include <utility>

namespace oslc {
namespace mt {

template<class Obj, class Lock>
class MTSafeObject : public std::pair<Obj,Lock>
{
public:
	using std::pair<Obj,Lock>::pair;
	//MTSafeObject(const Obj &obj, const Lock &lock): m_obj(obj), m_lock(lock) {}
	//~MTSafeObject();
	Obj &getObj()   { return std::pair<Obj,Lock>::first; }
	Lock &getLock() { return std::pair<Obj,Lock>::second; }
//private:
//	// data
//    Obj m_obj;
//    Lock m_lock;
};

} /* namespace mt */
} /* namespace oslc */

#endif /* SRC_OSLC_MT_MTSAFEOBJECT_H_ */
