/*
 * Queue.h
 *
 *  Created on: Oct 10, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_MT_QUEUE_H_
#define SRC_OSLC_MT_QUEUE_H_

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

namespace oslc {
namespace mt {

template<class T>
class Queue
{
public:
	Queue() {}

	// interface
	void enqueue(const T &data)
	{
	    std::unique_lock<std::mutex> lock(m_mutex);
	    m_queue.push(data);
	    lock.unlock();
	    m_cond.notify_one();
	}

	void enqueue(T &&data)
	{
	    std::unique_lock<std::mutex> lock(m_mutex);
	    m_queue.push(std::move(data));
	    lock.unlock();
	    m_cond.notify_one();
	}

	T dequeue()
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		// only if the queue is empty we should lock -> solve issue where cond was signaled before we started waiting
		if(m_queue.empty())
		  m_cond.wait(lock, [&] { return !m_queue.empty(); });
		T data = m_queue.front();
		m_queue.pop();
		return data;
	}

	bool dequeue(T &data, unsigned int maxWaitMill)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		// only if the queue is empty we should lock -> solve issue where cond was signaled before we started waiting
		if(m_queue.empty())
		{
		  bool success = m_cond.wait_until(lock, std::chrono::steady_clock::now() + std::chrono::milliseconds(maxWaitMill), [&] { return !m_queue.empty(); });
		  if(!success)
		    return false;
		}
		data = m_queue.front();
		m_queue.pop();
		return true;
	}

    // a version can return false, useful when we want to signal the thread to do something else besides waiting for data available
	bool dequeue(T &data)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		// only if the queue is empty we should lock -> solve issue where cond was signaled before we started waiting
		if(m_queue.empty())
		{
		  m_cond.wait(lock);
		  if(m_queue.empty())
		    return false;
		}
		data = m_queue.front();
		m_queue.pop();
		return true;
	}

	size_t size() { return m_queue.size(); }
	bool empty()  { return m_queue.empty();}

	// can be used to wake up all threads waiting/blocking on m_cond and let them exit gracefully
	void signalAllWaiting() { m_cond.notify_all(); }

	~Queue() {}

	// data
private:
   std::queue<T> m_queue;
   std::mutex m_mutex;
   std::condition_variable m_cond;
};

} /* namespace mt */
} /* namespace oslc */

#endif /* SRC_OSLC_MT_QUEUE_H_ */
