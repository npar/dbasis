/*
 * JobQueue_test.cpp
 *
 *  Created on: Oct 11, 2018
 *      Author: admin
 */
#include<atomic>

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/Time.h>

#include <oslc/mt/JobQueue.h>

namespace oslc {
namespace mt {

class MyFuncObj //: public std::funtion<int>
{
public:
  MyFuncObj(const MyFuncObj &rhs)
  {
	this->id = rhs.id;
	LOG_INFO("job obj (cp) created: "<<id);
  }
  MyFuncObj(MyFuncObj &&rhs)
  {
	this->id = rhs.id;
	LOG_INFO("job obj (mv) created: "<<id);
  }
  MyFuncObj(int id): id(id)
  {
	LOG_INFO("job obj created: "<<id);
  }
  void operator()()
  {
	counter++;
	LOG_INFO("Hello from job obj: "<<id<<" "<<counter);
	// sim workload - give other threads chance to run
	oslc::lul::WallTime::sleepMilli(id%10);
  }
  ~MyFuncObj()
  {
	LOG_INFO("job obj detroyed: "<<id);
  }
  // data
  int id;
  static std::atomic<int> counter;
};

std::atomic<int> MyFuncObj::counter(0);

//void blaFunc()
//{
//	int i= 0;
//	LOG_INFO("Hello from job: "<<i);
//}

const int NumOfThreads = 4;
const int N = 10;

REGISTER_TEST("JobQueue","test 1")
{
	//typedef JobQueue<void()> JobQueueType;

	// for non object type (X) we need std function of X
	JobQueue<std::function<void()>> jq(NumOfThreads);

	std::atomic<int> counter(0);

	//auto func = []{ LOG_INFO("Hello from job: "<<1); };
//	const int N = 1;

	for(int i=0;i<N;i++)
	{
		//void (*func)() = [i]{ LOG_INFO("Hello from job: "<<i); };
		//jq.enqueue(blaFunc);
		auto func = [i,&counter]{ counter++; LOG_INFO("Hello from job: "<<i<<" "<<counter); };
		jq.enqueue(func);
		//jq.enqueue(JobQueueType::Job(func));
		//oslc::lul::WallTime::sleepMilli(100);
	}

//	while(!jq.empty())
//	{
//		LOG_INFO("Waiting for queue: "<<jq.size());
//		oslc::lul::WallTime::sleepMilli(100);
//	}

	//auto func = [&jq](){return jq.empty();};
	oslc::lul::WallTime::bussyWait(100,1,[&jq]{return jq.empty();});

	TEST_EXP2(counter==N,"counter=="<<counter);

}

REGISTER_TEST("JobQueue","test 2")
{
	JobQueue<MyFuncObj> jq(NumOfThreads);

    //
	//!!! Note: this loop uses the copy constructor for MyFuncObj and so creates a new copy of MyFuncObj !!!
	// See next loop example of shared pointer use
	for(int i=0;i<N;i++)
	{
		MyFuncObj mfo(i);
		jq.enqueue(mfo);
		//jq.enqueue(JobQueueType::Job(mfo));
	}

	oslc::lul::WallTime::bussyWait(100,1,[&jq]{return jq.empty();});

	TEST_EXP2(MyFuncObj::counter==N,"counter=="<<MyFuncObj::counter);

	typedef JobQueue<MyFuncObj> JobQueueType;

	//
	//!!! Note: shared pointer usage example !!!
	//
	for(int i=0;i<N;i++)
	{
		JobQueueType::JobSPtr jobPtr = std::make_shared<JobQueueType::Job>(i);
		jq.enqueue(jobPtr);
	}

	//oslc::lul::WallTime::bussyWait(100,1,[&jq]{return jq.empty();});
	jq.waitForJobsToComplete();

	TEST_EXP2(MyFuncObj::counter==N*2,"counter=="<<MyFuncObj::counter);

}

//REGISTER_TEST("JobQueue","test 3")
//{
//	typedef JobQueue<MyFuncObj> JobQueueType;
//	JobQueueType jq(NumOfThreads);
//
////	const int N = 1;
//
//	for(int i=0;i<N;i++)
//	{
//		//std::shared_ptr<MyFuncObj> mfo = std::make_shared<MyFuncObj>(i);
//		//MyFuncObj mfo(i);
//		JobQueueType::JobSPtr jobPtr = std::make_shared<JobQueueType::Job>(i);
//		jq.enqueue(jobPtr);
//		//jq.enqueue(JobQueueType::Job(mfo));
//	}
//
//	oslc::lul::WallTime::bussyWait(100,1,[&jq]{return jq.empty();});
//
//	TEST_EXP2(MyFuncObj::counter==N,"counter=="<<MyFuncObj::counter);
//
//	return true;
//}


} /* namespace mt */
} /* namespace oslc */
