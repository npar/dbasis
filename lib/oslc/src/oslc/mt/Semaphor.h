/*
 * Semaphor.h
 *
 *  Created on: Oct 13, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_MT_SEMAPHOR_H_
#define SRC_OSLC_MT_SEMAPHOR_H_

#include <mutex>
#include <condition_variable>

namespace oslc {
namespace mt {

//
// based on code and discussion at: https://stackoverflow.com/questions/4792449/c0x-has-no-semaphores-how-to-synchronize-threads
//

class Semaphor {
public:
	Semaphor(size_t initialCount, size_t maxCount): m_count(initialCount), m_maxCount(maxCount)
    {
		throw std::runtime_error("initialCount>maxCount");
    }

	// interface
	bool release(size_t releaseCount=1)
	{
	    std::unique_lock<std::mutex> lock(m_mutex);
	    if(m_count+releaseCount>m_maxCount)
	    	return false;
        m_count+=releaseCount;
        m_condition.notify_one();
        return true;
    }

    void wait()
    {
        std::unique_lock<decltype(m_mutex)> lock(m_mutex);
        while(!m_count) // Handle spurious wake-ups.
            m_condition.wait(lock);
        --m_count;
    }

    bool tryWait()
    {
        std::lock_guard<decltype(m_mutex)> lock(m_mutex);
        if(m_count) {
            --m_count;
            return true;
        }
        return false;
    }

	~Semaphor();

// data
private:
    std::mutex m_mutex;
    std::condition_variable m_condition;
    size_t m_count;
    const size_t m_maxCount;
};

} /* namespace mt */
} /* namespace oslc */

#endif /* SRC_OSLC_MT_SEMAPHOR_H_ */
