/*
 * Log.h
 *
 *  Created on: Jul 31, 2017
 *      Author: admin
 */

#ifndef OSLC_LUL_LOG_H_
#define OSLC_LUL_LOG_H_

#include<iostream>
#include <iomanip>
#include <mutex>
#include <thread>

#include <oslc/lul/TextFX.h>
#include <oslc/lul/Time.h>
#include <map>

namespace oslc {
namespace lul {


class MsgLog
{
public:

  enum Type
  {
	  // base types
	  None    = 0x0,
	  Error   = 0x1,
	  Warning = 0x2,
	  Info    = 0x4,
	  Debug   = 0x8,
	  // predefined levels
	  ErrorLevel  = Error,
	  WarnLevel   = Warning | Error,
	  InfoLevel   = Info | Warning | Error,
	  DebugLevel  = Debug | Info | Warning | Error,
	  // user defined values start here
	  UserDefinedBase = 0x10,
  };

  class DummyStream : public std::ostream, public std::streambuf
  {
  public:
	DummyStream() : std::ostream(this) {}
    template <class T>
    DummyStream &operator<<(const T &data)
    {
  	  return *this;
    }
  };

  MsgLog(std::ostream &dest, int level)
  {
	  //m_cpuTime.start();
	  m_wallTimer.start();
	  setLogLevel(level);
	  setDest(dest);
	  // format strings if terminal output
	  setOutputFormat(lul::isOutputDirectedToTerminal(dest) ? OutputFormat::Terminal : OutputFormat::File);
  }

  void setDest(std::ostream &dest)
  {
	  m_destPtr = &dest;
  }

  enum OutputFormat { Terminal, File };

  void setOutputFormat(OutputFormat outputFormat)
  {
	  // format strings if terminal output
	  if(outputFormat==OutputFormat::Terminal)
	  {
		  warningFormatedStr = txtFX::yellow.str + "Warning" + txtFX::reset.str;
		  errorFormatedStr = txtFX::red.str + "Error" + txtFX::reset.str;
	  }
	  else // if not terminal output
	  {
		  warningFormatedStr = "Warning";
		  errorFormatedStr   = "Error";
	  }
  }

  const char *type2String(Type type)
  {
	  switch(type)
	  {
	  case Info:
		  return "Info";
	  case Warning:
		  return warningFormatedStr.c_str();
	  case Error:
		  return errorFormatedStr.c_str();
	  case Debug:
		  return "Debug";
	  default:
	    {
//	      static char customType[]="CustomXX";
//	      customType[6]= type / 10 + 48;
//	      customType[7]= type % 10 + 48;
//		  return customType;
	      return m_typeStrMap[type].c_str();
	    }
	  }
  }

  void registerType(int id, std::string name)
  {
	  std::lock_guard<std::mutex> autoGuard(getLock());

	  if(id>=UserDefinedBase)
	    m_typeStrMap[id]=name;
	  else
	  {
        log(__FILE__,__LINE__,MsgLog::Error)<<"invalid type value: "<<id<<"\n";
	  }
  }

  static MsgLog &getSysLog()
  {
	  static MsgLog theOne(std::clog,MsgLog::InfoLevel);
	  return theOne;
  }

  std::ostream &log(const char *fileName, int lineNum, Type type)
  {
	  if(type & m_logLevel) //type<=m_logLevel
	  {
	    //return std::clog<<fileName<<":"<<lineNum<<":"<<type2String(type)<<": ";
		  //_::stdch::time_point t;
		//std::thread::id tIdComp = std::this_thread::get_id();
		//unsigned int tIdSimp = *reinterpret_cast<unsigned int *>(&tIdComp);
		// (tIdSimp & 0xFFFFFFFF)

		// !!! because of the macros this is not enough!!! -> see LOG... below
		//std::lock_guard<std::mutex> autoGuard(m_mutex);

	    return *m_destPtr<<
	    		DateTime::now().format("%F:%H-%M-%S:")<<
				std::fixed<<std::setw(8)<<std::setfill('0')<<m_wallTimer.elapsedSec()<<":"<< //<< std::setfill('0')//<< std::setprecision(6)
				std::hex<<std::this_thread::get_id()<<std::dec<<":"<<
	    		fileName<<":"<<lineNum<<":"<<type2String(type)<<": ";
	  }
	  else
		return MsgLog::m_dummyStream;
  }

  void setLogLevel(int logLevel) { m_logLevel = logLevel; }
  std::mutex &getLock() { return m_mutex; }
  // data
private:
  int m_logLevel;
  DummyStream m_dummyStream;
  std::ostream *m_destPtr;
  std::string warningFormatedStr;
  std::string errorFormatedStr;
  //CPUTime m_cpuTime;
  WallTimer m_wallTimer;
  std::mutex m_mutex;
  std::map<int,std::string> m_typeStrMap;
};

//MsgLog::DummyStream  MsgLog::m_dummyStream;
//int MsgLog::m_logLevel = MsgLog::Info;


//  static void logHelper( const char * fmt, ... )
//  {
//      va_list ap;
//      va_start( ap, fmt );
//      vfprintf( stderr, fmt, ap );
//      va_end( ap );
//  }
  //__FILE__, __LINE__,

//#define logMsg(type,x) clog<<type<<": "<<x
//#define LOG_MSG(level,format, ...) CLogClass::DoLogWithFileLineInfo("%s:%d " format , __FILE__, __LINE__, __VA_ARGS__)
//#define LOG_MSG(format, ...) MessageLog::logHelper("%s:%d " format , __FILE__, __LINE__, __VA_ARGS__)

//#define LOG_BASE(logInst,type,x) logInst.log(__FILE__,__LINE__,type)<<x<<"\n"

#define LOG_BASE(logInst,type,x) \
{ \
  std::lock_guard<std::mutex> autoGuard(logInst.getLock()); \
  logInst.log(__FILE__,__LINE__,type)<<x<<"\n"; \
}



//#define LOG(type,x) oslc::lul::MsgLog::log(__FILE__,__LINE__,type)<<x<<"\n"
#define LOG(type,x) LOG_BASE(oslc::lul::MsgLog::getSysLog(),type,x)

#define LOG_IF(type,x,cond)\
	if (cond) { LOG(type,x); }

// log with file/line specified, useful log an error inside a call to log from a different location
//#define LOG_LOC_BASE(logInst,file,line,type,x) logInst.log(file,line,type)<<x<<"\n"

#define LOG_LOC_BASE(logInst,file,line,type,x) \
{ \
  std::lock_guard<std::mutex> autoGuard(logInst.getLock());	\
  logInst.log(file,line,type)<<x<<"\n"; \
}

//#define LOG_LOC(file,line,type,x) oslc::lul::MsgLog::log(file,line,type)<<x<<"\n"
#define LOG_LOC(file,line,type,x) LOG_LOC_BASE(oslc::lul::MsgLog::getSysLog(),file,line,type,x)


#define LOG_SET_LEVEL_BASE(logInst,level) logInst.setLogLevel(level)

//#define LOG_SET_LEVEL(l) oslc::lul::MsgLog::setLogLevel(l)
#define LOG_SET_LEVEL(level) LOG_SET_LEVEL_BASE(oslc::lul::MsgLog::getSysLog(),level)



#ifdef LOG_ERRORS_ONLY
  #define LOG_ENABLE_ERROR
#else
  #ifdef LOG_WARNINGS_AND_ERRORS
    #define LOG_ENABLE_WARNING
    #define LOG_ENABLE_ERROR
  #else
    #ifndef LOG_DISABLE_ALL
      #define LOG_ENABLE_DEBUG
      #define LOG_ENABLE_INFO
      #define LOG_ENABLE_WARNING
      #define LOG_ENABLE_ERROR
    #endif
  #endif
#endif



#ifdef LOG_ENABLE_DEBUG
  #define LOG_DEBUG(x)         LOG(oslc::lul::MsgLog::Debug,x)
  #define LOG_DEBUG_IF(x,cond) LOG_IF(oslc::lul::MsgLog::Debug,x,cond)
#else
  #define LOG_DEBUG(x)
  #define LOG_DEBUG_IF(x,cond)
#endif

#ifdef LOG_ENABLE_INFO
  #define LOG_INFO(x)         LOG(oslc::lul::MsgLog::Info,x)
  #define LOG_INFO_IF(x,cond) LOG_IF(oslc::lul::MsgLog::Info,x,cond)
#else
  #define LOG_INFO(x)
  #define LOG_INFO_IF(x,cond)
#endif

#ifdef LOG_ENABLE_WARNING
  #define LOG_WARNING(x)         LOG(oslc::lul::MsgLog::Warning,x)
  #define LOG_WARN(x)            LOG_WARNING(x)
  #define LOG_WARNING_IF(x,cond) LOG_IF(oslc::lul::MsgLog::Warning,x,cond)
  #define LOG_WARN_IF(x,cond)    LOG_WARNING_IF(x,cond)
#else
  #define LOG_WARNING(x)
  #define LOG_WARN(x)
  #define LOG_WARNING_IF(x,cond)
  #define LOG_WARN_IF(x,cond)
#endif

#ifdef LOG_ENABLE_ERROR
  #define LOG_ERROR(x)         LOG(oslc::lul::MsgLog::Error,x)
  #define LOG_ERROR_IF(x,cond) LOG_IF(oslc::lul::MsgLog::Error,x,cond)
#else
  #define LOG_ERROR(x)
  #define LOG_ERROR_IF(x,cond)
#endif



} // namespace oslc
} // namespace lul



#endif /* OSLC_LUL_LOG_H */
