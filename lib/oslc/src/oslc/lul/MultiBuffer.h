/*
 * MultiBuffer.h
 *
 *  Created on: Oct 9, 2019
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_MULTIBUFFER_H_
#define SRC_OSLC_LUL_MULTIBUFFER_H_

#include <vector>
#include <streambuf>
#include <oslc/lul/Log.h>
#include <oslc/lul/MemBuffer.h>

namespace oslc {
namespace lul {

// based on discussion in https://stackoverflow.com/questions/1760726/how-can-i-compose-output-streams-so-output-goes-multiple-places-at-once

//TODO: add remove buffer/stream functionality
//TODO: finish/test buffer read functionality

class MultiBuffer: public std::streambuf
{
public:
	// default constructor no reading involved
	MultiBuffer(): m_memBuff(0,0)
	{

	}
	// set rw pointers
	MultiBuffer(char* begin, char* end): m_memBuff(begin,end)
    {
	}
	// if this will be used for reading allocate buffer
	MultiBuffer(const int size): m_memBuff(size)
	{

	}
	// add a buffer to list
	void add(std::streambuf *buff)
	{
		m_buffList.push_back(buff);
	}
	int underflow() override
	{
		//LOG_INFO("MultiBuffer underflow called");
		for(auto buff :m_buffList)
			m_memBuff.sputc(buff->sgetc());
        //return std::streambuf::underflow();
		return m_memBuff.sgetc();
	}

	int overflow (int c) override
	{
		//LOG_INFO("MemBuffer overflow called: "<<c);
		for(auto buff :m_buffList)
			buff->sputc(c);
		return c;
	}
	// data
protected:
	std::vector<std::streambuf *> m_buffList;
	MemBuffer m_memBuff;

};


template<class T>
class MultiStream : public T
{
  public:
	template<class... ARGS>
	MultiStream(ARGS && ... args) : T(0), m_buff(std::forward<ARGS>(args)...)
    {
        T::rdbuf(&m_buff);
    }
	template<class S>
	void add(S &strm)
	{
        strm.flush();
		m_buff.add(strm.rdbuf());
	}
	MultiBuffer &getMemBuff() { return m_buff; }
	// data
  protected:
	MultiBuffer m_buff;
};

using OMultiStream  = MultiStream<std::ostream>;
using IMultiStream  = MultiStream<std::istream>;
using IOMultiStream = MultiStream<std::iostream>;

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_MULTIBUFFER_H_ */
