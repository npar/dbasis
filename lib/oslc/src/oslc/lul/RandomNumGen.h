/*
 * RandomNumGen.h
 *
 *  Created on: May 25, 2018
 *      Author: admin
 */

#ifndef OSLC_LUL_RANDOMNUMGEN_H_
#define OSLC_LUL_RANDOMNUMGEN_H_

// enable if etc
#include <type_traits>
#include <oslc/lul/Misc.h>


////////
// for random numbers
#include <chrono>
#include <random>

namespace oslc {
namespace lul {

template <class RND_GEN = std::default_random_engine, template<class> class DIST = std::uniform_int_distribution, class T=int>
class RandomNumGen {
public:
	RandomNumGen(bool autoSeed=false)
    {
		// construct a trivial random generator engine from a time-based seed:
		if(autoSeed)
			seed();
    }

	RandomNumGen(T min, T max, bool autoSeed=false): m_distribution(min,max)
	  //RandomNumGen(min,max,autoSeed ? genClockBasedSeed() : 0)
	{
		if(autoSeed)
			seed();
	}

	RandomNumGen(T min, T max, unsigned seed): m_distribution(min,max)
	{
		RandomNumGen::seed(seed);
	}

	~RandomNumGen()
	{

	}
	// interface
	static unsigned genClockBasedSeed()
	{
      // obtain a seed from the system clock
	  return std::chrono::system_clock::now().time_since_epoch().count();
	}
	void seed(unsigned seed = genClockBasedSeed())
	{
	  m_randNumGen.seed(seed);
	}
	T rand(T low, T high)
	{
	  return m_distribution(m_randNumGen,typename DIST<T>::param_type(low,high));
	}
	void setRange(T low, T high)
	{
	  m_distribution.param(typename DIST<T>::param_type(low,high));
	}
	T rand()
	{
	  return m_distribution(m_randNumGen);
	}

	T operator ()() { return rand(); }
	T operator () (T min, T max) { return rand(min,max); }

	// data
	RND_GEN m_randNumGen;
	DIST<T> m_distribution;

};

template<class T=int>
using IntRNG = RandomNumGen<std::default_random_engine,std::uniform_int_distribution,T>;

template<class T=double>
using RealRNG = RandomNumGen<std::default_random_engine,std::uniform_real_distribution,T>;

//template<class T>
//class BaseRNG { };
//
//template<>
//class BaseRNG<int> : public IntRNG
//{
//	//using RNGType = typename std::enable_if<!std::is_floating_point<T>::value,IntRNG>::type;
//};
//
//template<>
//class BaseRNG<double> : public RealRNG
//{
//	//using RNGType = typename std::enable_if<!std::is_floating_point<T>::value,IntRNG>::type;
//};
//
//template<class T>
//class RNG: public BaseRNG<T>
//{
//};

template<class T, class Enable = void>
class BaseRNG { };

template<class T>
class BaseRNG<T,typename std::enable_if_t<std::is_integral<T>::value> > : public IntRNG<T>
{
	//using RNGType = typename std::enable_if<!std::is_floating_point<T>::value,IntRNG>::type;
	using IntRNG<T>::IntRNG;
};

template<class T>
class BaseRNG<T,typename std::enable_if_t<std::is_floating_point<T>::value> > : public RealRNG<T>
{
	//using RNGType = typename std::enable_if<!std::is_floating_point<T>::value,IntRNG>::type;
	using RealRNG<T>::RealRNG;
};

template<class T>
class RNG: public BaseRNG<T>
{
	using BaseRNG<T>::BaseRNG;
};

//template<class T>
//class RNG:
//using RNG = IntRNG;

} // namespace oslc
} // namespace lul

#endif /* OSLC_LUL_RANDOMNUMGEN_H_ */
