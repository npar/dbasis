/*
 * Misc.h
 *
 *  Created on: Dec 4, 2017
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_MISC_H_
#define SRC_OSLC_LUL_MISC_H_

#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <memory>

namespace oslc {
namespace lul {

//
// create a stringify macro -> missing in GCC
// useful for #pragma message among other things
// example: #pragma message ("This variable =" STRINGIFY(_MY_VAR))
//

#define XSTRINGIFY(s) #s
#define STRINGIFY(s) XSTRINGIFY(s)

#define CONCAT_IMPL( x, y ) x##y
#define MACRO_CONCAT( x, y ) CONCAT_IMPL( x, y )

//
// usage example: GEN_STR_CALL_FUNC("hello "<<12345,LOG_FUNC);
//
#define GEN_STR_CALL_FUNC_BASE(tempStrm,s,f) \
  std::stringstream tempStrm; \
  tempStrm<<s; \
  f(tempStrm.str())

#define GEN_STR_CALL_FUNC(s,f) GEN_STR_CALL_FUNC_BASE(MACRO_CONCAT(gen_str_call_auto_var,__COUNTER__),s,f)


//
// template helpers
//

//template<class T>
struct FalseType {
  static bool const value = false;
};

//template<class T>
struct TrueType {
  static bool const value = true;
};

template<class T>
struct is_vector : FalseType {};

template<class T>
struct is_vector<std::vector<T> > : TrueType {};

template<class T>
struct is_map : FalseType {};

template<class K, class V>
struct is_map< std::map<K,V> > : TrueType {};

template<class T>
struct is_string : FalseType {};

template<>
struct is_string<std::string> : TrueType {};

//template<class T>
//struct is_iteratable : FalseType {};
//
//template<class T>
//struct is_iteratable: decltype(T::iterator, std::true_type() ) {};

template <typename T>
struct is_shared_pointer: FalseType
{};

template <typename T>
struct is_shared_pointer< std::shared_ptr<T> >
  : TrueType
{};

template <typename T>
struct is_unique_pointer: FalseType
{};

template <typename T>
struct is_unique_pointer< std::unique_ptr<T> >
  : TrueType
{};

//
// generate helper classes to check for existence of member function/data
//
// discussion and example references:
//   https://dev.krzaq.cc/post/checking-whether-a-class-has-a-member-function-with-a-given-signature/
//   https://www.boost.org/doc/libs/1_67_0/libs/tti/doc/html/index.html
// usage example:
//   OSLC_GEN_HAS_MEMBER(testFunc)
//   OSLC_GEN_HAS_TEMPLATE_MEMBER(testTemplateFunc)
//   ...
//   bool x = has_member_testFunc<TestMember>::value;
//   bool t = has_template_member_testTemplateFunc<TestMember>::value;

#define OSLC_GEN_HAS_MEMBER(F)\
template <typename T>\
class has_member_##F \
{\
  template <typename U>\
  static constexpr std::false_type test(...) { return {};}\
  template <typename U>\
  static constexpr auto test(U *) ->\
     decltype(&U::F, std::true_type() ){ return {}; }\
public:\
  static constexpr bool value = test<T>(nullptr);\
};

#define OSLC_GEN_HAS_TEMPLATE_MEMBER(F)\
template <typename T>\
class has_template_member_##F \
{\
  template <typename U>\
  static constexpr std::false_type test(...) { return {};}\
  template <typename U>\
  static constexpr auto test(U *) ->\
     decltype(&U:: template F<T>, std::true_type() ){ return {}; }\
public:\
  static constexpr bool value = test<T>(nullptr);\
};


//
// Erase container elements for which pred function returns true
// based on: http://en.cppreference.com/w/cpp/experimental/vector/erase_if
//
template <class Container, class Predicate>
void erase_if(Container& c, Predicate pred)
{
	c.erase(std::remove_if(c.begin(), c.end(), pred), c.end());
}

} /* namespace lul */
} /* namespace oslc */


// smart pointers
#include <memory>

// make unique is missing from c++ 11
// http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3656.htm
#if ( !defined(__cpp_lib_make_unique) || (__cplusplus < 201402L) )

namespace std
{
// source: http://en.cppreference.com/w/cpp/memory/unique_ptr/make_unique
// note: this implementation does not disable this overload for array types
//template<typename T, typename... Args>
//std::unique_ptr<T> make_unique(Args&&... args)
//{
//    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
//}

template<class T> struct _Unique_if {
    typedef unique_ptr<T> _Single_object;
};

template<class T> struct _Unique_if<T[]> {
    typedef unique_ptr<T[]> _Unknown_bound;
};

template<class T, size_t N> struct _Unique_if<T[N]> {
    typedef void _Known_bound;
};

template<class T, class... Args>
    typename _Unique_if<T>::_Single_object
    make_unique(Args&&... args) {
        return unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

template<class T>
    typename _Unique_if<T>::_Unknown_bound
    make_unique(size_t n) {
        typedef typename remove_extent<T>::type U;
        return unique_ptr<T>(new U[n]());
    }

template<class T, class... Args>
    typename _Unique_if<T>::_Known_bound
    make_unique(Args&&...) = delete;

//
// enable helper added in c++ 14
//
template< bool B, class T = void >
using enable_if_t = typename enable_if<B,T>::type;

//
// conditional helper added in c++ 14
//
template< bool B, class T, class F >
using conditional_t = typename conditional<B,T,F>::type;

} // namespace std

#endif

//
// Unique ptr casting is not available in std because of possible failure and ownership issues. This is a workaround.
// see discussion at: https://stackoverflow.com/questions/11002641/dynamic-casting-for-unique-ptr
//
namespace stdExt
{

template<class B,class D>
D &unique_ptr_refCast(const std::unique_ptr<B> &sptr)
{
  return dynamic_cast< D & >(*sptr.get());
}

} // namespace stdExt

// detect container types

template<typename T, typename _ = void>
struct is_container : std::false_type {};

template<typename... Ts>
struct is_container_helper {};

template<typename T>
struct is_container<
        T,
		std::conditional_t<
            false,
            is_container_helper<
                typename T::value_type,
                typename T::size_type,
                typename T::allocator_type,
                typename T::iterator,
                typename T::const_iterator,
                decltype(std::declval<T>().size()),
                decltype(std::declval<T>().begin()),
                decltype(std::declval<T>().end()),
                decltype(std::declval<T>().cbegin()),
                decltype(std::declval<T>().cend())
                >,
            void
            >
        > : public std::true_type {};

template<typename T, typename _ = void>
struct is_associative_container : std::false_type {};

template<typename T>
struct is_associative_container<T,
std::conditional_t<
    false,
    is_container_helper<
	    typename T::key_type,
        typename T::value_type,
        typename T::iterator,
        typename T::const_iterator,
        decltype(std::declval<T>().size()),
        decltype(std::declval<T>().begin()),
        decltype(std::declval<T>().end()),
        decltype(std::declval<T>().cbegin()),
        decltype(std::declval<T>().cend())
        >,
    void
    >
> : public std::true_type {};


#endif /* SRC_OSLC_LUL_MISC_H_ */
