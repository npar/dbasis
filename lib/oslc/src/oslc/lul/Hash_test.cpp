/*
 * Hash_test.cpp
 *
 *  Created on: Jun 19, 2019
 *      Author: admin
 */


#include <oslc/lul/Hash.h>
#include <oslc/lul/TestHarness.h>


namespace oslc {
namespace lul {

REGISTER_TEST("Hash","test 1")
{

	size_t seed = 0;
	const int N = 5;
	for(int i=0;i<N;i++)
	{
		size_t newHash = std::hash<int>{}(i);
		size_t newSeed = combineHashValues(seed,newHash);
		TEST_EXP(seed!=newSeed);
		seed = newSeed;
	}

}


} /* namespace lul */
} /* namespace oslc */
