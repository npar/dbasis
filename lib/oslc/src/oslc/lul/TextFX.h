/*
 * TextFX.h
 *
 *  Created on: May 4, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_TEXTFX_H_
#define SRC_OSLC_LUL_TEXTFX_H_

#include <iostream>
#include <sstream>

#include <oslc/lul/Misc.h>

// for isAtty function
#include <cstdio>
#include <unistd.h>

namespace oslc {
namespace lul {

//
// A C++ modern take on text attributes
// Inspired by code and discussion at: https://stackoverflow.com/questions/2616906/how-do-i-output-coloured-text-to-a-linux-terminal
//


//#define TXTFX_ATT_RESET           0
//#define TXTFX_ATT_BRIGHT          1
//#define TXTFX_ATT_DIM             2
//#define TXTFX_ATT_UNDERLINE       3
//#define TXTFX_ATT_ BLINK          5
//#define TXTFX_ATT_REVERSE         7
//#define TXTFX_ATT_HIDDEN          8
//
//#define TXTFX_FGC_BLACK           30
//#define TXTFX_FGC_RED             31
//#define TXTFX_FGC_GREEN           32
//#define TXTFX_FGC_YELLOW          33
//#define TXTFX_FGC_BLUE            34
//#define TXTFX_FGC_MAGENTA         35
//#define TXTFX_FGC_CYAN            36
//#define TXTFX_FGC_WHITE           37
//
//#define TXTFX_BGC_BLACK           40
//#define TXTFX_BGC_RED             41
//#define TXTFX_BGC_GREEN           42
//#define TXTFX_BGC_YELLOW          43
//#define TXTFX_BGC_BLUE            44
//#define TXTFX_BGC_MAGENTA         45
//#define TXTFX_BGC_CYAN            46
//#define TXTFX_BGC_WHITE           47


#define TXTFX_PROPS_RESET "\033[0m"

inline const std::string genTextPropsStr(int fgColor, int bgColor, int att)
{
	// if reset, ignore other parameters
	if(att==0)
		return TXTFX_PROPS_RESET;
	// else build a string
	std::stringstream strStrm;
	fgColor+=30;  // need to translate to real values
	bgColor+=40;  // "
	strStrm<< "\033[" << fgColor << ";"<< bgColor << ";"<< att << "m";
	return strStrm.str();
}


template<int FGC, int BGC = 0, int ATT = 0>
struct TextFormat
{
  const std::string str = genTextPropsStr(FGC,BGC,ATT);
  operator std::string() { return str; }
};

inline bool isOutputDirectedToTerminal(std::ostream& os)
{
	// file stream should return valid position
	bool isFileStream = (os.tellp()!=-1);
	// if file return false
	if(!isFileStream)
		return true;
	else
		return false;
}

template<int FGC, int BGC = 0, int ATT = 0>
std::ostream &operator<<(std::ostream& os, const TextFormat<FGC,BGC,ATT> & ta)
{
	// if in console mode print text effects otherwise do nothing!
	if(isOutputDirectedToTerminal(os))
	  return os<<ta.str;
	else
	  return os;
}

namespace txtFX
{
	enum Color
	{
		Black =  0,
		Red   =  1,
		Green =  2,
		Yellow = 3,
		Blue   = 4
	};
	enum Attribute
	{
		Reset = 0,
		Bright    = 1,
		Dim     = 2,
		Underlined = 3,
		Blink    = 5,
		Reverse  = 7,
		Hidden   =8
	};

    const TextFormat<Color::Red,Color::Black,Attribute::Bright> red;
    const TextFormat<Color::Green,Color::Black,Attribute::Bright> green;
    const TextFormat<Color::Yellow,Color::Black,Attribute::Bright> yellow;
    const TextFormat<Color::Blue,Color::Black,Attribute::Bright> blue;
    const TextFormat<Color::Black,Color::Black,Attribute::Reset> reset;
}

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_TEXTFX_H_ */
