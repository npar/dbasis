/*
 * FileUtil.h
 *
 *  Created on: Oct 28, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_FILEUTIL_H_
#define SRC_OSLC_LUL_FILEUTIL_H_

// for mkdir
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>

#include<string>
#include <vector>

#include <climits>


namespace oslc {
namespace lul {

class FileUtil
{
public:
	//ToDo: #define for different OS...
	// path separator OS dependent
	static const char PathSeparator = '/';
	static const std::string CurrentDir;
	static const std::string ParentDir;

	class Path
	{
	public:
		Path(std::string base=".") { add(base); }
		Path &add(std::string part)
		{
			size_t marker;
			size_t i;
			// handle special cases
			// an absolute path part
			// TODO:this will be handled differently on OS with drive letters
			// check second letter for drive separator e.g.: part[1]==':'
			if(empty() && part[0]==PathSeparator)
			{
				m_parts.push_back(part.substr(0,1));
				marker = 1;
				i = 1;
			}
			else
			{
				marker = 0;
				i = 0;
			}
			// all cases
			for(;i<part.size();i++)
				// if separator
				if(part[i]==PathSeparator)
				{
					// if not empty part -> can happen if string starts with leading separator
					if(i != marker)
					  m_parts.push_back(part.substr(marker,i - marker));
					marker = i + 1;
				}
			if(i>marker)
				m_parts.push_back(part.substr(marker,i - marker));
            // allow to concatenate path parts
			return *this;
		}
		std::string toString(size_t startPos=0, size_t endPos=PATH_MAX) const
		{
			if(m_parts.empty())
				return "";
			endPos = std::min(m_parts.size()-1,endPos);
			std::string res;
			// if we have an abs path handle separator
			if(startPos==0 && isAbsolute())
			{
				//TODO:this will be handled differently on OS with drive letters
				//res = "";
			}
			else
			    res = m_parts[startPos];
			for(size_t i=startPos+1;i<=endPos;i++)
				res+= PathSeparator + m_parts[i];
			//res=m_parts[m_parts.size()-1];
			return res;
		}
		operator std::string () const { return toString(); }

		std::string firstPart() { return toString(0,0); }
		std::string lastPart()  { return toString(size()-1); }

		bool isEqual(const Path &other) const
		{
			return toString()==other.toString();
		}
		bool isAbsolute() const
		{
			// TODO:this will be handled differently on OS with drive letters
			return !empty() && m_parts[0][0]==PathSeparator;
		}
		bool isRelative() const { return !empty() &&  !isAbsolute(); }
		std::string operator[](size_t index) { return m_parts[index]; }
		size_t size() const { return m_parts.size(); }
		bool empty()  const { return m_parts.empty();}
	private:
		// data
		std::vector<std::string> m_parts;
	};

	struct DirEntry
	{
		std::string name;
		size_t flags;
	};

	FileUtil();

	static bool makeDir(std::string dir, mode_t mode = S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	static std::string createTempDir(std::string nameTemplate = "tempDir-XXXXXX");
	static bool getFileInfo(const std::string &pathName, struct stat &s)
	{
		return stat(pathName.c_str(), &s) == 0;
	}

	static bool setFileTimes(const std::string &pathName,const struct timespec &accTime, const struct timespec &modTime, int flags = 0)
	{
		// check if relative to current folder
		int fdDir;
		if(Path(pathName).isRelative())
			fdDir = AT_FDCWD;
		else
			fdDir = 0;
		// copy new values
		struct timespec times[2]{accTime,modTime};
        // set times
		return utimensat(fdDir,pathName.c_str(),times,flags) == 0;
	}

	static bool isDir(const std::string &pathName)
	{
		struct stat s;
		return getFileInfo(pathName,s) and S_ISDIR(s.st_mode);
	}

	static bool isFile(const std::string &pathName)
	{
		struct stat s;
		return getFileInfo(pathName,s) and S_ISREG(s.st_mode);
	}

	static bool remove(const std::string &pathName);

	template<class F>
	static bool processFiles(const DirEntry &dirEntry, F &dirEntryHandler, bool dirEntryFirst = true, size_t depth=UINT_MAX);

	template<class F>
	static bool processFiles(const std::string &pathName, F &dirEntryHandler, bool dirEntryFirst = true, size_t depth=UINT_MAX)
	{
		//if(isDir(pathName))
		DirEntry dirEntry;
		dirEntry.name = pathName;
		dirEntry.flags = isDir(pathName) ? DT_DIR : DT_REG;
		return processFiles(dirEntry,dirEntryHandler,dirEntryFirst,depth);
	}

	static bool getDirEntries(std::string pathName, std::vector<DirEntry> &dirEntries);

	static bool getDirEntriesRecursive(std::string pathName, std::vector<DirEntry> &dirEntries, bool dirEntryFirst = true, size_t depth=UINT_MAX);

	static bool getFilteredDirEntriesRecursive(std::string pathName, std::vector<DirEntry> &dirEntries, std::string filter, bool fileOnly, bool dirEntryFirst = true, size_t depth=UINT_MAX);

	static std::string getCWD();

	~FileUtil();
};

template<class F>
bool FileUtil::processFiles(const DirEntry &dirEntry, F &onDirEntry, bool dirEntryFirst, size_t depth)
{
	// update depth variable
	if(depth==0)
		return true;
	else
		depth--;
	// check if folder
	if(dirEntry.flags==DT_DIR)
	{
		if(dirEntryFirst)
			onDirEntry(dirEntry);
		// first get all files in folder -> depth first
		std::vector<DirEntry> dirEntries;
		getDirEntries(dirEntry.name,dirEntries);
		for(DirEntry dirEntryNextLevel: dirEntries)
		{
			// overkill
			//remove(Path(pathName).add(entry).toString());
			if(!processFiles(
					DirEntry{dirEntry.name+PathSeparator+dirEntryNextLevel.name,dirEntryNextLevel.flags},
				    onDirEntry,dirEntryFirst,depth))
			{
				//LOG_INFO("onDirEntry returned false: "<<dirEntry.name);
				return false;
			}
		}
		// now we can handle the folder itself
		if(!dirEntryFirst)
			return onDirEntry(dirEntry);
		else
			return true;
	}
	else
	  return onDirEntry(dirEntry);
}

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_FILEUTIL_H_ */
