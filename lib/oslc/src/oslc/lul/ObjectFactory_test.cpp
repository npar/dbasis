/*
 * ObjectFactory_test.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: admin
 */


#include<string>
using namespace std;

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/ObjectFactory.h>

namespace oslc {
namespace lul {

class BaseType
{
public:
  virtual string getName() { return "BaseType"; }
  virtual ~BaseType() {}
};

class DerivedType: public BaseType
{
public:
  DerivedType() {}
  DerivedType(const DerivedType &d)
  {
     LOG_TEST_MSG("test DerivedType copy constructor");
  }
  virtual string getName() { return "DerivedType"; }
	// data
};

class DerivedType2: public BaseType
{
public:
  DerivedType2() {}
  DerivedType2(int param)
  {
     LOG_TEST_MSG("test DerivedType2  int constructor: "<<param);
     LOG_TEST_MSG("func name: "<<__func__<<" : "<< __PRETTY_FUNCTION__);

  }
  virtual string getName() { return "DerivedType2"; }
	// data
};

REGISTER_OBJ_TYPE(BaseType,DerivedType,"DType");
REGISTER_OBJ_TYPE(BaseType,DerivedType2,"DType2");


REGISTER_TEST("ObjectFactory","test 1")
{

	DerivedType dType;
	DerivedType2 dType2;

	std::shared_ptr<BaseType> obj1 (ObjectFactory<BaseType>::get()->create("DType"));

    LOG_TEST_MSG("testing virtual functions...");
    //LOG_TEST_MSG("testing: obj1->getName()==dType.getName()");

    TEST_EXP(obj1->getName()==dType.getName());

    std::unique_ptr<BaseType> obj2 (ObjectFactory<BaseType>::get()->create("DType2"));

    TEST_EXP(obj2->getName()!=dType.getName());

    TEST_EXP(obj2->getName()==dType2.getName());

    std::unique_ptr<BaseType> obj3 (ObjectFactory<BaseType>::create<DerivedType>());

    TEST_EXP(obj3->getName()==dType.getName());


}

REGISTER_OBJ_TYPE_WP(BaseType,DerivedType2,"DType2-1",int);

REGISTER_TEST("ObjectFactory","test 2")
{

	DerivedType dType;
	DerivedType2 dType2;


    LOG_TEST_MSG("testing virtual functions with params...");

//    ObjectFactory<BaseType,DerivedType&> objFact;
//    objFact.create<DerivedType>(dType);

    //TODO: can we make this easier to use? add nicer register function?
    ObjectFactory<BaseType,DerivedType &>::get()->registerType("DType",ObjectFactory<BaseType,DerivedType&>::create<DerivedType>);

	auto obj1 =  ObjectFactory<BaseType,DerivedType&>::get()->create("DType",dType);

    TEST_EXP(obj1->getName()==dType.getName());

    delete obj1;

    auto obj2 = ObjectFactory<BaseType,int>::get()->create("DType2-1",101);
    delete obj2;

    LOG_TEST_MSG("func name: "<<__func__<<" : "<< __PRETTY_FUNCTION__);


}


} /* namespace lul */
} /* namespace oslc */





