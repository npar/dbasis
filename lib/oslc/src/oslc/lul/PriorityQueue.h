/*
 * PriorityQueue.h
 *
 *  Created on: Jul 7, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_PRIORITYQUEUE_H_
#define SRC_OSLC_LUL_PRIORITYQUEUE_H_

#include <algorithm>    // std::make_heap, std::pop_heap, std::push_heap, std::sort_heap
#include <vector>       // std::vector
#include <functional>
#include <mutex>


namespace oslc {
namespace lul {

//TODO: change ContainerType from vector to set to avoid update on all elements
//      with a set we could update a single element

template<class T, class Compare = std::less<typename std::vector<T>::value_type> >
class PriorityQueue
{
public:
	typedef std::vector<T> ContainerType;

	PriorityQueue(size_t initialSize=0, Compare compare=Compare()) :
		m_compare(compare)
    {
		if(initialSize)
		  m_heap.reserve(initialSize);
    }

	~PriorityQueue()
	{

	}

	void update()
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		std::make_heap (m_heap.begin(),m_heap.end(),m_compare);
	}

	size_t size()
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		return m_heap.size();
	}

	void enqueue(const T &data)
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		m_heap.push_back(data);
		std::push_heap(m_heap.begin(),m_heap.end(),m_compare);
	}

	const T &front()
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		return m_heap.front();
	}
	const T &back()
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		return m_heap.back();
	}

	void dequeue()
	{
		std::lock_guard<std::mutex> autoGuard(m_lock);
		std::pop_heap(m_heap.begin(),m_heap.end(),m_compare);
		m_heap.pop_back();
	}

//	void dequeue(T &data)
//	{
//		std::lock_guard<std::mutex> autoGuard(m_lock);
//		std::pop_heap(m_heap.begin(),m_heap.end(),m_compare);
//		data = m_heap.back();
//		m_heap.pop_back();
//	}

	std::mutex &getLock() { return m_lock; }
	typename  ContainerType::iterator begin() { return m_heap.begin(); }
	typename  ContainerType::iterator end()   { return m_heap.end();   }

	ContainerType &getContainer() { return m_heap; }
	const ContainerType &getContainer() const { return m_heap; }


protected:
	ContainerType m_heap;
	Compare m_compare;
	std::mutex m_lock;
};

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_PRIORITYQUEUE_H_ */
