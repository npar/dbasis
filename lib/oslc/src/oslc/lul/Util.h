/*
 * Util.h
 *
 *  Created on: Sep 26, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_UTIL_H_
#define SRC_OSLC_LUL_UTIL_H_

#include<string>

namespace oslc {
namespace lul {

class Util {
public:
	Util();
	~Util();
};

std::string genUniqueName(const char *formatStr="%F-%H-%M-%S-");

int execSysCmd(const char *cmd);

inline size_t replaceStr(std::string& str, const std::string &from, const std::string &to)
{
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return 0;
    str.replace(start_pos, from.size(), to);
    return start_pos;
}

inline size_t replaceAllStr(std::string& str, const std::string &from, const std::string &to)
{
	size_t count = 0;
    while(replaceStr(str,from,to)!=std::string::npos)
    	count++;
    return count;
}


} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_UTIL_H_ */
