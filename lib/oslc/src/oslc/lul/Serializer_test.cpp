/*
 * Serializer_test.cpp
 *
 *  Created on: Feb 15, 2018
 *      Author: admin
 */

#include <iostream>
#include <sstream>

#include <oslc/lul/Serializer.h>
#include <oslc/lul/TestHarness.h>
#include <oslc/lul/MemBuffer.h>

#include<map>

namespace oslc {
namespace lul {



struct SerDataTest
{
  int x;
  float y;
  double z;
  //
  //
  //
  template<class Serializer>
  void serialize(Serializer &ser, const uint32_t version)
  {
	 ser & x;
	 ser & y & z;
	 //if(ser.isStoring())
	 //  ser.store(str);
//	 char sep = ' ';
//	 ser & sep;
//     ser & x;
//	 ser & sep;
//   ser & y & sep & z;
  }

};

struct SerDataTest2
{
  int x;
  float y;
  double z;
  //
  //
  //
//  template<class Serializer>
//  void serialize(Serializer &ser, const unsigned int version)
//  {
//	 ser & x;
//	 ser & y & z;
////	 char sep = ' ';
////	 ser & sep;
////     ser & x;
////	 ser & sep;
////   ser & y & sep & z;
//  }

  template<class Serializer>
  void load(Serializer &ser, const uint32_t version)
  {
	 ser & x;
	 ser & y & z;
//	 char sep = ' ';
//	 ser & sep;
//     ser & x;
//	 ser & sep;
//   ser & y & sep & z;
  }

  template<class Serializer>
  void store(Serializer &ser, const uint32_t version) const
  {
	 ser & x;
	 ser & y & z;
//	 char sep = ' ';
//	 ser & sep;
//     ser & x;
//	 ser & sep;
//   ser & y & sep & z;
  }

};

struct SerDataTest3: public SerDataTest
{
	SerDataTest3()
	{
		x = y = z = 1;
		m_data = { 2,2,2 };
	}

	template<class Serializer>
	void serialize(Serializer &ser, const uint32_t version)
	{
		SerDataTest::serialize(ser,version);
		ser & m_data;
	}

	// data
	SerDataTest2 m_data;
};

struct SerDataTest4
{
  int x;
  std::string str;

  template<class Serializer>
  void serialize(Serializer &ser, const uint32_t version)
  {
	 ser & x;
	 ser & str;
  }

};

struct SerDataTest5
{
  int x;
  std::string str;

  template<class Serializer>
  //void serialize(Serializer &ser, const void *version)
  void serialize(Serializer &ser)
  {
	 ser & x;
	 ser & str;
  }

};


REGISTER_TEST("Serializer","Serializer test 1")
{
  LOG_INFO("isNetworkOrder()=="<<isNetworkOrder());
  //Serializer<std::istream> ser(std::cin);
#define TEST_STR  std::string("12345")
  std::stringstream strstrm;
  Serializer<std::stringstream,SerializationType::Text,LoadStore> ser(strstrm);
  TEST_EXP(ser.mode()==LoadStore);
  ser<<TEST_STR;
  std::string str;
  ser>>str;
  TEST_EXP(str==TEST_STR);

  #define TEST_IVAL 12345

  //auto flags = strstrm.flags();
  // TODO: should we change behavior? need to clear before writing again...
  strstrm.clear();
  int x = 0;
  ser<<TEST_IVAL;
  ser>>x;
  TEST_EXP(x==12345);
  // must clear do both to clear stringstream:
  // https://stackoverflow.com/questions/12112259/how-to-reuse-stringstream
  strstrm.clear();
  strstrm.str("");
  x = 125;
  ser<<x;
  int y;
  ser>>y;
  TEST_EXP(y==x);
  // another variation
  std::stringstream strstrm2;
  Serializer<std::stringstream,SerializationType::Binary> ser2(strstrm2);
  ser2<<x;
  int z;
  ser2>>z;
  TEST_EXP(x==z);

  //std::stringstream strmX;
  using StreamType = std::fstream;
  StreamType strmX("testFile.x",std::ios::out | std::ios::in | std::ios::binary);
  OTextSerializer<StreamType> outSer(strmX,20);
  //outSer<<z;
  SerDataTest sdt = {1,1.2,3.1 };
  outSer<<sdt;
  strmX.clear();                 // clear fail and eof bits
  strmX.seekg(0, std::ios::beg); // back to the start!
  ITextSerializer<StreamType> inSer(strmX);
  //inSer>>y;
  SerDataTest sdt2 = {0,0,0};
  inSer>>sdt2;

  {
  const int N = 100;
  using StreamType = std::iostream;
  char *buff = new char[N];
  MemBuffer sbuf(buff, buff + N);

  StreamType strmX(&sbuf);
  OBinarySerializer<StreamType> outSer(strmX,20);
  //outSer<<z;
  SerDataTest sdt = {1,1.2,3.1 };
  outSer<<sdt;
  //strmX.clear();                 // clear fail and eof bits
  //strmX.seekg(0, std::ios::beg); // back to the start!
  IBinarySerializer<StreamType> inSer(strmX);
  //inSer>>y;
  SerDataTest2 sdt2_1 = {0,0,0};
  inSer>>sdt2_1;
  TEST_EXP(
		  sdt.x==sdt2_1.x &&
		  sdt.y==sdt2_1.y &&
		  sdt.z ==sdt2_1.z
		  )
  }

  {
  IOMemStream memStrm(100);
  OBinarySerializer<IOMemStream> serO(memStrm,21);
  SerDataTest3 sdt3,sdt3_1; //= { SerDataTest{ 0,1,2 }, SerDataTest2{1,2,3 } };
  sdt3.m_data = {1,2,3};
  sdt3.x = 3;
  sdt3.y = 2;
  sdt3.z = 1;
  serO<<sdt3;
  IBinarySerializer<IOMemStream> serI(memStrm);
  serI>>sdt3_1;
  TEST_EXP(
		  sdt3.x==sdt3_1.x &&
		  sdt3.y==sdt3_1.y &&
		  sdt3.z ==sdt3_1.z &&
		  sdt3.m_data.x == sdt3_1.m_data.x &&
		  sdt3.m_data.y == sdt3_1.m_data.y &&
		  sdt3.m_data.z == sdt3_1.m_data.z
		  )
  }

  {
  IOMemStream memStrm(100);
  OBinarySerializer<IOMemStream> serO(memStrm,21);
  SerDataTest4 sdt4,sdt4_1; //= { SerDataTest{ 0,1,2 }, SerDataTest2{1,2,3 } };
  sdt4.x = 3;
  sdt4.str = "hello";
  serO<<sdt4;
  IBinarySerializer<IOMemStream> serI(memStrm);
  serI>>sdt4_1;
  TEST_EXP(
		  sdt4.x==sdt4_1.x &&
		  sdt4.str ==sdt4_1.str
		  )
  }


  std::stringstream strStrm3;
  //Serializer<std::stringstream> strmY(strStrm3);
  //SerialHelper<Serializer<std::stringstream> ,'b'> tstSH(strmY);
  SerialHelper<std::stringstream ,SerializationType::Text> tstSH(strStrm3);
  int a;
  tstSH.load(a);

  // test existence of serial/load/store template member functions
  //bool xo = HasLoadFunc<int>::value;
  //bool xo1 = has_template_member_load<int>::value;
  TEST_EXP(HasLoadFunc<int>::value==has_template_member_load<int>::value);
  TEST_EXP(HasSerializeFunc<SerDataTest>::value==true);
  //bool tempHLF = HasLoadFunc<SerDataTest>::value;
  TEST_EXP(HasLoadFunc<SerDataTest>::value==false);
  TEST_EXP(HasStoreFunc<SerDataTest>::value==false);
  TEST_EXP(HasSerializeFunc<SerDataTest2>::value==false);
  TEST_EXP(HasLoadFunc<SerDataTest2>::value==true);
  TEST_EXP(HasStoreFunc<SerDataTest2>::value==true);


}


REGISTER_TEST("Serializer","Serializer test 2")
{
  // serialize stl vector collections
  {
	IOMemStream memStrm(100);
	OBinarySerializer<IOMemStream> serO(memStrm,21);
	std::vector<int> vec = {1,2,3,4,5};
	serO<<vec;
	//std::for_each(vec.begin(),vec.end(),[] (int &val) { return  val+=1; } );
	std::vector<int> vec2 = {0,0,0};
	IBinarySerializer<IOMemStream> serI(memStrm);
	serI>>vec2;
	TEST_EXP(vec==vec2);
  }

  {
	IOMemStream memStrm(100);
	OBinarySerializer<IOMemStream> serO(memStrm,21);
	typedef std::shared_ptr<int> PtrType;
	std::vector<PtrType> vec = {std::make_shared<int>(1),std::make_shared<int>(2)};
	serO<<vec;
	//std::for_each(vec.begin(),vec.end(),[] (int &val) { return  val+=1; } );
	std::vector<PtrType> vec2 = {0,0,0};
	IBinarySerializer<IOMemStream> serI(memStrm);
	serI>>vec2;
    TEST_EXP(vec.size()==vec2.size());
	for(size_t i=0;i<vec.size();i++)
	  TEST_EXP(*vec[i]==*vec2[i]);
  }

  // serialize stl map collections
  {
	IOMemStream memStrm(100);
	OBinarySerializer<IOMemStream> serO(memStrm,21);
	std::map<int,int> map;// = { {1,2} {3,4} };
	map[1]=2;
	map[2]=3;
	serO<<map;
//	size_t size = map.size();
//	serO & size;
//	for(auto val: map)
//	{
//		serO & (val.first);
//		serO & (val.second);
//	}
	std::map<int,int> map2;
	IBinarySerializer<IOMemStream> serI(memStrm);
	serI>>map2;
//	size_t size2;
//	serI & size2;
//	for(size_t i= 0; i<size2; i++)
//	{
//		int first;
//		int second;
//		serI & first;
//		serI & second;
//		map2[first]=second;
//	}
	TEST_EXP(map==map2);
  }

  {
	IOMemStream memStrm(100);
	OBinarySerializer<IOMemStream> serO(memStrm,21);
	typedef std::shared_ptr<int> PtrType;
	std::map<int,PtrType> map;// = { {1,2} {3,4} };
	map[1]=std::make_shared<int>(2);
	map[2]=std::make_shared<int>(3);
	serO<<map;
	std::map<int,PtrType> map2;
	IBinarySerializer<IOMemStream> serI(memStrm);
	serI>>map2;
	TEST_EXP(map.size()==map2.size());
	for(auto &pair: map)
	  TEST_EXP(*map[pair.first]==*map2[pair.first]);

  }


}

REGISTER_TEST("Serializer","Serializer test 3")
{
//#define TEST_STR  std::string("12345")
  std::stringstream strstrm;
  OBinarySerializer<std::stringstream,NoVersionInfo> oSer(strstrm);
  int testIVal = 5;
  SerDataTest5 sdt5{testIVal,TEST_STR},sdt5_1;
  oSer<<sdt5;
  IBinarySerializer<std::stringstream,NoVersionInfo> iSer(strstrm);
  std::string str;
  iSer>>sdt5_1;
  TEST_EXP(sdt5.str==TEST_STR && sdt5.x==testIVal);
  //Serializer<std::stringstream>
  //Serializer<std::stringstream,SerializationType::Text,LoadStore> ser(strstrm);
  //ser<<sdt3;

}

REGISTER_TEST("Serializer","Serializer test 4")
{
  std::vector<int> v;
  bool res = is_container< int >::value;
  res = is_container< std::vector<int> >::value;
  res = is_associative_container< std::vector<int> >::value;
  res = is_associative_container< std::map<int,int> >::value;
  LOG_INFO("res => "<<res);

  typedef typename std::enable_if<is_associative_container< std::map<int,int> >::value && !is_shared_pointer< std::map<int,int> >::value , float>::type bla;
  bla x = 3;
  LOG_INFO("bla => "<<x);

}


} /* namespace lul */
} /* namespace oslc */
