/*
 * MultiBuffer_test.cpp
 *
 *  Created on: Oct 9, 2019
 *      Author: admin
 */

#include <oslc/lul/MultiBuffer.h>

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/FileUtil.h>

namespace oslc {
namespace lul {

REGISTER_TEST("MultiBuffer","test 1")
{
    OMultiStream linkOut;
    linkOut.add(std::cout);
    linkOut<<"output from test 1"<<std::endl;
    linkOut.add(std::cerr);
    linkOut<<"output from test 1"<<std::endl;
    linkOut.add(std::clog);
    linkOut<<"output from test 1"<<std::endl;


}

REGISTER_TEST("MultiBuffer","test 2")
{
	std::string tempFolder = FileUtil::createTempDir();
	bool res = FileUtil::makeDir(tempFolder + "/testMakeDir",S_IRWXU | S_IRWXG);
	TEST_EXP(res);

    OMultiStream linkOut;
    std::ofstream logFile(tempFolder + "/testLogFile.log");
    linkOut.add(logFile);
    linkOut.add(std::clog);
    for(int i=0;i<10;i++)
      linkOut<<"output from test 2"<<std::endl;

    // cleanup
    FileUtil::remove(tempFolder);


}

REGISTER_TEST("MultiBuffer","test 3")
{
	std::string tempFolder = FileUtil::createTempDir();
	bool res = FileUtil::makeDir(tempFolder + "/testMakeDir",S_IRWXU | S_IRWXG);
	TEST_EXP(res);

	MsgLog testLog(std::clog,MsgLog::Info);


    OMultiStream linkOut;
    std::ofstream logFile(tempFolder + "/testLogFile.log");
    linkOut.add(logFile);
    linkOut.add(std::clog);
	testLog.setDest(linkOut);
    testLog.setOutputFormat(MsgLog::OutputFormat::File);

    for(int i=0;i<10;i++)
    {
    	  testLog.log(__FILE__,__LINE__,MsgLog::Info)<<"output from test3 Info"<<"\n";
    	  testLog.log(__FILE__,__LINE__,MsgLog::Warning)<<"output from test Warning"<<"\n";
    	  testLog.log(__FILE__,__LINE__,MsgLog::Error)<<"output from test Error"<<"\n";
    }

    // cleanup
    FileUtil::remove(tempFolder);


}


} /* namespace lul */
} /* namespace oslc */
