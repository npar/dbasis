/*
 * TextFX_test.cpp
 *
 *  Created on: May 4, 2018
 *      Author: admin
 */

#include <oslc/lul/TextFX.h>

#include <oslc/lul/TestHarness.h>


namespace oslc {
namespace lul {


REGISTER_TEST("TextFx","test 1")
{

  std::cout<<txtFX::red<<"* Test Text FX *"<<txtFX::reset<<std::endl;
  std::cout<<txtFX::green<<"* Test Text FX *"<<txtFX::reset<<std::endl;
  std::cout<<txtFX::blue<<"* Test Text FX *"<<txtFX::reset<<std::endl;

}


} /* namespace lul */
} /* namespace oslc */
