/*
 * StringParser_test.cpp
 *
 *  Created on: Mar 3, 2018
 *      Author: admin
 */


#include <oslc/lul/TestHarness.h>

#include "StringParser.h"

using namespace oslc::lul;

bool genTempFile(std::string fileData, std::string &tmpFileName)
{
  // warning: the use of `tmpnam' is dangerous, better use `mkstemp'
  //const char *fname = tmpnam(NULL);  // Get temp name
  //FILE *fp = fopen(fname, "w");  // Create the file

  char tmpNametemplate[] = P_tmpdir  "/tmpTstFolderXXXXXX";
  tmpFileName = mkdtemp(tmpNametemplate);  // Get temp name
  tmpFileName += "/tempTestFile.tmp";
  std::ofstream fOut;
  fOut.open(tmpFileName,std::ios::app | std::ios::out);
  if(!fOut.good())
  {
	LOG_ERROR("failed to create file: "<<tmpFileName);
	return false;
  }
  fOut<<fileData;
  //FILE *fp = fopen(tmpFileName.c_str(), "w");  // Create the file
  //fwrite(fileData.c_str(),1,fileData.size(),fp);
  //fclose(fp);
  //tmpFileName = fname;
  return true;
}

REGISTER_TEST("StringParser","test 1")
{

  std::string fileData =
"<Level><Name> L1 </Name><Description> bla bla level</Description><Challenge><Description> the description text </Description>"
"<Function>bonFuncV2</Function>"
"<Input>1 3</Input> <Output> 4 </Output>"
"<Input>4 3</Input> <Output> 7 </Output>"
"</Challenge>"
"</Level>"
;

  std::string tmpFileName;
  genTempFile(fileData,tmpFileName);
  StringParser sp;
  LOG_TEST_MSG("Loading tag data from file "<<tmpFileName);
  TEST_EXP(sp.loadDataFromFile(tmpFileName.c_str()));
  std::string tag, data;
  StringParser::TagReader tr;
  TEST_EXP(sp.initTagReader(tr));
  int counter1 = 0, counter2 = 0;
  while(tr.getNextTag(tag,data))
  {
	counter1++;
    LOG_TEST_MSG("Tag: "<< tag);
    LOG_TEST_MSG("Data: " <<data);
  }
  remove(tmpFileName.c_str());

  TEST_EXP(sp.loadDataFromString(fileData.c_str()));
  TEST_EXP(sp.initTagReader(tr));
  while(tr.getNextTag(tag,data))
  {
	counter2++;
    LOG_TEST_MSG("Tag: "<< tag);
    LOG_TEST_MSG("Data: " <<data);
  }
  TEST_EXP(counter1==counter2);
  //tag = "<*>";
  //tr.getNextTag(tag,data);
  //LOG_TEST_MSG(tag);
  //LOG_TEST_MSG(data);


}

