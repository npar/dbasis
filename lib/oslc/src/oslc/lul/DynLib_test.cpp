/*
 * DynLib_test.cpp
 *
 *  Created on: Aug 4, 2020
 *      Author: admin
 */

#include "DynLib.h"

#include<string>
using namespace std;

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/Util.h>

#include <oslc/lul/DynLib_test_dll.h>

namespace oslc {
namespace lul {

//class MyPlugin: public MyPluginBase
//{
//	//using IPlugin::IPlugin;
//public:
//	//MyPlugin(std::string name, uint32_t version): IPlugin(name,version) {}
//	//MyPlugin(): IPlugin("testP",1010) {}
//	int testMemberFunc(int x) { return x+1; }
//
//};
//
////DEFINE_PLUGIN(MyPlugin,"myPlugin",123)
//DYNLIB_DEFINE_PLUGIN(MyPluginBase, MyPlugin,"myPlugin");
//
////DYNLIB_LIBRARY;
//DYNLIB_PLUGIN_FACTORY(MyPluginBase);


//DYNLIB_DLL_EXPORTED_FUNC int myExportedFunc(int num);
//
//typedef int(*MyExpoFuncPtrType)(int);
//
//
//DYNLIB_DLL_EXPORTED_FUNC int myExportedFunc(int num)
//{
//	return num + 1;
//}

/*
bool buildHelper()
{
	// build dll
	std::string buildDllCmd;
#if defined(__GNUC__)
	buildDllCmd = "g++ --std=c++11 -shared Debug/DynLib.o Debug/DynLib_test.o -fPIC -pthread -ldl -o Debug/dynlib-test.so";
#elif defined(__clang__)
	buildDllCmd = "clang++ --std=c++11 -shared Debug/DynLib.o Debug/DynLib_test.o -fPIC -pthread -ldl -o Debug/dynlib-test.so";
#else
	LOG_ERROR("I do not know this C++ compiler and how to build the dll!");
#endif
	int res = execSysCmd(buildDllCmd.c_str());
	LOG_ERROR_IF("Unable to compile dll",res);
	//TEST_EXP(!res);
   return res!=0;
}
*/

bool buildDynLib()
{
	// copy code file
	LOG_ERROR_IF("compiling src file/s",execSysCmd("g++ --std=c++11 -O0 -g3 -c -I src -Wall -fPIC src/oslc/lul/DynLib_test.cpp -o Debug/DynLib_test.o"));
	LOG_ERROR_IF("compiling src file/s",execSysCmd("g++ --std=c++11 -O0 -g3 -c -I src -Wall -fPIC src/oslc/lul/DynLib.cpp -o Debug/DynLib.o"));
	LOG_ERROR_IF("compiling src file/s",execSysCmd("g++ --std=c++11 -O0 -g3 -c -I src -Wall -fPIC src/oslc/lul/DynLib_test_dll.cpp -o Debug/DynLib_test_dll.o"));
	// build dll
	std::string buildDllCmd;
#if defined(__GNUC__)
	buildDllCmd = "g++ --std=c++11 -O0 -g3 -shared Debug/DynLib.o Debug/DynLib_test.o Debug/DynLib_test_dll.o -fPIC -pthread -ldl -o Debug/dynlib-test.so";
#elif defined(__clang__)
	buildDllCmd = "clang++ --std=c++11 -O0 -g3 -shared Debug/DynLib.o Debug/DynLib_test.o Debug/DynLib_test_dll.o -fPIC -pthread -ldl -o Debug/dynlib-test.so";
#else
	LOG_ERROR("I do not know this C++ compiler and how to build the dll!");
#endif
	int res = execSysCmd(buildDllCmd.c_str());
	LOG_ERROR_IF("Unable to compile dll",res);
	return !res;
}

REGISTER_TEST("DynLib","Test 1")
{
	TEST_EXP(buildDynLib());
	std::string libName = "Debug/dynlib-test.so";
	DynLib dlib;
	int res = dlib.loadLibrary(libName);
	LOG_ERROR_IF("unable to load lib: "<<libName,!res);
	if(res)
	{
		PluginFactory<MyPluginBase> pf(dlib);
		PluginFactory<MyPluginBase>::PluginSPtr badPluginPtr = pf.loadPlugin("foobarlugin");
		TEST_EXP(badPluginPtr==nullptr);
		PluginFactory<MyPluginBase>::PluginSPtr pluginPtr = pf.loadPlugin("MyPlugin");
		LOG_ERROR_IF("unable to load lib: "<<libName,!pluginPtr);
		if(pluginPtr)
		{
			IPlugin::Info info = pluginPtr->getInfo();
			LOG_INFO("Plugin Info: "<<info.name<<":"<<info.version);
			LOG_INFO("plugin test member func: "<<pluginPtr->testMemberFunc(1));
		}
		else
		{
			TEST_SET_FAIL;
		}
	}
}

REGISTER_TEST("DynLib","Test 2")
{
	TEST_EXP(buildDynLib());
	std::string libName = "Debug/dynlib-test.so";
	DynLib dlib;
	//IPlugin *pluginPtr = dlib.loadPlugin(libName);
    //dlib.unloadLibrary();
	int res = dlib.loadLibrary(libName);
	LOG_ERROR_IF("unable to load lib: "<<libName,!res);
	if(res)
	{
	  MyExpoFuncPtrType fptr = dlib.getSymbolHandle<MyExpoFuncPtrType>("myExportedFunc");
	  int val = fptr(1);
      LOG_INFO("fptr output: "<<val);
      TEST_EXP(val==2);
	  //LOG_INFO("Plugin Info: "<<info.name<<":"<<info.version);
	}
	else
	{
		TEST_SET_FAIL;
	}
}

REGISTER_TEST("DynLib","Test 3")
{
	TEST_EXP(buildDynLib());
	std::string libName = "Debug/dynlib-test.so";
	DynLib dlib;
	//IPlugin *pluginPtr = dlib.loadPlugin(libName);
    //dlib.unloadLibrary();
	int res = dlib.loadLibrary(libName);
	LOG_ERROR_IF("unable to load lib: "<<libName,!res);
	if(res)
	{
	  MyExpoFuncPtrType fptr = dlib.getSymbolHandle<MyExpoFuncPtrType>("dllTestFunc1");
	  int val = fptr(1);
      LOG_INFO("fptr output: "<<val);
      TEST_EXP(val==3);
	  //LOG_INFO("Plugin Info: "<<info.name<<":"<<info.version);
	}
	else
	{
		TEST_SET_FAIL;
	}
}

} /* namespace lul */
} /* namespace oslc */
