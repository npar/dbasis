/*
 * Hash.h
 *
 *  Created on: Jun 19, 2019
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_HASH_H_
#define SRC_OSLC_LUL_HASH_H_

#include <functional>

namespace oslc {
namespace lul {

class Hash {
public:
	Hash();
	~Hash();
};

//// based on boost combine hash ->
//template<typename T> void hash_combine(size_t &seed, const T &v)
//{
//	seed ^= hash_value(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
//}

// based on boost combine hash ->
// prevHashVal (seed) hash should be zero in the beginning
inline std::size_t combineHashValues(std::size_t prevHashVal, std::size_t nextHashVal)
{
	prevHashVal ^= nextHashVal + 0x9e3779b9 + (prevHashVal << 6) + (prevHashVal >> 2);
	return prevHashVal;
}

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_HASH_H_ */
