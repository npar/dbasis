/*
 * WorkerFactory.h
 *
 *  Created on: Sep 19, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_WORKERFACTORY_H_
#define SRC_OSLC_LUL_WORKERFACTORY_H_

#include<oslc/lul/ObjectFactory.h>

//#include<memory>

namespace oslc {
namespace lul {

//
// Worker factory
//
// Notes -
//   Can have multiple workers per type!

template<class Worker>
class WorkerFactory
{
public:
	WorkerFactory() {}
	typedef Worker WorkerType;
	typedef std::shared_ptr<Worker> WorkerPtr;
	typedef std::multimap<std::string,WorkerPtr> WorkerMap;
	typedef typename WorkerMap::iterator WorkerMapIterator;
	typedef typename std::pair<WorkerMapIterator,WorkerMapIterator> WorkerMapRange;
	typedef ObjectFactory<Worker> WorkerObjFactory;
	typedef typename WorkerObjFactory::ObjMap WorkerObjFactoryMap;


 // compiler does not like this
 // error: expected primary-expression before ‘>’ token     ObjectFactory< MyWorker::Worker >::create<W>();
 //	template<class W>
 //	void registerWorker(std::string type)
 //	{
 //	    //auto createFuncPtr =ObjectFactory<WorkerType>::create<W>();
 //		//ObjectFactory<Worker>::get()->registerType(type,createFuncPtr);
 //	}
	WorkerPtr createWorker(std::string type)
	{
		WorkerPtr wPtr(WorkerObjFactory::get()->create(type));
		if(!wPtr)
			throw std::runtime_error("bad worker type");
		return wPtr;
	}

	void addWorker(std::string type)
	{
		WorkerPtr wPtr = createWorker(type);
		addWorker(type,wPtr);
	}

	void addWorker(const std::string &type, const WorkerPtr &wPtr)
	{
		m_workers.emplace(type,wPtr);
	}

	WorkerPtr getWorker(std::string type)
	{
		WorkerMapIterator itr = m_workers.find(type);
		if(itr!=m_workers.end())
		  return itr->second;
		else
		  return nullptr;
	}

	WorkerPtr removeWorker(std::string type)
	{
		WorkerMapIterator itr = m_workers.find(type);
		if(itr==m_workers.end())
			return nullptr;
		WorkerPtr wptr = itr->second;
		m_workers.erase(itr);
		return wptr;
	}

	WorkerObjFactoryMap &getRegisteredWorkerTypes()
	{
		return WorkerObjFactory::get()->getMap();
	}

	WorkerMap &getAllWorkers()
	{
		return m_workers;
	}

	WorkerMapRange getWorkers(std::string type)
	{
		return m_workers.equal_range(type);
	}

	size_t getWorkersCount(std::string type)
	{
		return m_workers.count(type);
	}

	~WorkerFactory() {}

	// data
	WorkerMap m_workers;
};

#define DECLARE_WORKER_FACTORY(name,BaseWorker) typedef oslc::lul::WorkerFactory<BaseWorker> name

#define REGISTER_FACTORY_WORKER_TYPE(factoryType,workerType,typeNameStr) REGISTER_OBJ_TYPE(factoryType::WorkerType,workerType,typeNameStr)

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_WORKERFACTORY_H_ */
