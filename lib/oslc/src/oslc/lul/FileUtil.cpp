/*
 * FileUtil.cpp
 *
 *  Created on: Oct 28, 2018
 *      Author: admin
 */

#include <oslc/lul/FileUtil.h>

#include <oslc/lul/Log.h>

#include <cerrno>
#include <cstring>
#include <regex>

#include <unistd.h>

namespace oslc {
namespace lul {

const std::string FileUtil::CurrentDir = ".";
const std::string FileUtil::ParentDir = "..";

FileUtil::FileUtil()
{
	// TODO Auto-generated constructor stub
}


// Note:
// returns true on success. use errno to find error.
// Since C++ 11 errno is thread safe: https://en.cppreference.com/w/cpp/error/errno
bool FileUtil::makeDir(std::string dir, mode_t mode)
{
	// fail on empty dir
	if(dir.empty())
		return false;
	// add a trailing slash if its missing
	if(dir[dir.size()-1] != PathSeparator)
	  dir.push_back(PathSeparator);
	// create all folders in the path -
	// start from position 1, separator at pos 0 means abs path and so we skeep it
	for(size_t i = 1; i<dir.size(); i++)
	{
		if(dir[i] == PathSeparator)
		{
			std::string subDir = dir.substr(0,i);
			// check if it is an existing directory and if so skip
			if(isDir(subDir))
				continue;
			// mkdir return non zero on failure
			if(mkdir(subDir.c_str(), mode))
				return false;
		}
	}
	return true;
}

std::string FileUtil::createTempDir(std::string nameTemplate)
{
  // warning: the use of `tmpnam' is dangerous, better use `mkstemp'

  // at least the last six characters of TEMPLATE must be "XXXXXX"
  if(nameTemplate.empty() or nameTemplate[nameTemplate.size()-1]!='X')
    nameTemplate+="XXXXXX";

  return mkdtemp((char *)nameTemplate.c_str());  // Get temp name
}

bool FileUtil::getDirEntries(std::string pathName, std::vector<DirEntry> &dirEntries)
{
  DIR *dirHandle;
  struct dirent *dirEntry;
  dirHandle = opendir(pathName.c_str());
  if(dirHandle)
  {
    while ((dirEntry = readdir(dirHandle)) != NULL)
    {
    	// dirty and quick?
    	//      if(
    	//         (dirEntry->d_type==DT_DIR && dirEntry->d_name[0]=='.')
    	//		 &&
    	//    	 (dirEntry->d_name[1]==0  ||
    	//   		 (dirEntry->d_name[1]=='.' && dirEntry->d_name[2]==0)
    	//		 )
    	//    	)
      // overkill?
      if(dirEntry->d_type==DT_DIR && (dirEntry->d_name==CurrentDir || dirEntry->d_name==ParentDir))
    	  continue;

      dirEntries.push_back(DirEntry{dirEntry->d_name,dirEntry->d_type});
    }
    closedir(dirHandle);
    return true;
  }
  else
    return false;
}

bool FileUtil::getDirEntriesRecursive(std::string pathName, std::vector<DirEntry> &dirEntries, bool dirEntryFirst, size_t depth)
{
	auto func = [&dirEntries] (const FileUtil::DirEntry &de)
			{
		        dirEntries.push_back(de);
		        return true;
			};
	// search with dir last so we can delete after all files wwere deleted
	return FileUtil::processFiles(pathName,func,dirEntryFirst,depth);
}

bool FileUtil::getFilteredDirEntriesRecursive(std::string pathName, std::vector<DirEntry> &dirEntries, std::string filter, bool fileOnly, bool dirEntryFirst, size_t depth)
{
	//!!! important note => c++11 regex support is buggy on old gcc compilers
	// https://stackoverflow.com/questions/12530406/is-gcc-4-8-or-earlier-buggy-about-regular-expressions/12665408
	std::regex regExp;
	std::smatch m;
	try
	{
		regExp.assign(filter,std::regex_constants::ECMAScript);
	}
	catch(std::regex_error& e)
	{
	  LOG_ERROR("bad regex: "<<e.what()<<" <=> "<<e.code());
	  return false;
	}

	auto func = [&dirEntries,&regExp,&m,fileOnly] (const FileUtil::DirEntry &de)
			{
		        // if directory
		        if(de.flags==DT_DIR)
		        {
			        // if we want folders save entry
		        	if(!fileOnly)
			          dirEntries.push_back(de);
		        	// in any case return true to continue search
		        	return true;
		        }
		        // if we are here this is not a folder
		        // add only if we have a match
		        std::string str(de.name);
		        if(std::regex_search(str,m,regExp))
		          dirEntries.push_back(de);
		        // in any case return true
		        return true;
			};
	// search with dir last so we can delete after all files wwere deleted
	return FileUtil::processFiles(pathName,func,dirEntryFirst,depth);
}

// old version
#ifndef OLD_REMOVE
bool FileUtil::remove(const std::string &pathName)
{
	auto func = [] (const FileUtil::DirEntry &de)
			{
		        if(de.flags==DT_REG || de.flags==DT_LNK)
		        	return ::unlink(de.name.c_str())==0;
		        else
		        if(de.flags==DT_DIR)
		        	return ::rmdir(de.name.c_str())==0;
		        else
		        {
		        	LOG_WARN("remove error: "<<de.name<<" <-> "<<de.flags);
		        	return false;
		        }
			};
	// search with dir last so we can delete after all files were deleted
	return FileUtil::processFiles(pathName,func,false);
}
#else
bool FileUtil::remove(const std::string &pathName)
{
	if(isFile(pathName))
		return ::unlink(pathName.c_str())==0;
	else
	if(isDir(pathName))
	{
		// first get all files in folder -> depth first
		std::vector<DirEntry> dirEntries;
		getDirEntries(pathName,dirEntries);
		for(auto entry: dirEntries)
		{
			// overkill
			//remove(Path(pathName).add(entry).toString());
			if(!remove(pathName+PathSeparator+entry.name))
			{
				LOG_WARN("remove error: "<<pathName<<" <-> "<<std::strerror(errno));
			}
		}
		// now we can remove the folder itself
		return ::rmdir(pathName.c_str())==0;
	}
	else
	{
		LOG_WARN("remove error: "<<pathName<<" <-> "<<std::strerror(errno));
		return false;
	}
}
#endif //old remove


std::string FileUtil::getCWD()
{
	std::string path(PATH_MAX,'\0');
	if(!getcwd(&path[0],PATH_MAX))
		return "";
	size_t pos = path.find_first_of('\0');
	if(pos!=std::string::npos)
		path.resize(pos);
	return path;
}


FileUtil::~FileUtil()
{
	// TODO Auto-generated destructor stub
}

} /* namespace lul */
} /* namespace oslc */
