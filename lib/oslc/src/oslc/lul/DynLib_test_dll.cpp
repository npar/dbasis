/*
 * DynLib_test_dll.cpp
 *
 *  Created on: Oct 17, 2020
 *      Author: admin
 */

#include <oslc/lul/DynLib.h>
#include <oslc/lul/Log.h>
#include <oslc/lul/DynLib_test_dll.h>
using namespace oslc::lul;


class MyPlugin: public MyPluginBase
{
	//using IPlugin::IPlugin;
public:
	//MyPlugin(std::string name, uint32_t version): IPlugin(name,version) {}
	//MyPlugin(): IPlugin("testP",1010) {}
	int testMemberFunc(int x) { return x+1; }

};

class MyPlugin2: public IPlugin
{
	using IPlugin::IPlugin;
public:
	//MyPlugin(std::string name, uint32_t version): IPlugin(name,version) {}
	int testFunc(int x) { return x+1; }

};

DYNLIB_DLL_EXPORTED_FUNC int dllTestFunc1(int p)
{
	LOG_INFO("dllTestFunc1: "<<p);
	return p+2;
}

DYNLIB_DLL_EXPORTED_FUNC int myExportedFunc(int num)
{
	return num + 1;
}


// !!! g++ bug, need to use inside same name space !!!
//error: specialization of ‘template<class T> T* oslc::lul::PluginFactory<T>::loadPlugin(const string&)’ in different namespace [-fpermissive]
namespace oslc { namespace lul {
DYNLIB_DEFINE_PLUGIN(MyPluginBase, MyPlugin,"MyPlugin");
DYNLIB_DEFINE_PLUGIN(IPlugin, MyPlugin2,"MyPlugin2");
}}

DYNLIB_PLUGIN_FACTORY(IPlugin);
DYNLIB_PLUGIN_FACTORY(MyPluginBase);



