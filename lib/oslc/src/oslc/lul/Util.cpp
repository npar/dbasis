/*
 * Util.cpp
 *
 *  Created on: Sep 26, 2018
 *      Author: admin
 */

//for getpid
#include<unistd.h>

#include<sstream>

#include <oslc/lul/Time.h>
#include <oslc/lul/Util.h>

namespace oslc {
namespace lul {

Util::Util() {
	// TODO Auto-generated constructor stub

}

Util::~Util() {
	// TODO Auto-generated destructor stub
}

std::string genUniqueName(const char *formatStr)
{
	static int seqNum = 0;
	std::stringstream strm;
	strm<<
			oslc::lul::DateTime::now().format(formatStr)<<
			::getpid()<<"-"<<++seqNum;
	return strm.str();
}

//TODO: add add uni and bidirectional pipes
// see discussion at:
// https://en.cppreference.com/w/cpp/utility/program/system and
// https://stackoverflow.com/questions/6171552/popen-simultaneous-read-and-write
// https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-the-output-of-the-command-within-c-using-po
int execSysCmd(const char *cmd)
{
	return std::system(cmd);
}

} /* namespace lul */
} /* namespace oslc */
