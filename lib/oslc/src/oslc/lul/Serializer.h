/*
 * Serializer.h
 *
 *  Created on: Feb 15, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_SERIALIZER_H_
#define SRC_OSLC_LUL_SERIALIZER_H_

// for declval
#include <utility>

#include<oslc/lul/Misc.h>

namespace oslc {
namespace lul {

//  misc network order related functions
// TODO: need more work. could be other hw architectures
// based on code and discussion at: https://stackoverflow.com/questions/2100331/c-macro-definition-to-determine-big-endian-or-little-endian-machine
//

// big/little endian
inline bool isLittleEndian()
{
  volatile uint32_t i=0x01234567;
  // return 0 for big endian, 1 for little endian.
  return (*((uint8_t*)(&i))) == 0x67;
}

// TODO: need more work. could be other hw architectures
inline bool isNetworkOrder()
{
	return !isLittleEndian();
}

template <typename T>
T reverse(T val)
{
    T retVal;
    char *pVal = (char*) &val;
    char *pRetVal = (char*)&retVal;
    int size = sizeof(T);
    for(int i=0; i<size; i++) {
        pRetVal[size-1-i] = pVal[i];
    }
    return retVal;
}

enum SerializationType { Unknown, Text, Binary };

template<class Stream, SerializationType SType>
class SerialHelper
{
public:
	SerialHelper(Stream &strm): strm(strm) {}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	load(Type &data)
	{
	  strm.read((char *)&data,sizeof(data));
	}

	template<SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	load(std::string &data)
	{
	  size_t size;
	  strm.read((char *)&size,sizeof(size));
	  data.resize(size);
	  //!!! legal in c++ 11 !!!
	  strm.read((char *)&data[0],size);
	}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Text, void>::type
	load(Type &data)
	{
	  strm>>data; // -> this serializes text
	}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	store(const Type &data)
	{
	  strm.write((const char *)&data,sizeof(data));
	}

	template<SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	store(const std::string &data)
	{
	  size_t size = data.size();
	  strm.write((const char *)&size,sizeof(size));
	  strm.write(data.c_str(),size);
	}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Text, void>::type
	store(const Type &data)
	{
	  strm<<data; //-> this serializes text
	}

	// data
	Stream &strm;
};

enum SerializationMode { unknown = 0x0, Load = 0x1 , Store = 0x2, LoadStore = 0x3 };

template<class Stream, SerializationType SType, SerializationMode SMode=LoadStore>
class Serializer
{
public:
	Serializer(Stream &strm): strm(strm) {  }

	~Serializer() {}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	load(Type &data)
	{
	  strm.read((char *)&data,sizeof(data));
	}

	template<SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	load(std::string &data)
	{
	  size_t size;
	  strm.read((char *)&size,sizeof(size));
	  data.resize(size);
	  //!!! legal in c++ 11 !!!
	  strm.read((char *)&data[0],size);
	}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Text, void>::type
	load(Type &data)
	{
	  strm>>data; // -> this serializes text
	}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	store(const Type &data)
	{
	  strm.write((const char *)&data,sizeof(data));
	}

	template<SerializationType Select=SType >
	typename std::enable_if<Select==Binary, void>::type
	store(const std::string &data)
	{
	  size_t size = data.size();
	  strm.write((const char *)&size,sizeof(size));
	  strm.write(data.c_str(),size);
	}

	template<class Type, SerializationType Select=SType >
	typename std::enable_if<Select==Text, void>::type
	store(const Type &data)
	{
	  strm<<data; //-> this serializes text
	}

	template<class Type>
	Serializer &operator>>(Type &data)
	{
	  load(data);
	  return *this;
	}

	template<class Type>
	Serializer &operator<<(const Type &data)
	{
	  store(data);
	  return *this;
	}
	SerializationMode mode() { return SMode; }
	bool isStoring() { return mode() == Store ; }
	bool isLoading() { return mode() == Load;   }
	// data
	Stream &strm;
};

// helpers for detecting member functions
OSLC_GEN_HAS_TEMPLATE_MEMBER(serialize)
OSLC_GEN_HAS_TEMPLATE_MEMBER(load)
OSLC_GEN_HAS_TEMPLATE_MEMBER(store)

template<class T>
using HasSerializeFunc = has_template_member_serialize<T>;
template<class T>
using HasLoadFunc      = has_template_member_load<T>;
template<class T>
using HasStoreFunc     = has_template_member_store<T>;

//TODO: consider moving VersionInfo to conditional data type, enable if ...
///////////////////////////
// type to use when no version information is required for store/load operations
///////////////////////////
typedef void * NoVersionInfo;
///////////////////////////

template<class Stream, SerializationType SType, class VersionType=uint32_t>
class ISerializer : public Serializer<Stream,SType,Load>
{
public:
	using SerializerType = Serializer<Stream,SType,Load>;

	// conditional constructor - for auto version management
	template<typename V = VersionType, typename = std::enable_if_t<!std::is_same<V,NoVersionInfo>::value > >
	ISerializer(Stream &s): SerializerType(s), m_version(VersionType())
	{
	  // first thing we load the version
		SerializerType::load(m_version);
	}
    // alt constructor no version
	template<typename V = VersionType, typename = std::enable_if_t<std::is_same<V,NoVersionInfo>::value > >
	ISerializer(Stream &s, const VersionType version=VersionType()): SerializerType(s), m_version(version)
	{
	}

	template<class T>
	typename std::enable_if<!std::is_class<T>::value,ISerializer & >::type
	operator &(T &obj)
	{
	  SerializerType::load(obj);
      return *this;
	}

	template<class T>
	typename std::enable_if<std::is_class<T>::value,ISerializer & >::type
	//typename std::enable_if<std::is_class<T>::value && (HasLoadFunc<T>::value || HasSerializeFunc<T>::value) ,ISerializer & >::type
	operator &(T &obj)
	{
	  serialize(*this,obj,m_version);
      return *this;
	}

    ////////////////////////////
	// check which loading function the object has (serialize or load)*
	// * you can either have one serialize function or specialized load/store
    ////////////////////////////
	template <class T>
	auto serialize(ISerializer &ser, T &obj, const VersionType version) -> decltype (obj.serialize(ser,version),void())
	{
	  obj.serialize(ser,m_version);
	}
	template <class T>
	auto serialize(ISerializer &ser, T &obj, const VersionType version) -> decltype (obj.load(ser,version),void())
	{
	  obj.load(ser,m_version);
	}
    //
	// serialize interface without version information
    //
	template <class T>
	auto serialize(ISerializer &ser, T &obj, const VersionType version) -> decltype (obj.serialize(ser),void())
	{
	  obj.serialize(ser);
	}
	template <class T>
	auto serialize(ISerializer &ser, T &obj, const VersionType version) -> decltype (obj.load(ser),void())
	{
	  obj.load(ser);
	}
	//TODO: make collection/map iterators more general i.e for any iteratable/mapped type...
    // collection iterator
	template <class T>
	typename std::enable_if<is_vector<T>::value &&
	!is_shared_pointer<typename T::value_type >::value &&
	!is_unique_pointer<typename T::value_type >::value, void>::type
	serialize(ISerializer &ser, T &obj, const VersionType version)
	{
	  size_t size;
	  SerializerType::load(size);
	  obj.resize(size);
	  for(auto &val: obj)
		  ser & val;
	}
	template <class T>
	typename std::enable_if<is_vector<T>::value && is_shared_pointer<typename T::value_type >::value, void>::type
	serialize(ISerializer &ser, T &obj, const VersionType version)
	{
	  size_t size;
	  SerializerType::load(size);
	  obj.reserve(size);
	  obj.clear();
	  for(size_t i=0;i<size;i++ )
	  {
		  typedef typename T::value_type::element_type DataType;
		  typename T::value_type tempPtr = std::make_shared<DataType>();
		  ser & *tempPtr;
		  obj.emplace_back(tempPtr);
	  }
	}
	template <class T>
	typename std::enable_if<is_vector<T>::value && is_unique_pointer<typename T::value_type >::value, void>::type
	serialize(ISerializer &ser, T &obj, const VersionType version)
	{
	  size_t size;
	  SerializerType::load(size);
	  obj.reserve(size);
	  obj.clear();
	  for(size_t i=0;i<size;i++ )
	  {
		  typedef typename T::value_type::element_type DataType;
		  typename T::value_type tempPtr = std::make_unique<DataType>();
		  ser & *tempPtr;
		  obj.emplace_back(std::move(tempPtr));
	  }
	}


    // map iterator
	template <class T>
	typename std::enable_if<is_map<T>::value &&
	!is_shared_pointer<typename T::mapped_type >::value &&
	!is_unique_pointer<typename T::mapped_type >::value, void>::type
	serialize(ISerializer &ser, T &obj, const VersionType version)
	{
	  size_t size;
	  SerializerType::load(size);
	  for(size_t i= 0; i<size; i++)
	  {
		  typename T::value_type pair;
		  ser & (pair.first);
		  ser & (pair.second);
		  obj.insert(pair);
	  }
	}
	template <class T>
	typename std::enable_if<is_map<T>::value && is_shared_pointer<typename T::mapped_type >::value, void>::type
	serialize(ISerializer &ser, T &obj, const VersionType version)
	{
	  size_t size;
	  SerializerType::load(size);
	  for(size_t i= 0; i<size; i++)
	  {
		  typename T::value_type pair;
		  ser & (pair.first);
		  typedef typename T::mapped_type::element_type DataType;
		  typename T::mapped_type tempPtr = std::make_shared<DataType>();
		  ser & *tempPtr;
		  pair.second = tempPtr;
		  obj.insert(pair);
	  }
	}

	template <class T>
	typename std::enable_if<is_map<T>::value && is_unique_pointer<typename T::mapped_type >::value, void>::type
	serialize(ISerializer &ser, T &obj, const VersionType version)
	{
	  size_t size;
	  SerializerType::load(size);
	  for(size_t i= 0; i<size; i++)
	  {
		  typename T::value_type pair;
		  ser & (pair.first);
		  typedef typename T::mapped_type::element_type DataType;
		  typename T::mapped_type tempPtr = std::make_unique<DataType>();
		  ser & *tempPtr;
		  pair.second = std::move(tempPtr);
		  obj.insert(pair);
	  }
	}

	// is string
	template <class T>
	typename std::enable_if<is_string<T>::value, void>::type
	serialize(ISerializer &ser, T &obj, const VersionType version)
	{
	  SerializerType::load(obj);
	}

	template <class T>
	typename std::enable_if<is_string<T>::value, void>::type
	serialize(ISerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::load((T &)obj);
	}

	////////////////////////////

	template<class T>
	typename std::enable_if<std::is_class<T>::value,ISerializer & >::type
	operator >>(T &obj)
	{
	  serialize(*this,obj,m_version);
      return *this;
	}

	template<class T>
	typename std::enable_if<!std::is_class<T>::value,ISerializer & >::type
	operator >>(T &obj)
	{
	  SerializerType::load(obj);
      return *this;
	}

	// data
	VersionType m_version;
};


template<class Stream, SerializationType SType,class VersionType=uint32_t>
class OSerializer : public Serializer<Stream,SType,Store>
{
public:
	using SerializerType = Serializer<Stream,SType,Store>;

	// conditional constructor - for auto version management
	template<typename V = VersionType, typename = std::enable_if_t<!std::is_same<V,NoVersionInfo>::value > >
	OSerializer(Stream &s, const VersionType version=VersionType()): SerializerType(s), m_version(version)
	{
	  // first thing we store the version
	  SerializerType::store(m_version);
	}
    // alt constructor no version
	template<typename V = VersionType, typename = std::enable_if_t<std::is_same<V,NoVersionInfo>::value > >
	OSerializer(Stream &s): SerializerType(s), m_version(VersionType())
	{
	}

	template<class T>
	typename std::enable_if<!std::is_class<T>::value,OSerializer & >::type
	operator &(const T &obj)
	{
	  SerializerType::store(obj);
      return *this;
	}

	template<class T>
	typename std::enable_if<std::is_class<T>::value,OSerializer & >::type
	operator &(const T &obj)
	{
	  serialize(*this,obj,m_version);
      return *this;
	}

	template<class T>
	typename std::enable_if<std::is_class<T>::value,OSerializer & >::type
	operator &(T &obj)
	{
	  serialize(*this,obj,m_version);
      return *this;
	}

    ////////////////////////////
	// check which storing function the object has (serialize or store)*
	// * you can either have one serialize function or specialized load/store
    ////////////////////////////
	template <class T>
	auto serialize(OSerializer &ser, T &obj, const VersionType version) -> decltype (obj.serialize(ser,version),void())
	{
	  obj.serialize(ser,m_version);
	}
	// note this overrides the const modifier because serialize is one function used for both const and non const
	template <class T>
	auto serialize(OSerializer &ser, const T &obj, const VersionType version) -> decltype (std::declval<T &>().serialize(ser,version),void())
	{
		const_cast<T *>(&obj)->serialize(ser,m_version);
	}
	template <class T>
	auto serialize(OSerializer &ser, const T &obj, const VersionType version) -> decltype (obj.store(ser,version),void())
	{
	  obj.store(ser,m_version);
	}
    //
	// serialize interface without version information
    //
	template <class T>
	auto serialize(OSerializer &ser, T &obj, const VersionType version) -> decltype (obj.serialize(ser),void())
	{
	  obj.serialize(ser);
	}
	// note this overrides the const modifier because serialize is one function used for both const and non const
	template <class T>
	auto serialize(OSerializer &ser, const T &obj, const VersionType version) -> decltype (std::declval<T &>().serialize(ser),void())
	{
		const_cast<T *>(&obj)->serialize(ser);
	}
	template <class T>
	auto serialize(OSerializer &ser, const T &obj, const VersionType version) -> decltype (obj.store(ser),void())
	{
	  obj.store(ser);
	}
	//TODO: make collection/map iterators more general i.e for any iteratable/mapped type...
    // collection iterator
	template <class T>
	typename std::enable_if<is_vector<T>::value &&
	!is_shared_pointer<typename T::value_type >::value 	&&
	!is_unique_pointer<typename T::value_type >::value, void>::type
	serialize(OSerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::store(obj.size());
	  for(auto &val: obj)
		  ser & val;
	}
	template <class T>
	typename std::enable_if<is_vector<T>::value && is_shared_pointer<typename T::value_type >::value, void>::type
	serialize(OSerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::store(obj.size());
	  for(auto &val: obj)
		  ser & *val;
	}
	template <class T>
	typename std::enable_if<is_vector<T>::value && is_unique_pointer<typename T::value_type >::value, void>::type
	serialize(OSerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::store(obj.size());
	  for(auto &val: obj)
		  ser & *val;
	}

    // map iterator
	template <class T>
	typename std::enable_if<is_map<T>::value &&
	!is_shared_pointer<typename T::mapped_type >::value &&
	!is_unique_pointer<typename T::mapped_type >::value, void>::type
	serialize(OSerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::store(obj.size());
	  for(auto &pair: obj)
	  {
		  ser & (pair.first);
		  ser & (pair.second);
	  }
	}
	template <class T>
	typename std::enable_if<is_map<T>::value && is_shared_pointer<typename T::mapped_type >::value, void>::type
	serialize(OSerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::store(obj.size());
	  for(auto &pair: obj)
	  {
		  ser & (pair.first);
		  ser & *(pair.second);
	  }
	}
	template <class T>
	typename std::enable_if<is_map<T>::value && is_unique_pointer<typename T::mapped_type >::value, void>::type
	serialize(OSerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::store(obj.size());
	  for(auto &pair: obj)
	  {
		  ser & (pair.first);
		  ser & *(pair.second);
	  }
	}

    // is string
	template <class T>
	typename std::enable_if<is_string<T>::value, void>::type
	serialize(OSerializer &ser, const T &obj, const VersionType version)
	{
	  SerializerType::store(obj);
	}
    ////////////////////////////

	template<class T>
	typename std::enable_if<std::is_class<T>::value,OSerializer & >::type
	//auto
	operator <<(T &obj)
	{
	  serialize(*this,obj,m_version);
      return *this;
	}

	template<class T>
	typename std::enable_if<std::is_class<T>::value,OSerializer & >::type
	//auto
	operator <<(const T &obj)
	{
	  serialize(*this,obj,m_version);
      return *this;
	}

	template<class T>
	typename std::enable_if<!std::is_class<T>::value,OSerializer & >::type
	operator <<(const T &obj)
	{
	  SerializerType::store(obj);
      return *this;
	}

	// data
	const VersionType m_version;
};

//template<class Serializer>
//void serialize(Serializer &ser, const unsigned int version)
//{
////    ser & xxx;
//}

//#define SERIALIZER(x)

///////////////////////////
// predefined types
///////////////////////////
template<class StreamType,class VersionType=uint32_t>
using ITextSerializer = ISerializer<StreamType,SerializationType::Text,VersionType>;
template<class StreamType,class VersionType=uint32_t>
using IBinarySerializer = ISerializer<StreamType,SerializationType::Binary,VersionType>;
template<class StreamType,class VersionType=uint32_t>
using OTextSerializer = OSerializer<StreamType,SerializationType::Text,VersionType>;
template<class StreamType,class VersionType=uint32_t>
using OBinarySerializer = OSerializer<StreamType,SerializationType::Binary,VersionType>;


} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_SERIALIZER_H_ */
