#ifndef OSLC_LUL_CMDLINEOPTIONS_H
#define OSLC_LUL_CMDLINEOPTIONS_H

#include <map>
#include <sstream>
#include <string>

#include <oslc/lul/Log.h>

#include <oslc/lul/Option.h>

// needed for macro functions (concat, stringfy etc.)
#include <oslc/lul/Misc.h>

namespace oslc {
namespace lul {

struct CmdLineOption: OptionBase
{
	  enum OptionType
	  {
		  Unknown, // unknown type
		  ValueOpt, // an option with a value on the cmd line. example: -p value
		  BinaryOpt, // an option without a value, it either exist or it doesn't exist on the cmd line. example: -b
		  PlainVal  // not an option just a value on the cmd line
	  };

	  CmdLineOption(): optType(Unknown), isRequired(false), isAvailable(false)
	  {

	  }

	  CmdLineOption(std::string name,  std::string desc, OptionType optType, bool isRequired, bool isAvailable):
		  name(name),desc(desc),optType(optType),isRequired(isRequired),isAvailable(isAvailable)
	  {
	  }

	  // a predefined option is one that was defined/anticipated by the code, not an unknown or value option which was provided by user as a parameter
	  bool isPredefinedOption()
	  {
        return ( (optType==ValueOpt) || (optType==BinaryOpt) );
	  }

	  // data
	  std::string name;
	  std::string desc;
	  //std::string value;
	  OptionType optType;
	  bool isRequired;
	  bool isAvailable;
};

  class CmdLineOptions: public OptionContainer<CmdLineOption>
  {
  public:
	  //using namespace oslc::lul;

	  // default constructor
	  CmdLineOptions() {}

	  CmdLineOptions(std::string title, std::string desc): m_title(title), m_desc(desc)
	  {

	  }

	  bool init(std::string title, std::string desc)
	  {
		  m_title = title;
		  m_desc  = desc;
		  return true;
	  }

	  template<class T>
	  CmdLineOptions &addOption(std::string name, std::string desc, T defaultValue, CmdLineOption::OptionType optType, bool isRequired, bool isAvailable)
	  {
		  OptionBasePtr optBasePtr = (OptionBasePtr)new Option<T,CmdLineOption>(CmdLineOption(name,desc,optType,isRequired,isAvailable),defaultValue);
		  OptionContainer::addOption(name,optBasePtr);
		  return *this;
	  }

	  template<class T>
	  bool addOption2(std::string name, std::string desc, T defaultValue, CmdLineOption::OptionType optType, bool isRequired, bool isAvailable)
	  {
		  OptionBasePtr optBasePtr = (OptionBasePtr)new Option<T,CmdLineOption>(CmdLineOption(name,desc,optType,isRequired,isAvailable),defaultValue);
		  OptionContainer::addOption(name,optBasePtr);
		  return true;
	  }

	  //
	  // for now there could be only one type of separator char ('-') (that is enough to support the common '-'  and '--' options)
	  //
      #define OPTION_STR_SEP_CHAR '-'

	  bool isOptionStr(std::string optStr)
	  {
		  return optStr[0]==OPTION_STR_SEP_CHAR;
	  }

	  bool parse(int argc, char **argv)
	  {
		  addOption<std::string>("__ExecName__", "executable name", argv[0],CmdLineOption::OptionType::PlainVal,true,true);
		  int i=1;
		  while(i<argc)
		  {
			  // first check if its a value or option
			  if(!isOptionStr(argv[i]))
			  {
				std::stringstream strStream;
				strStream<<"__val"<<i;
				addOption<std::string>(strStream.str(),"plain value type",argv[i],CmdLineOption::OptionType::PlainVal,false,true);
			    i++;
			    continue;
			  }
		      std::string opt = argv[i];
			  if(!isValidOption(opt))
			  {
		    	  LOG_ERROR("Command line option not valid: "<<argv[i]);
				  return false;
			  }
			  // else
			  OptionBasePtr optData = m_options[opt];//this->operator [](opt);
			  // mark it as available
			  optData->isAvailable = true;
			  // inc index for next param/option
			  i++;
			  // check if its a value type, which means we now need to read the value
			  if(optData->optType==CmdLineOption::OptionType::ValueOpt)
			  {
				if(i<argc)
				{
				  if(!optData->setValueFromString(argv[i]))
				  {
			    	  LOG_ERROR("Bad command line option value: "<<argv[i]);
					  return false;
				  }
				  else
			        i++;
				}
				else //error
				{
		    	  LOG_ERROR("Missing command line option value: "<<argv[i]);
				  return false;
				}
			  }
		  }
		  // return success
		  //return true;
		  // check for missing cmdline options and return as final parse result
		  return isMissingCmdlineOpts();
	  }

	  bool isMissingCmdlineOpts()
	  {
		  for(auto opt: m_options)
		  {
			  OptionBasePtr optData = opt.second;
			  if(optData->isRequired && !optData->isAvailable)
			  {
				  LOG_ERROR("Missing command line option: "<<optData->desc);
				  return false;
			  }
		  }
		  return true;
	  }

	  OptionBasePtr operator[](std::string name)
	  {
		  return m_options[name];
	  }

	  static CmdLineOptions &getGlblCmdLineOptions()
	  {
		  static CmdLineOptions theOne;
		  return theOne;
	  }

	  void displayUsageMsg()
	  {
		  std::cout<<"--------"<<std::endl;
		  std::cout<<m_title<<std::endl;
		  std::cout<<"--------"<<std::endl;
		  std::cout<<m_desc<<std::endl;
		  std::cout<<"--------"<<std::endl;
		  std::cout<<"Options:"<<std::endl;
		  std::cout<<"--------"<<std::endl;

		  // first get groups of keys becasue an option can have multiple names (friendly name, shortcut etc.)
		  SynonymGroups sg;
		  getSynonymKeyGroups(sg);

		  for(SynonymGroups::iterator itr = sg.begin();itr!=sg.end();itr++)
		  {
		    OptionBasePtr opt = itr->first;//optBasePtrToCmdLinePtr(itr->first);
		    // if this is not a valid or predefined option
		    if(!opt || !opt->isPredefinedOption())
		    	continue;

		    std::vector<std::string> &altNameList = itr->second;
		    for(int i=0;i<altNameList.size()-1;i++)
			  std::cout<<"  "<<altNameList[i]<<",";

		    std::cout<<"  "<<altNameList[altNameList.size()-1]<<std::endl;
			std::cout<<"\t"<<opt->desc<<std::endl;
		  }
	  }

	  //
	  // utility to convert argc/argv pair to a string to recreate original command line
	  //
	  static std::string cmdLineToString(int argc, char **argv)
	  {
  		  std::stringstream strStream;
  		  for(int i=0;i<argc-1;i++)
  		    strStream<<argv[i]<<" ";
	      strStream<<argv[argc-1];
  		  return strStream.str();
	  }

  private:
	  // data
	  //OptionMap m_options;
	  std::string m_title;
	  std::string m_desc;
};

template<class T> using  CmdLineOptionPtr =  std::shared_ptr<oslc::lul::Option<T,oslc::lul::CmdLineOption> >;

#define INIT_CMD_LINE(title,desc) \
static volatile bool global_cmd_line_init = oslc::lul::CmdLineOptions::getGlblCmdLineOptions().init(title,desc)

#define REGISTER_CMDLINE_OPT(name, desc, type, defVal, optType) \
static volatile bool MACRO_CONCAT(global_cmd_line_option_,__COUNTER__) = oslc::lul::CmdLineOptions::getGlblCmdLineOptions().addOption2<type>(name,desc,defVal,optType,false,false)

// register a required cmdline option - raise error if missing
#define REGISTER_CMDLINE_OPT_REQ(name, desc, type, defVal, optType) \
static volatile bool MACRO_CONCAT(global_cmd_line_option_,__COUNTER__) = oslc::lul::CmdLineOptions::getGlblCmdLineOptions().addOption2<type>(name,desc,defVal,optType,true,false)


#define REGISTER_CMDLINE_SYNONYM(name, synonym) \
static volatile bool MACRO_CONCAT(global_cmd_line_option_synonym_,__COUNTER__) = oslc::lul::CmdLineOptions::getGlblCmdLineOptions().addOptionSynonym(name,synonym)


} /* namespace lul */
} /* namespace oslc */

#endif // OSLC_LUL_CMDLINEOPTIONS_H
