/*
 * TerminalUtil_test.cpp
 *
 *  Created on: Jan 14, 2019
 *      Author: admin
 */

//#include <cstdio>

#include <oslc/lul/TerminalUtil.h>

#include "oslc/lul/TestHarness.h"


namespace oslc {
namespace lul {


REGISTER_TEST("TerminalUtil","test 1")
{
  {
	  TermManip tm;
	  // reseting ICANON allows kbhit function to report every key press without waiting for enter key to be pressed
	  // reseting ECHO stops echoing keypresses to stdout
	  tm.resetFlags(ICANON | ECHO);
	  int res = tm.kbhit();
	  // TODO: fix kbhit for windows => fails on cygwin
	  TEST_EXP(res==0);
	  LOG_INFO("kbhit returned: "<<res);
	  if(res!=0)
	  {
		  LOG_WARN("res!=0 are we running on cygwin/windows?");
	  }
  }

}

REGISTER_TEST("TerminalUtil","test 2 - interactive")
{
//TODO: allow to selective run tests according to traits? interactive etc.
#ifdef INTERACTIVE_TEST
  {
	  TermManip tm;
	  // reseting ICANON allows kbhit function to report every key press without waiting for enter key to be pressed
	  // reseting ECHO stops echoing keypresses to stdout
	  tm.resetFlags(ICANON | ECHO);
	  int res = tm.kbhit();
	  TEST_EXP(res==0);
	  //ungetc('a',stdin);
	  while(!(res=tm.kbhit()))
	  {
		  //res = tm.kbhit();
		  LOG_INFO("Sleeping...");
		  sleep(3);
	  }
	  TEST_EXP(res>0);
	  LOG_INFO("res=="<<res);
	  while(res-->0)
	  {
		  int ch = getchar();
		  LOG_INFO("char=="<<(char)ch<<":"<<ch);
	  }
	  int ch = getchar();
	  LOG_INFO("post 1 -> char=="<<(char)ch<<":"<<ch);
  }
  int ch = getchar();
  LOG_INFO("post 2 -> char=="<<(char)ch<<":"<<ch);

//  while(true)
//  {
//	if(tm.kbhit()>0)
//      LOG_INFO("char=="<<(char)getchar());
//  }
  // test passphrase functionality
  LOG_INFO("Enter passwd:");
  //std::cout.flush();
  TermManip tm;
  std::string testPass;
  TEST_EXP(tm.getPassphrase(testPass));
  //std::cout<<std::endl;
  LOG_INFO("passwd is:"<<testPass);

#else
  LOG_INFO("Not running interactive test");
#endif //INTERACTIVE_TEST

}

} /* namespace lul */
} /* namespace oslc */
