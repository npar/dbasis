/*
 * PriorityQueue_test.cpp
 *
 *  Created on: Jul 7, 2018
 *      Author: admin
 */

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/PriorityQueue.h>

#include <oslc/lul/RandomNumGen.h>

namespace oslc {
namespace lul {

template<class T, class U>
void printQ(const PriorityQueue<T,U> &pq)
{
   for(auto i: pq.getContainer())
	   std::cout<<" "<<i;
   std::cout<<std::endl;
}

bool compare(int a, int b)
{
	return a>b;
}

REGISTER_TEST("PriorityQueue","test 1")
{
	PriorityQueue<int,bool (*)(int, int)> pq(5,&compare);
	IntRNG<int> rng;

	for(int i=0;i<20;i++)
	{
		int randNum = rng.rand(0,99);
		pq.enqueue(randNum);
		printQ(pq);
		if(i % 4 == 3)
			pq.dequeue();
	}

}


} /* namespace lul */
} /* namespace oslc */
