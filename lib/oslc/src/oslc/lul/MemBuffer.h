/*
 * MemBuffer.h
 *
 *  Created on: Jan 8, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_MEMBUFFER_H_
#define SRC_OSLC_LUL_MEMBUFFER_H_


#include <streambuf>
#include <istream>
// for memcpy
#include <cstring>

#include <oslc/lul/Log.h>

namespace oslc {
namespace lul {

//
// memory buffers for serialization etc.
//
// based on ideas from: https://stackoverflow.com/questions/13059091/creating-an-input-stream-from-constant-memory

class MemBuffer : public std::streambuf
{
public:
	// init with existing buffer
	MemBuffer(char* begin, char* end): m_isBuffOwner(false)
    {
		// set rw pointers
		init(begin, end);
	}
	// init with existing buffer
	MemBuffer(const int size): m_isBuffOwner(true)
    {
		// set rw pointers
		init(size);
	}
	~MemBuffer()
	{
		if(m_isBuffOwner)
			delete []m_buffPtr;
	}

	int underflow() override
	{
		LOG_INFO("MemBuffer underflow called");
        return std::streambuf::underflow();
	}

	int overflow (int c) override
	{
		LOG_INFO("MemBuffer overflow called: "<<c);
	    // if we are not owner we can not resize buffer - call default handler
	    if(!m_isBuffOwner)
		  return std::streambuf::overflow(c);

		// if c is eof, return not eof
	    if (c == traits_type::eof())
	      return traits_type::not_eof(c);

	    // resize the buffer to accommodate more data
		resize();

	    if (pptr() != epptr())
	    {
		  *pptr() = c;
		  pbump(1);
		  return c;
	    }
	    else
	      return traits_type::eof();
	}
	unsigned int buffSize()
	{
		//std::streambuf::in_avail();
		return m_buffSize;
	}
	// how much space is available to read
	unsigned int availInSize()
	{
		return egptr()-gptr();
	}
	// how much data is available to read
	unsigned int usedInSize()
	{
		return gptr()-eback();
	}
	// how much space is available to write
	unsigned int availOutSize()
	{
		return epptr()-pptr();
	}
	// how much data is available to write
	unsigned int usedOutSize()
	{
		return pptr()-pbase();
	}
	// distance between write and read pointers*
	// !!! note we have one buffer for read and write !!!
	//unsigned int availableToRead()
	unsigned int usedSpace()
	{
		return pptr()-gptr();
	}
	// distance between write pointer and eof buffer
	unsigned int writeSpaceAvail()
	{
		return availOutSize();
	}
	// distance between read pointer and eof buffer
	unsigned int readSpaceAvail()
	{
		return availInSize();
	}

	char *buff()
	{
		return m_buffPtr;
	}

	// !!! return 1 char after buffer
	char *eofBuff()
	{
	  return m_buffPtr + m_buffSize;
	}

	void resetWritePos()
	{
		// set write pointers
		setp(buff(),eofBuff());
	}

	void resetReadPos()
	{
		// set read pointers
		setg(buff(),buff(),eofBuff());
	}

	void resize(bool copyOldData=true, unsigned int newSize=0)
	{
		LOG_INFO("MemBuffer resize called: "<<copyOldData<<":"<<newSize);
		// get sizes/offsets
		unsigned int oldSize = buffSize();
		// by default double the space
		if(newSize==0)
		  newSize = oldSize * 2;
		// calc read offset position
		unsigned int oldReadPos = usedInSize();
		// save old buffer
		char* oldBuff = buff();
		// try to allocate new bigger buffer
		init(newSize);
		// copy data into buffer
		if(copyOldData)
		{
		  // if new buff is smaller copy new size otherwise copy old size
		  unsigned int copySize = std::min(oldSize,newSize);
		  std::memcpy(buff(), oldBuff, copySize);
		  //setp();
		  // set write pointer
		  pbump(copySize);
		}
		// set read pointers
		setg(buff(), buff() + oldReadPos, eofBuff());
		// delete old buffer
		delete[] oldBuff;
	}

private:
	void init(const int size)
	{
		char *buffPtr = new char[size];
		init(buffPtr,buffPtr + size);
	}
	void init(char* begin, char* end)
	{
		// init buff and size
		m_buffPtr = begin;
		m_buffSize = end - begin;
		// set RW pointers
		//setRWPos(begin, end);
		resetWritePos();
		resetReadPos();
	}

	// data
	bool m_isBuffOwner;
	char *m_buffPtr;
	size_t m_buffSize;
};

template<class T>
class MemStream : public T
{
  public:
	template<class... ARGS>
	MemStream(ARGS && ... args) : T(0), m_buff(std::forward<ARGS>(args)...)
    {
        T::rdbuf(&m_buff);
    }
	MemBuffer &getMemBuff() { return m_buff; }
	// data
  protected:
	MemBuffer m_buff;
};

using OMemStream  = MemStream<std::ostream>;
using IMemStream  = MemStream<std::istream>;
using IOMemStream = MemStream<std::iostream>;


} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_MEMBUFFER_H_ */
