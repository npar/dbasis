/*
 * DynLib.h
 *
 *  Created on: Aug 4, 2020
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_DYNLIB_H_
#define SRC_OSLC_LUL_DYNLIB_H_

#include <memory>
#include <string>
#include <dlfcn.h>

#include <oslc/lul/Log.h>
#include <oslc/lul/Util.h>
#include <oslc/lul/ObjectFactory.h>

namespace oslc {
namespace lul {

class IPlugin // interface
{
public:
	struct Info
	{
		std::string name;
		uint32_t version;
	};
	typedef std::shared_ptr<IPlugin> SPtr;

	IPlugin() {}
	IPlugin(std::string name, uint32_t version): m_info{name,version} {}
    virtual ~IPlugin() {}

    // overide this if need be by creating a new info derived type
    Info &getInfo() { return m_info; }
    // data
    Info m_info;
};

#define DYNLIB_DLL_EXPORTED_ATTRIB __attribute__((__visibility__("default")))

#define DYNLIB_PLUGIN_FACTORY_FUNC_NAME "createPlugin"

// declaration for exported dynamic linked functions
#define DYNLIB_DLL_EXPORTED_FUNC extern "C" DYNLIB_DLL_EXPORTED_ATTRIB

class DynLib
{
public:
	typedef void *DynLibHandle;
	typedef void *SymbolHandle;
	typedef SymbolHandle FuncHandle;

	DynLib()  { m_libHandle = nullptr; }

	static const int DEFAULT_OPEN_FLAGS = RTLD_LAZY | RTLD_LOCAL;

#define REPORT_DYNLIB_ERROR(handle) LOG_ERROR_IF("DynLib error: "<<dlerror(),!handle);

	// interface
	bool loadLibrary(const std::string &filename, int flags = DEFAULT_OPEN_FLAGS)
	{
		m_libHandle = dlopen(filename.c_str(), flags);
		REPORT_DYNLIB_ERROR(m_libHandle);
	    return m_libHandle!=0;
	}

	SymbolHandle getSymbolHandle(const std::string &symbName)
	{
		SymbolHandle sh = dlsym(m_libHandle, symbName.c_str());
		REPORT_DYNLIB_ERROR(sh);
		return sh;
	}

	template<class T>
	T getSymbolHandle(const std::string &symbName)
	{
		return reinterpret_cast<T>(getSymbolHandle(symbName.c_str()));
	}

	void unloadLibrary()
	{
		if(m_libHandle)
		{
			if(dlclose(m_libHandle)!=0)
			  REPORT_DYNLIB_ERROR(false);
			m_libHandle = nullptr;
		}
	}

	DynLibHandle getLibHandle() { return m_libHandle; }
/*
	std::string getCompiler()
	{
#if defined(__GNUC__)
	    return "g++";
#elif defined(__clang__)
	    return "clang++";
#else
	   LOG_ERROR("I do not know this C++ compiler and how to build the dll!");
	   return "";
#endif
	}

	std::string defCompileCmd()
	{
	   std::string cmd = getCompiler() + " --std=c++11 -c -I src -Wall -fPIC $SRC_FILE$";
       return cmd;
	}

	std::string defBuildCmd()
	{
		std::string cmd = getCompiler() + " --std=c++11 -shared $OBJ_FILE_LIST$ -fPIC -pthread -ldl -o $LIB_NAME$";
       return cmd;
	}

	bool compile(const std::string &fileName,const std::string &compileCmd)
	{
		// compile code file
		std::string cmd = compileCmd + " " + fileName;
		int res = execSysCmd(cmd.c_str());
		LOG_ERROR_IF("compiling src file/s",res);
		return !res;
	}

	bool buildLibrary(const std::string &cmdLine)
	{
		// copy code file
		LOG_ERROR_IF("compiling src file/s",execSysCmd("g++ --std=c++11 -c -I src -Wall -fPIC src/oslc/lul/DynLib_test.cpp -o Debug/DynLib_test.o"));
		LOG_ERROR_IF("compiling src file/s",execSysCmd("g++ --std=c++11 -c -I src -Wall -fPIC src/oslc/lul/DynLib.cpp -o Debug/DynLib.o"));
		// build dll
		std::string buildDllCmd;
	#if defined(__GNUC__)
		buildDllCmd = "g++ --std=c++11 -shared Debug/DynLib.o Debug/DynLib_test.o -fPIC -pthread -ldl -o Debug/dynlib-test.so";
	#elif defined(__clang__)
		buildDllCmd = "clang++ --std=c++11 -shared Debug/DynLib.o Debug/DynLib_test.o -fPIC -pthread -ldl -o Debug/dynlib-test.so";
	#else
		LOG_ERROR("I do not know this C++ compiler and how to build the dll!");
	#endif
		int res = execSysCmd(buildDllCmd.c_str());
		LOG_ERROR_IF("Unable to compile dll",res);
		TEST_EXP(!res);

       return false;
	}
*/
	~DynLib() { unloadLibrary(); }

protected:
	// data
	DynLibHandle m_libHandle;
};

template<class T>
class PluginFactory
{
public:
	typedef std::shared_ptr<T> PluginSPtr;
	PluginFactory(DynLib &dynLib): m_dynLib(dynLib) {}
	PluginSPtr loadPlugin(const std::string &pluginName);
	// data
	DynLib &m_dynLib;
};

//
// Note bug in g++ (< v7.0?) => https://gcc.gnu.org/bugzilla/show_bug.cgi?id=56480
// error: specialization of ‘template<class T> T* oslc::lul::PluginFactory<T>::loadPlugin(const string&)’ in different namespace [-fpermissive]
#if defined(__GNUC__)
  #if GCC_VERSION < 7
     #pragma message "Note template specialization bug in g++ (< v7.0?) => https://gcc.gnu.org/bugzilla/show_bug.cgi?id=56480"
  #endif
#endif


// define plugin factory
#define DYNLIB_DEFINE_PLUGIN(baseType,type,name)\
REGISTER_OBJ_TYPE(baseType,type,name);\
template<>\
oslc::lul::PluginFactory<baseType>::PluginSPtr oslc::lul::PluginFactory<baseType>::loadPlugin(const std::string &pluginName)\
{\
	typedef baseType *(*PluginFactoryFuncPtr)(const std::string &);\
	if(!m_dynLib.getLibHandle())\
		return nullptr;\
	oslc::lul::DynLib::FuncHandle factory_function = m_dynLib.getSymbolHandle(STRINGIFY(MACRO_CONCAT(createPlugin,baseType)));\
	if(!factory_function)\
		return nullptr;\
	PluginSPtr sptr(reinterpret_cast<PluginFactoryFuncPtr>(factory_function)(pluginName));\
	return sptr;\
}


#define DYNLIB_PLUGIN_FACTORY(baseType) extern "C" DYNLIB_DLL_EXPORTED_ATTRIB baseType *MACRO_CONCAT(createPlugin,baseType)(const std::string &name) { return oslc::lul::ObjectFactory<baseType>::get()->create(name); }



} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_DYNLIB_H_ */
