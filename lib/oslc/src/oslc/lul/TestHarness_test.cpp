/*
 * TestHarness_test.cpp
 *
 *  Created on: Feb 15, 2018
 *      Author: admin
 */

#include "TestHarness.h"
#include <sstream>
#include <iostream>
//#include <memory>

namespace oslc {
namespace lul {


//template<typename T>
//struct lval { T t; T &getlval() { return t; } };


//#define GEN_STR_BASE(tempStrm,s,f)
//  std::stringstream tempStrm;
//  tempStrm<<s;
//  f(tempStrm.str())
//
//#define GEN_STR_CALL_FUNC(s,f) GEN_STR_BASE(MACRO_CONCAT(TEST_BASE_NAME,__COUNTER__),s,f)


REGISTER_TEST("TestHarness","test 1")
{
  LOG_TEST_MSG("Logging a test message");
  std::string testStr = "abcd";
  std::stringstream tempStrm;
  tempStrm<<"Using str: "<<testStr<<12345;
  LOG_TEST_MSG(tempStrm.str());
  //LOG_TEST_MSG(GEN_STR("abc"<<"def"<<345));
  GEN_STR_CALL_FUNC("Gen test "<<" def "<<345,LOG_TEST_MSG);
  LOG_TEST_MSG("abc "<<"def "<<345);
//  auto a = (static_cast<std::ostringstream&>(
//    lval<std::ostringstream>().getlval() << "Value: " << 5).str().c_str());
  int num = 10;
  TEST_EXP(num==10);

}

REGISTER_TEST("TestHarness","test 2")
{
  LOG_TEST_MSG("Logging a test message");
  int num = 2;
  TEST_EXP(num==2);

}

template<class T>
struct XYZ
{
	//virtual int test()
	virtual void test()
	{
	   abc++;
       //return 0;
	}

	T abc;
};

//class TestFuncBase
//{
//public:
//	virtual int operator()(TestHarness::TestInfo *tiPtr) = 0;
//};



//template <class T, class U>
//class TestFunc: public TestFuncBase<T>
//{
//public:
//	std::function<T *, int (U *)> stdFMem;
//};
//
//template <class T>
//class TestFunc: public TestFuncBase<T>
//{
//public:
//	std::function<int (T *)> stdFMem;
//};

//template <class T, class U>
//class TestMemberFunc: public TestFuncBase<U>
//{
//public:
//	std::function<T *, int (U *)> stdFMem;
//};

//class TestFuncBase
//{
//public:
//	typedef std::unique_ptr<TestFuncBase> UPtr;
//	virtual int runTest(TestHarness::TestInfo *tiPtr) const = 0;
//};
//
//class TestFunc: public TestFuncBase
//{
//public:
//	typedef int (*TestFunction)(TestHarness::TestInfo *);//(TestHarness *context);
//			//typedef int (*::TestMemberFunction)(TestInfo *);//(TestHarness *context);
//
//	TestFunc(TestFunction tf): m_tf(tf) {}
//
//	//operator TestFuncBase *() { return this; }
//
//	virtual int runTest(TestHarness::TestInfo *tiPtr) const
//	{
//		return m_tf(tiPtr);
//	}
//
//private:
//	// data
//	TestFunction m_tf;
//};
//
//template<class T>
//class TestMemberFunc: public TestFuncBase
//{
//public:
//	typedef int (T::*TestFunction)(TestHarness::TestInfo *);//(TestHarness *context);
//
//	TestMemberFunc(TestFunction tf): m_tf(tf) {}
//
//	virtual int runTest(TestHarness::TestInfo *tiPtr) const
//	{
//		T var;
//		return (var.*m_tf)(tiPtr);
//	}
//
//private:
//	// data
//	TestFunction m_tf;
//};
//
//template <class T>
//TestFuncBase::UPtr makeUniqueTestPtr(T data)
//{
//  return std::make_unique<T>(data);
//}

//int testFunc(TestHarness::TestInfo *tiPtr)
void testFunc(TestHarness::TestInfo *tiPtr)
{
    LOG_INFO("in testFunc");
	//return 1;
}

struct TestClass
{
  //int testFunc(TestHarness::TestInfo *tiPtr)
  void testFunc(TestHarness::TestInfo *tiPtr)
  {
	  LOG_INFO("in TestClass::testFunc;");
	  //return 2;
  }

  int x;
};

//int testCallFunc(TestFuncBase *fbPtr)
//{
//	return fbPtr->runTest(0);
//}
//
//int testCallFunc2(const TestFuncBase &fbPtr)
//{
//	//return fbPtr.runTest(0);
//	return 0;
//}
//
//int testCallFunc3(TestFuncBase::SPtr ubPtr)
//{
//	return ubPtr->runTest(0);
//}

REGISTER_TEST("TestHarness","test 3")
{
  LOG_TEST_MSG("Logging a test message");

    XYZ<int> xyz;
    xyz.test();
  //TestHarness::TestMemberFunction tmf
  //TestHarness::TestMemberFunction
  //std::function< int (TestHarness::TestInfo *) > func = &TestClass::testFunc;
//  TestFuncBase *fbPtr;
//  TestFunc tf(&testFunc);
//  TestMemberFunc<TestClass> tfm(&TestClass::testFunc);
    TestClass tc;
    tc.x = 5;
    //TestHarness::TestMemberFunction
	//std::function<int (TestHarness::TestInfo *)> stdFMem = std::bind(&TestClass::testFunc,&tc,std::placeholders::_1);
	std::function<void (TestHarness::TestInfo *)> stdFMem = std::bind(&TestClass::testFunc,&tc,std::placeholders::_1);
	stdFMem(0);
	//stdFMem = stdFMem1;

	//std::function<int (TestClass *, TestHarness::TestInfo *)> stdFMem2 = std::bind(&TestClass::testFunc,std::placeholders::_1,std::placeholders::_2);
	std::function<void (TestClass *, TestHarness::TestInfo *)> stdFMem2 = std::bind(&TestClass::testFunc,std::placeholders::_1,std::placeholders::_2);
	stdFMem2(&tc,0);

	//std::function<int (TestHarness::TestInfo *)> stdF = std::bind(&testFunc,std::placeholders::_1);
	std::function<void (TestHarness::TestInfo *)> stdF = std::bind(&testFunc,std::placeholders::_1);
	stdF(0);


	TestHarness::TestHarnessFuncBase::SPtr tfbPtr = std::make_shared<TestHarness::TestHarnessFunc>(&testFunc);////std::make_unique<TestHarnessFunc>(testFunc);
	tfbPtr->runTest(0);
	TestHarness::TestHarnessFuncBase::SPtr tfbPtr2 = std::make_shared<TestHarness::TestHarnessClassFunc<TestClass>>(&TestClass::testFunc);//std::make_unique<TestHarnessClassFunc<TestClass> >(&TestClass::testFunc);
	tfbPtr2->runTest(0);


//
//  fbPtr = &tf;
//  int res = fbPtr->runTest(0);
//  fbPtr = &tfm;
//  res = fbPtr->runTest(0);
//
//  res = testCallFunc(&tf);
//  res = testCallFunc(&tfm);
//
//  res = testCallFunc2(TestFunc(&testFunc));
//
//  TestFuncBase::SPtr ubptr = std::make_unique<TestFunc>(&testFunc);
//  ubptr->runTest(0);
//  TestFuncBase::SPtr ubptr2 = std::move(ubptr);
//
//  TestFuncBase::SPtr ubptr3 =  std::make_unique<TestMemberFunc<TestClass>>(&TestClass::testFunc);

  int num = 2;
  TEST_EXP(num==2);
}


REGISTER_TEST("TestHarness","test 4")
{
  int num = 10;
  TEST_EXP(num==10);
  // test on error user messages
  ON_ERROR_MSG("value of "<<num<< "is bad");
  num = 20;
  TEST_EXP(num==10);
  // test on error user messages
  ON_ERROR_MSG("value of "<<num<< " is bad");
  // reset error if failed correctly
  if(tiPtr->lastErrorLine==247)
  {
	  TEST_SET_PASS
  }

}

REGISTER_TEST("TestHarness","test 5")
{
  int num = 10;
  // test exp + error form
  TEST_EXP2(num==20,"value of "<<num<< " is bad");
  // reset error if failed correctly
  if(tiPtr->lastErrorLine==262)
  {
	  TEST_SET_PASS
  }
}

class TestFixtureClass
{
public:
	TestFixtureClass() : val(0)
    {
	  LOG_TEST_MSG("Constructor of TestFixtureClass called");
    }

	int func()
	{
	  	return val + 2;
	}
	~TestFixtureClass()
	{
		  LOG_TEST_MSG("Destructor of TestFixtureClass called");
	}
protected:
	int val;
};


REGISTER_TEST_CLASS(TestFixtureClass,"TestHarness","test fixture 1")
{
	val++;
	TEST_EXP(val==1);
	TEST_EXP(func()==3);
}

REGISTER_TEST_FIXTURE(TestFixtureClass,"TestHarness","test fixture 2")
{
	TEST_EXP(val==0);
	TEST_EXP(func()==2);
}



} /* namespace lul */
} /* namespace oslc */
