/*
 * TerminalUtil.h
 *
 *  Created on: Jan 14, 2019
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_TERMINALUTIL_H_
#define SRC_OSLC_LUL_TERMINALUTIL_H_

#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <string>

// Error on cygwin windows =>  error: ‘FIONREAD’ was not declared in this scope
#ifndef FIONREAD
  //#include <sys/filio.h>
  #define FIONREAD TIOCINQ
#endif

namespace oslc {
namespace lul {

class TerminalUtil
{
public:
	TerminalUtil();
	~TerminalUtil();
};

class TermManip
{
public:
  TermManip(int fileDesc=STDIN_FILENO): m_fileDesc(fileDesc) //tcflag_t flags
  {
     tcgetattr(m_fileDesc, &m_prevTermios);
     m_currTerminos = m_prevTermios;
     //setFlags(flags);
  }

  int setFlags(tcflag_t flags, int when=TCSANOW)
  {
	  m_currTerminos.c_lflag |= flags;
	  return tcsetattr(m_fileDesc, when, &m_currTerminos);
  }

  int resetFlags(tcflag_t flags, int when=TCSANOW)
  {
	  m_currTerminos.c_lflag &= ~flags;
	  return tcsetattr(m_fileDesc, when, &m_currTerminos);
  }

  int reset(int when=TCSANOW)
  {
	    return tcsetattr(m_fileDesc, when, &m_prevTermios);
  }

  // TODO: fix kbhit for windows => fails on cygwin
  int kbhit()
  {
	    int byteswaiting;
	    ioctl(m_fileDesc, FIONREAD, &byteswaiting);
	    return byteswaiting;
  }

  // inspired by: https://www.gnu.org/savannah-checkouts/gnu/libc/manual/html_node/getpass.html
  // Note: check errno if function returns false
  bool getPassphrase(std::string &pass,size_t MaxPassSize=64)
  {
	  // clear in case there is some residual value in there
	  pass.clear();
	  // reseting ECHO stops echoing key presses to stdout
	  // fail if we can't reset echo
	  if(resetFlags(ECHO,TCSAFLUSH)!=0)
		  return false;
	  // read the input
	  pass.resize(MaxPassSize,'X');
	  size_t readCount = read(m_fileDesc,(char *)pass.data(),MaxPassSize);
	  if(readCount>0)
		  pass.resize(readCount-1);
	  else
		  pass = "";
	  // restore flags and return success/failure
	  return reset(TCSAFLUSH)==0;
  }

  ~TermManip()
  {
	  reset();
  }

protected:
  // data
  termios m_prevTermios;
  termios m_currTerminos;
  int m_fileDesc;


};

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_TERMINALUTIL_H_ */
