/*
 * WorkerFactory_test.cpp
 *
 *  Created on: Sep 19, 2018
 *      Author: admin
 */

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/WorkerFactory.h>

#include <mutex>
#include <oslc/mt/MTSafeObject.h>

namespace oslc {
namespace lul {

//typedef WorkerFactory<int,int> WorkerTestFactory;

class WorkerBase
{
public:
	virtual void init(int &c) {}
	virtual void doWork(const int &io)
	{
		LOG_TEST_MSG("doing some const work on "<<io);
	}
	virtual void doWork(int &io)
	{
		LOG_TEST_MSG("doing some non-const work on "<<io);
	}
	virtual ~WorkerBase() {}
};

DECLARE_WORKER_FACTORY(WorkerTestFactory,WorkerBase);


class WorkerX: public WorkerBase//  public WorkerTestFactory::Worker //public WorkerFactoryWorker<int,int> // public WorkerFactory<int,int>::Worker
{
	virtual void init(int &c) {}
	virtual void doWork(const int &io)
	{
		LOG_TEST_MSG("doing some const work on "<<io);
	}
	virtual void doWork(int &io)
	{
		LOG_TEST_MSG("doing some non-const work on "<<io);
	}
	virtual ~WorkerX() {}
};

//typedef WorkerFactory<WorkerBase> WorkerTestFactory;

#define WorkerXSTR "WorkerX"

//REGISTER_OBJ_TYPE(WorkerTestFactory::Worker,WorkerX,WorkerXSTR);

REGISTER_FACTORY_WORKER_TYPE(WorkerTestFactory,WorkerX,WorkerXSTR);

//////////////////////////////////////////


//DECLARE_WORKER_FACTORY(WorkerTestFactory2,int,int);
//
//
class WorkerY:  public WorkerBase//WorkerTestFactory2::Worker
{
	virtual void init(int &c) {}
	virtual void doWork(const int &io)
	{
		LOG_TEST_MSG("doing some const work on "<<io);
	}
	virtual void doWork(int &io)
	{
		LOG_TEST_MSG("doing some non-const work on "<<io);
	}
	virtual ~WorkerY() {}
};

#define WorkerYSTR "WorkerY"

//REGISTER_FACTORY_WORKER_TYPE(WorkerTestFactory2,WorkerY,WorkerYSTR);

REGISTER_FACTORY_WORKER_TYPE(WorkerTestFactory,WorkerY,WorkerYSTR);

//////////////////////////////////////////

typedef mt::MTSafeObject<WorkerTestFactory,std::mutex> MTWorkerTestFactory;

//DECLARE_WORKER_FACTORY(WorkerTestFactory3,int,int);
//
//
//class WorkerY:  public WorkerTestFactory2::Worker
//{
//	virtual void init(int &c) {}
//	virtual void doWork(const int &io)
//	{
//		LOG_TEST_MSG("doing some const work on "<<io);
//	}
//	virtual void doWork(int &io)
//	{
//		LOG_TEST_MSG("doing some non-const work on "<<io);
//	}
//	virtual ~WorkerY() {}
//};
//
//#define WorkerYSTR "WorkerY"
//
//
//REGISTER_FACTORY_WORKER_TYPE(WorkerTestFactory2,WorkerY,WorkerYSTR);


REGISTER_TEST("worker factory","test 1")
{
  // works outside the class or with specific types but not inside ... see deleted registerWorker function
  //auto x =ObjectFactory<WorkerFactory<int,int>::Worker>::create<WorkerX>();
  WorkerTestFactory wtf;
  wtf.addWorker(WorkerXSTR);
  wtf.addWorker(WorkerYSTR);

  WorkerTestFactory::WorkerPtr wPtr = wtf.getWorker(WorkerXSTR);
  TEST_EXP(wPtr!=0);
  wPtr->doWork(3);

  wPtr = wtf.getWorker(WorkerYSTR);
  TEST_EXP(wPtr!=0);
  int x =4;
  wPtr->doWork(x);

  // add same worker twice - two workers of the same type
  wtf.addWorker(WorkerYSTR);
  wtf.getWorkers(WorkerYSTR);

  size_t workerCount = wtf.getWorkersCount(WorkerXSTR);
  TEST_EXP2(workerCount==1,"should be "<<workerCount);

  workerCount = wtf.getWorkersCount(WorkerYSTR);
  TEST_EXP2(workerCount==2,"should be "<<workerCount);

  WorkerTestFactory::WorkerMapRange range = wtf.getWorkers(WorkerYSTR);
  for(WorkerTestFactory::WorkerMapIterator itr = range.first; itr!=range.second;itr++)
	 itr->second->doWork(1);
  //range.first->second->doWork(1);
  //range.second->second->doWork(1);
  TEST_EXP2(workerCount==2,"should be "<<workerCount);


  MTWorkerTestFactory mtWtf;
  mtWtf.getLock().lock();
  mtWtf.getObj().addWorker(WorkerYSTR);
  mtWtf.getObj().getWorker(WorkerYSTR)->doWork(3);
  workerCount = mtWtf.getObj().getWorkersCount(WorkerYSTR);
  TEST_EXP2(workerCount==1,"should be "<<workerCount);
  mtWtf.getLock().unlock();


  //  WorkerTestFactory2 wtf2;
//  wtf2.addWorker(WorkerYSTR);
//  WorkerTestFactory::WorkerPtr wPtr2 = wtf2.getWorker(WorkerYSTR);
//  TEST_EXP(wPtr2!=0);
//  wPtr2->doWork(3);
//  int var = 7;
//  wPtr2->doWork(var);

}


REGISTER_TEST("worker factory","test 2")
{

  WorkerTestFactory wtf;
  //WorkerTestFactory::WorkerObjFactoryMap &objMap = wtf.getRegisteredWorkerTypes();
  for(auto wType: wtf.getRegisteredWorkerTypes())
  {
	  wtf.addWorker(wType.first);
  }

  int i = 0;
  for(auto wItr: wtf.getAllWorkers())
  {
	  wItr.second->init(i);
	  wItr.second->doWork(i);
	  i++;
  }


  WorkerTestFactory::WorkerPtr wPtr = wtf.getWorker(WorkerXSTR);
  TEST_EXP(wPtr!=0);
  wPtr->doWork(3);

}

} /* namespace lul */
} /* namespace oslc */
