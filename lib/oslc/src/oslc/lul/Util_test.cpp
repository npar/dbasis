/*
 * Util_test.cpp
 *
 *  Created on: Sep 26, 2018
 *      Author: admin
 */

#include <vector>

#include <oslc/lul/Util.h>
#include <oslc/lul/TestHarness.h>


namespace oslc {
namespace lul {

REGISTER_TEST("Util","test 1")
{

	typedef std::vector<std::string> StringVector;
	const int N = 5;
	StringVector strVec;
	for(int i=0;i<N;i++)
	{
	  std::string un = genUniqueName();
	  LOG_INFO("Unique name "<<i<<" = "<<un);
	  StringVector::iterator itr = std::find(strVec.begin(),strVec.end(),un);
	  TEST_EXP2(itr==strVec.end(),*itr<<":"<<un);
	  strVec.push_back(un);
	}
}

REGISTER_TEST("Util","test 2")
{
    execSysCmd("ls -l");
}

REGISTER_TEST("Util","test 3")
{
    //execSysCmd("ls -l");
	std::string src = "Bla $r$ bla $r$ foo $r$ bar $r$";
	std::string r = "abc";
	TEST_EXP(replaceStr(src,"$r$",r));
}


} /* namespace lul */
} /* namespace oslc */
