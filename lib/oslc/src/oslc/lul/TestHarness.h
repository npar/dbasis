/*
 * TestHarness.h
 *
 *  Created on: Feb 15, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_TESTHARNESS_H_
#define SRC_OSLC_LUL_TESTHARNESS_H_

#include <string>
#include <map>
#include <functional>

// needed for macro functions (concat, stringfy etc.)
#include <oslc/lul/Misc.h>
#include <oslc/lul/Log.h>

#include <oslc/lul/TextFX.h>

#include <oslc/lul/StringParser.h>

#ifdef USE_SIG_HANDLER
// handle signals
//#include <csignal>
//#include <signal.h>
#include <oslc/lul/SignalHandler.h>
#endif //USE_SIG_HANDLER

namespace oslc {
namespace lul {


template<class T>
class TestFuncBase
{
public:
	typedef std::shared_ptr<TestFuncBase> SPtr;
	virtual void runTest(T *tiPtr) const = 0;
	virtual ~TestFuncBase() {}
};

template <class T>
class TestFunc: public TestFuncBase<T>
{
public:
	typedef  void (*FPtr)(T *);
	TestFunc(FPtr fptr): stdFMem(fptr) {}
	virtual void runTest(T *tiPtr) const
	{
		stdFMem(tiPtr);
	}
private:
	std::function<void (T *)> stdFMem;
};

template <class U, class T>
class TestClassFunc: public TestFuncBase<T>
{
public:
	typedef void (U::*FPtr)(T *);
	TestClassFunc(FPtr fptr): stdFMem(fptr) {}
	virtual void runTest(T *tiPtr) const
	{
		U temp;
		stdFMem(&temp,tiPtr);
	}
private:
	std::function<void (U *, T *)> stdFMem;
};

class TestHarness
{
public:
	struct TestInfo;
	typedef TestFuncBase<TestInfo> TestHarnessFuncBase;
	using TestHarnessFunc = TestFunc<TestInfo>;

	template<class C>
	using TestHarnessClassFunc = TestClassFunc<C,TestInfo>;
	struct TestInfo
	{
		enum Status { Pass, Fail, Warn, Unknown };
		std::string type;
		std::string name;
		const char *file;
		int line;
		TestHarnessFuncBase::SPtr func;
		Status status;
		int lastErrorLine;
	};

	typedef std::multimap<std::string,TestInfo> TestListType;

private:
	TestHarness() {}

	void handleExcpetion(TestInfo& ti,const char *errorStr)
	{
		if(errorStr)
		{
		  LOG_LOC(ti.file, ti.line, MsgLog::Info,
				txtFX::red<<"**** "<< absTestName(ti) << " Raised Exception: "<<errorStr<< " ****"<<txtFX::reset);
		}
		else
		{
		  LOG_LOC(ti.file, ti.line, MsgLog::Info,
				txtFX::red<<"**** "<< absTestName(ti) << " Raised Unknown Exception ****"<<txtFX::reset);
		}

		// TODO: find a better mechanism maybe point to last known good line? or add excpetion flag in final report?
		// set last error line to valid position.
		ti.lastErrorLine = ti.line;
		ti.status = TestInfo::Fail;
	}

	void runTest(TestInfo& ti)
	{
		LOG_LOC(ti.file, ti.line, MsgLog::Info,
				"**** Running: " << absTestName(ti) << " ****");
		// catch any in-test exceptions
		try
		{
		  ti.func->runTest(&ti);
		}
		// if we caught an unhandled exception, report it
		catch(std::exception &e)
		{
			handleExcpetion(ti,e.what());
		}
		catch(...)
		{
			handleExcpetion(ti,nullptr);
			//throw; should we abort ???
		}
		if (ti.status == TestInfo::Fail) {
			LOG_LOC(ti.file, ti.line, MsgLog::Info,
					txtFX::red<<"**** "<< absTestName(ti) << " Failed ****"<<txtFX::reset);
			m_stats.failed++;
		} else {
			LOG_LOC(ti.file, ti.line, MsgLog::Info,
					"**** "<<absTestName(ti) << " Passed ****");
			m_stats.passed++;
		}
	}

public:
	static TestHarness &getTestHarness()
	{
	  static TestHarness theOne;
	  return theOne;
	}

	std::string absTestName(TestInfo &ti) { return  ti.type + " - " + ti.name; }
	//
	// register test function
	// Notes:
	//   Functions of the same type will be executed in order of registration*
	//   This can be used to allow setup/tear-down functionality for test groups.
	//   for example: The first function in a group can be used as setup and the last as teardown
	//   * https://en.cppreference.com/w/cpp/container/multimap - The order of the key-value pairs whose keys compare equivalent is the order of insertion and does not change. (since C++11)
	//   TODO: consider adding the standard test fixtures that exist in other frameworks i.e. class with constructor/destructor to do setup and teardown
	bool registerTest(std::string type, std::string name, TestHarnessFuncBase::SPtr func, const char *file = __FILE__, int line = __LINE__)
	{
		TestInfo ti = {type,name,file,line,func,TestInfo::Unknown};
		m_testList.insert(std::pair<std::string,TestInfo>(type,ti));
		return true;
	}

	// misc test stats
	struct Stats
	{
		Stats() { reset(); }
		void reset() { failed = 0; passed = 0; }
		// data
		int failed;
		int passed;
	};
	Stats m_stats;
	Stats &getStats() { return m_stats; }

	void displayStatsSummary()
	{
//		LOG_INFO("Tests Complete");
//		//LOG_INFO("Tests Results:");
//		LOG_INFO(m_stats.failed << " Tests Failed");
//		LOG_INFO(m_stats.passed << " Tests Passed");
		//TODO: add test run time use chrono...
		std::cout<<"************************"<<std::endl;
		std::cout<<"**** Tests Summary *****"<<std::endl;
		std::cout<<"************************"<<std::endl;
		int totalTestsRan = m_stats.passed + m_stats.failed;
		if(totalTestsRan>0)
		  std::cout<<"* "<<totalTestsRan<<" Tests Complete"<<std::endl;
		else
		  std::cout<<"* "<<txtFX::yellow<<totalTestsRan<<" Tests Complete"<<txtFX::reset<<std::endl;
		std::cout<<"************************"<<std::endl;
		if(totalTestsRan==0)
			return;
		if(m_stats.failed)
		  std::cout<<"*"<<txtFX::red<<" Tests Failed: "<<m_stats.failed<<txtFX::reset<<std::endl;
		else
		  std::cout<<"*"<<txtFX::green<<" Tests Failed: "<<m_stats.failed<<txtFX::reset<<std::endl;
		std::cout<<"************************"<<std::endl;
		std::cout<<"*"<<txtFX::green<<" Tests Passed: "<<m_stats.passed<<txtFX::reset<<std::endl;
		std::cout<<"************************"<<std::endl;

		if(m_stats.failed)
		  displayErrors();
	}

	void displayErrors()
	{
		std::cout<<"************************"<<std::endl;
		std::cout<<"**** List of Errors ****"<<std::endl;
		std::cout<<"************************"<<std::endl;

		int counter = 0;
        for(auto testItr : m_testList)
        {
          TestInfo &ti =  testItr.second;
		  if (ti.status == TestInfo::Fail)
		  {
			std::cout<<"["<<++counter<<"] "<<ti.file<<":"<<ti.lastErrorLine<<": "<<absTestName(ti) << " Failed" << std::endl;
		  }
		}
		std::cout<<"************************"<<std::endl;

	}

	int runTests(const std::string &testPathSpec = "")
	{
		  std::cout<<"Tests spec: "<<testPathSpec<<std::endl;
		  std::vector<std::string> pathElements = StringParser::getTokenList(testPathSpec,"/");
		  //for(auto element : pathElements)
		  //RUN_ALL_TESTS;
		  // check if asked to run all tests
		  if(pathElements.empty() || pathElements[0]=="*")
			  return runTests("","");
		  else
		  {
			  std::string testType = pathElements[0];
			  std::string testName;
		      if(pathElements.size()>1)
		    	  testName = pathElements[1];
		      // run tests
			  return runTests(testType,testName);
		  }
	}


	//
	// Run tests  using type and name as a guide for coverage
	// Notes:
	//   if type == "" -> run all tests
	//   if type == group -> run all group tests
	//   if name == test name -> run a specific group/name test

	int runTests(const std::string &type, const std::string &name)
	{
#ifdef USE_SIG_HANDLER
		SignalHandler sigHandler;
		sigHandler.set(SIGABRT).set(SIGFPE).set(SIGSEGV);
#endif
		m_stats.reset();
		//for(auto testItr : m_testList)
		TestListType::iterator testItr, lowerBound, upperBound;
		// if we have a group type requested
		if(!type.empty())
		{
		  //  testItr = m_testList.find(type);
		  lowerBound = m_testList.lower_bound(type);
		  upperBound = m_testList.upper_bound(type);
		}
		else
		{
		  lowerBound = m_testList.begin();
		  upperBound = m_testList.end();
		}

		//auto testItr : m_testList
		//std::pair <TestListType::iterator, TestListType::iterator> itrTestPair = m_testList.equal_range(type);

		for(testItr =lowerBound; testItr!=upperBound; testItr++)
		{

            TestInfo &ti =  testItr->second;

            // check for name filtering
            if(!name.empty() && name!=ti.name)
            	continue;
            // run the test
			runTest(ti);
#ifdef USE_SIG_HANDLER
			volatile int retVal = sigHandler.retPoint();
			if(retVal==0)
			ti.func();
			else
			{
				LOG_LOC(ti.file,ti.line,MsgLog::Info,"System exception "<<retVal<<" raised in test: " << ti.name);
			}
#else
#endif //USE_SIG_HANDLER

		}
		// display stats
		displayStatsSummary();
		return m_stats.failed;
	}

	std::ostream &log(MsgLog::Type type, const char *fileName = __FILE__, int lineNum = __LINE__)
	{
	    static MsgLog testLog(std::clog,MsgLog::Info);
	    return testLog.log(fileName,lineNum,type);
	}

	~TestHarness() {}

	// data
	TestListType m_testList;
};


#define TEST_BASE_NAME test_harness_test_auto_

/*
#define REGISTER_TEST_BASE(uniqueName,type,name) \
static int uniqueName(oslc::lul::TestHarness::TestInfo *); \
static volatile bool MACRO_CONCAT(TEST_BASE_NAME,__COUNTER__) = oslc::lul::TestHarness::getTestHarness().registerTest(type,name,std::make_shared<oslc::lul::TestHarness::TestHarnessFunc>(&uniqueName),__FILE__,__LINE__); \
static int uniqueName(oslc::lul::TestHarness::TestInfo *tiPtr)
*/

#define REGISTER_TEST_BASE(uniqueName,type,name) \
static void uniqueName(oslc::lul::TestHarness::TestInfo *); \
static volatile bool MACRO_CONCAT(TEST_BASE_NAME,__COUNTER__) = oslc::lul::TestHarness::getTestHarness().registerTest(type,name,std::make_shared<oslc::lul::TestHarness::TestHarnessFunc>(&uniqueName),__FILE__,__LINE__); \
static void uniqueName(oslc::lul::TestHarness::TestInfo *tiPtr)

//	static volitile bool MACRO_CONCAT(global_test_func_,__COUNTER__) = oslc::lul::TestHarness::getTestHarness().registerTest(type,name,func);

#define REGISTER_TEST(type,name) REGISTER_TEST_BASE(MACRO_CONCAT(TEST_BASE_NAME,__COUNTER__),type,name)

#define REGISTER_TEST_FIXTURE_BASE(className,uniqueName,type,name) \
struct uniqueName: public className \
{ \
  void testFunc(oslc::lul::TestHarness::TestInfo *tiPtr); \
}; \
static volatile bool MACRO_CONCAT(TEST_BASE_NAME,__COUNTER__) = oslc::lul::TestHarness::getTestHarness().registerTest(type,name,std::make_shared<TestHarness::TestHarnessClassFunc<uniqueName>>(&uniqueName::testFunc),__FILE__,__LINE__); \
void uniqueName::testFunc(oslc::lul::TestHarness::TestInfo *tiPtr)

#define REGISTER_TEST_FIXTURE(className,type,name) REGISTER_TEST_FIXTURE_BASE(className,MACRO_CONCAT(TEST_BASE_NAME,__COUNTER__),type,name)

#define REGISTER_TEST_CLASS(className,type,name) REGISTER_TEST_FIXTURE(className,type,name)


#define LOG_TEST_MSG(msg) oslc::lul::TestHarness::getTestHarness().log(oslc::lul::MsgLog::Info, __FILE__,__LINE__)<<msg<<"\n"

#define LOG_TEST_ERR(msg) oslc::lul::TestHarness::getTestHarness().log(oslc::lul::MsgLog::Error, __FILE__,__LINE__)<<oslc::lul::txtFX::red<<msg<<oslc::lul::txtFX::reset<<"\n"


#define TEST_SET_FAIL tiPtr->status = oslc::lul::TestHarness::TestInfo::Fail; tiPtr->lastErrorLine = __LINE__;

#define TEST_SET_PASS tiPtr->status = oslc::lul::TestHarness::TestInfo::Pass; tiPtr->lastErrorLine = 0;

#define TEST_EXP(exp)\
	try \
    {    \
      if(!(exp)) \
	  { \
          TEST_SET_FAIL; \
    	  LOG_TEST_ERR(STRINGIFY(exp)); \
      } \
      else \
      { \
         LOG_TEST_MSG(STRINGIFY(exp)); \
      } \
    } \
    catch(...) \
    { \
      TEST_SET_FAIL; \
      LOG_TEST_ERR("Exception raised"); \
	  LOG_TEST_ERR(STRINGIFY(exp)); \
    }

#define ON_ERROR_MSG(msg)\
        if(tiPtr->status==oslc::lul::TestHarness::TestInfo::Fail)\
          LOG_TEST_ERR(msg);

#define TEST_EXP2(exp,msg)\
		TEST_EXP(exp)\
		ON_ERROR_MSG(msg);

#define RUN_ALL_TESTS    oslc::lul::TestHarness::getTestHarness().runTests();

// !!! main must be defined in global name space !!!
#define GENERATE_TEST_MAIN \
int main(int argc, char **argv)\
{\
	   std::string testPathSpec;\
	   if(argc>1)\
		 testPathSpec = argv[1];\
       oslc::lul::TestHarness::getTestHarness().runTests(testPathSpec);\
       return 0;\
}

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_TESTHARNESS_H_ */
