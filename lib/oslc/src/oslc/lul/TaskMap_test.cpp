/*
 * TaskMap_test.cpp
 *
 *  Created on: Dec 1, 2018
 *      Author: admin
 */

#include <oslc/lul/TaskMap.h>
#include <oslc/lul/TestHarness.h>


namespace oslc {
namespace lul {

int viFunc(int a)
{
	LOG_INFO("viFunc callback -> "<<a);
	return a;
}

int viFunc2(int a)
{
	LOG_INFO("viFunc2 callback -> "<<a);
	return a;
}

typedef TaskMap<std::string, int(int)> VITaskMap;

REGISTER_TASK_MAP(MyTaskMapType,std::string, int(int));

REGISTER_TASK_MAP_TASK(MyTaskMapType,"blaTask",viFunc);

REGISTER_TEST("TaskMap","test 1")
{

	VITaskMap tm1;
	tm1.registerTask("viFunc",viFunc);
	tm1["viFunc"](1);
	TEST_EXP(tm1["viFunc"](1)==1);

	tm1["viFunc2"] = viFunc2;
	TEST_EXP(tm1["viFunc2"](2)==2);

	TEST_EXP(MyTaskMapType::get()["blaTask"](3)==3);


}

} /* namespace lul */
} /* namespace oslc */

//
// use outside namespace to verify clean compile in external name spaces..
//
int viFunc3(int a)
{
	LOG_INFO("viFunc callback -> "<<a);
	return a;
}

REGISTER_TASK_MAP(MyTaskMapType2,std::string, int(int));
REGISTER_TASK_MAP_TASK(MyTaskMapType2,"blaTask2",viFunc3);

REGISTER_TEST("TaskMap","test 2")
{

	TEST_EXP(MyTaskMapType2::get()["blaTask2"](3)==3);

}

