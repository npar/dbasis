/*
 * Misc_test.cpp
 *
 *  Created on: Feb 25, 2018
 *      Author: admin
 */



#include <oslc/lul/TestHarness.h>

#include <oslc/lul/Misc.h>
using namespace oslc::lul;

//
// TODO: add has function etc. non complete example below
// discussion and example references:
//   https://dev.krzaq.cc/post/checking-whether-a-class-has-a-member-function-with-a-given-signature/
//   https://www.boost.org/doc/libs/1_67_0/libs/tti/doc/html/index.html

/*
//#define OSLC_HAS_FUNC(F,RTYPE)\
//template <typename T>\
//class has_func_##F \
//{\
//  template <typename U>\
//  static constexpr std::false_type test(...) { return {};}\
//  template <typename U>\
//  static constexpr auto test(U* u) ->\
//    typename std::is_same<RTYPE, decltype(u->F())>::type { return {}; }\
//public:\
//  static constexpr bool value = test<T>(nullptr);\
//};

//OSLC_HAS_FUNC(testX,int)


//#define OSLC_HAS_MEMBER(F)\
//template <typename T>\
//class has_member_##F \
//{\
//  template <typename U>\
//  static constexpr std::false_type test(...) { return {};}\
//  template <typename U>\
//  static constexpr auto test(U *) ->\
//     decltype(&U::F, std::true_type() ){ return {}; }\
//public:\
//  static constexpr bool value = test<T>(nullptr);\
//};


OSLC_GEN_HAS_MEMBER(testFunc)
OSLC_GEN_HAS_MEMBER(testMember)
OSLC_GEN_HAS_TEMPLATE_MEMBER(testTemplateFunc)

//template<class Type, class Ret, class Arg>
//struct has_member_func
//{
//  //template<class> struct sfinae_true : std::true_type{};
//
//  template<class U, class R, class A0>
//  static auto test_func( int)
//    -> decltype(
//    		std::is_same< decltype(std::declval<U>().func(std::declval<A0>())), decltype(std::declval<R>()) >::value, TrueType()
//			); -> this version needs more work -> is same false will not fail decltype!
//    		//std::declval<U>().func(std::declval<A0>()), TrueType()); -> this one was correct
//  template<class U, class R, class A0>
//  static auto test_func(long)
//    -> decltype(FalseType());
//
//  static constexpr bool value = std::is_same<decltype(TrueType()),decltype(test_func<Type,Ret,Arg>(0))>::value;
//};
//
//template<class Type, class Ret, class Arg>
//struct has_x : decltype(std::declval<Type>().func(std::declval<Arg>()),TrueType()) {};
//
class TestMember
{
public:
	float testFunc(int);
	int testMember;
	template<class T>
	int testTemplateFunc();
};

//
//template<class T>
//struct XYZ
//{
//	typename std::enable_if<has_member_func<T,int,int>::value,bool>::type
//	works()
//		{
//		  return true;
//		}
//};

//GENERATE_HAS_MEMBER_TYPE(func)

//#define GEN_HAS_MEM_FUNC(func, name)                                        \
//    template<typename T, typename Sign>                                 \
//    struct name {                                                       \
//        typedef char yes[1];                                            \
//        typedef char no [2];                                            \
//        template <typename U, U> struct type_check;                     \
//        template <typename _1> static yes &chk(type_check<Sign, &_1::func > *); \
//        template <typename   > static no  &chk(...);                    \
//        static bool const value = sizeof(chk<T>(0)) == sizeof(yes);     \
//    }

	//template<class T>
    //struct name##_x : name<T, float(T::*)(int)>::value;



//GEN_HAS_MEM_FUNC(func,isFuncAvail);



REGISTER_TEST("Misc","test 1")
{
  bool x = has_member_testFunc<int>::value;
  bool y = has_member_testFunc<TestMember>::value;
  bool z = has_member_testMember<TestMember>::value;
  bool t = has_template_member_testTemplateFunc<TestMember>::value;
//  bool x = has_member_func<int,int,int>::value;
//  bool y = has_member_func<TestMember,int,int>::value;
//
//  //XYZ<int> xyz;
//  XYZ<TestMember> xyz;
//  xyz.works();

  //bool z = has_member_type_func<TestMember>::value;
  //bool z = isFuncAvail<TestMember, float(TestMember::*)(int)>::value;
  //bool z1 = isFuncAvail<int, float(int::*)(int)>::value;

  std::vector<int> v = { 1,2,3,4,5,6,7,8,9,10 };
  TEST_EXP(v.size()==10);
  oslc::lul::erase_if(v,[](int x){ return x<=5; });
  TEST_EXP(v.size()==5);
  oslc::lul::erase_if(v,[](const int &x){ return x<=5; });
  TEST_EXP(v.size()==5);
  return true;
}
*/
