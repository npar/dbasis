/*
 * FileUtil_test.cpp
 *
 *  Created on: Oct 28, 2018
 *      Author: admin
 */

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/FileUtil.h>

#include <cerrno>
#include <cstring>
#include <regex>

namespace oslc {
namespace lul {

// test object with process files
struct FileHandler
{
	bool operator()(const FileUtil::DirEntry &de)
	{
	   if(counter==0)
		   first = de.name;
	   counter++;
	   last = de.name;
	   return true;
	}
	// data
	int counter = 0;
	std::string first, last;
};


REGISTER_TEST("FileUtil","test 1")
{
  std::string tempFolder = FileUtil::createTempDir();
  std::string fullPath1 = tempFolder + "/testMakeDir";
  bool res = FileUtil::makeDir(fullPath1,S_IRWXU | S_IRWXG);
  TEST_EXP(res);
  if(!res)
	  LOG_TEST_MSG(std::strerror(errno));
  std::string fullPath2 = tempFolder + "/testMakeDir2/secFolder";
  res = FileUtil::makeDir(fullPath2,S_IRWXU | S_IRWXG);
  TEST_EXP(res);
  if(!res)
	  LOG_TEST_MSG(std::strerror(errno));
  //cleanup
  //TEST_EXP(FileUtil::remove(fullPath1));
  //TEST_EXP(FileUtil::remove(fullPath2));
  TEST_EXP(FileUtil::remove(tempFolder))
  // try double delete
  TEST_EXP(!FileUtil::remove(tempFolder))
  // try to create abs path folder
  std::string cwd = FileUtil::getCWD();
  TEST_EXP(FileUtil::makeDir(cwd));

}

REGISTER_TEST("FileUtil","test 2")
{
  std::string path = ".";
  std::vector<FileUtil::DirEntry> folderEntries;
  bool res = FileUtil::getDirEntries(path,folderEntries);
  TEST_EXP(res);
  int countFU = 0;
  auto func = [&countFU] (const FileUtil::DirEntry &de) { LOG_INFO("DirEntry => "<<de.name<<" "<<de.flags); countFU++; return true; };
#define SEARCH_DIR "src"
  res = FileUtil::processFiles(SEARCH_DIR,func,true);
  LOG_INFO("countFU == "<<countFU);
  // find  system file count
  FileUtil::Path tempFilePath(FileUtil::createTempDir());
  tempFilePath.add("testout.txt");
  std::string cmdLine = std::string("find " SEARCH_DIR " | wc -l > ") + tempFilePath.toString();
  std::system(cmdLine.c_str()); // execute the UNIX command
  std::ifstream f(tempFilePath.toString());
  int countSys;
  f>>countSys;
  f.close();
  LOG_INFO("countSys == "<<countSys);
  TEST_EXP(countFU==countSys);
  TEST_EXP(FileUtil::remove(tempFilePath.firstPart()));
  //test process files with object
  FileHandler fh;
  res = FileUtil::processFiles(SEARCH_DIR,fh,true);
  TEST_EXP(countSys==fh.counter);
  TEST_EXP(fh.first==SEARCH_DIR);
  fh.counter = 0;
  res = FileUtil::processFiles(SEARCH_DIR,fh,false);
  TEST_EXP(res && fh.last==SEARCH_DIR);
  fh.counter = 0;
  res = FileUtil::processFiles(SEARCH_DIR,fh,true,2);
  TEST_EXP(res && countSys>fh.counter);
  std::vector<FileUtil::DirEntry> folderEntries2;
  res = FileUtil::getDirEntriesRecursive(SEARCH_DIR,folderEntries2,true);
  TEST_EXP(countSys==folderEntries2.size() && folderEntries2[0].name==SEARCH_DIR);
  folderEntries2.clear();
  res = FileUtil::getDirEntriesRecursive(SEARCH_DIR,folderEntries2,false);
  TEST_EXP(countSys==folderEntries2.size() && folderEntries2[countSys-1].name==SEARCH_DIR);
  // test filtered version
  std::string filterC = "\\.cpp";
  std::string filterH = "\\.h";
  std::string filterCH = "\\.cpp|\\.h";
//  try   {
//  std::regex regExp(filter);
//  std::smatch m;
//  std::string str("abc.cpp");
//  res = std::regex_search(str,m,regExp);
//  }
//  catch(std::exception &e)
//  {
//	  LOG_INFO("bad regex: "<<e.what());
//  }
  folderEntries2.clear();
  res = FileUtil::getFilteredDirEntriesRecursive(SEARCH_DIR,folderEntries2,filterC,true);
  //size_t cfileCount = folderEntries2.size();
  //LOG_INFO("cfolder size "<<cfileCount);
  //folderEntries2.clear();
  res = FileUtil::getFilteredDirEntriesRecursive(SEARCH_DIR,folderEntries2,filterH,true);
  //size_t hfileCount = folderEntries2.size();
  //LOG_INFO("hfolder size "<<hfileCount);
  std::vector<FileUtil::DirEntry> folderEntries3;
  res = FileUtil::getFilteredDirEntriesRecursive(SEARCH_DIR,folderEntries3,filterCH,true);
  TEST_EXP(folderEntries2.size()==folderEntries3.size());


}

REGISTER_TEST("FileUtil","test 3")
{
  std::string path = ".";
  //std::vector<std::string> folderEntries;
  //bool res = FileUtil::getFolderEntries(path,folderEntries);
  FileUtil::Path fup(path);
  fup.add("abc/");
  fup.add("/efg/").add("//xy").add("tre/");
  std::string fullPath = fup.toString();
  LOG_TEST_MSG(fullPath);
  FileUtil::Path fup2(fullPath);
  TEST_EXP(fup2.isEqual(fup));
  FileUtil::Path fup3 = FileUtil::Path("/").add("abc").add("efg");
  TEST_EXP(fup3.isEqual(std::string("/abc/efg")));
  TEST_EXP(fup3.toString(1,2)==std::string("abc/efg"));
  TEST_EXP(fup3.toString(2,2)==fup3.lastPart());
  // test casting
  std::string pathStr = fup3;

}

REGISTER_TEST("FileUtil","test 4")
{
	std::string tempFolder = FileUtil::createTempDir();
    FileUtil::Path tempFilePath(tempFolder);
	tempFilePath.add("testout.txt");
    std::string cmdLine = std::string("find " SEARCH_DIR " | wc -l > ") + tempFilePath.toString();
    std::system(cmdLine.c_str()); // execute the UNIX command

	struct stat s;
	TEST_EXP(FileUtil::getFileInfo(tempFilePath,s));
	struct timespec accTime(s.st_atim), modTime(s.st_mtim);
	accTime.tv_sec += 60 ;
	modTime.tv_sec += 120;
	TEST_EXP(FileUtil::setFileTimes(tempFilePath,accTime,modTime));
	// now check that set was successful
	TEST_EXP(FileUtil::getFileInfo(tempFilePath,s));
	TEST_EXP(accTime.tv_sec==s.st_atim.tv_sec && modTime.tv_sec==s.st_mtim.tv_sec);
    TEST_EXP(FileUtil::remove(tempFolder))


}

} /* namespace lul */
} /* namespace oslc */
