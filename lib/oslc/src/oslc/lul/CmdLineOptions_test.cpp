/*
 * Serializer_test.cpp
 *
 *  Created on: Feb 15, 2018
 *      Author: admin
 */

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/CmdLineOptions.h>

#include<iostream>
using namespace std;

//namespace oslc {
//namespace lul {


using namespace oslc::lul;

#define TEST_CMD_OPT_STR "-xij"
#define TEST_CMD_OPT_VEC "--IntVec"
#define BAD_CMD_OPT_VEC "--BadOpt"
#define TEST_CMD_OPT_STR_REQ "-yij"


INIT_CMD_LINE("CmdLineTest", "Usage is CmdLineTest <-i inputFile> [<-o outputFile> [...]");
REGISTER_CMDLINE_OPT(TEST_CMD_OPT_STR, "Input file path\n\tmore info..", std::string,"",CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_SYNONYM(TEST_CMD_OPT_STR, "-x1");
REGISTER_CMDLINE_SYNONYM(TEST_CMD_OPT_STR, "-x2");
REGISTER_CMDLINE_OPT("-yyyy", "Output file path\n\tmore info..", int, 3,CmdLineOption::OptionType::ValueOpt);

REGISTER_CMDLINE_OPT(TEST_CMD_OPT_VEC, "Combination group. Usage is: [1,2,3]\n\toptional argument", std::vector<int>, std::vector<int>(), CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_OPT("--FloatVec", "Combination group. Usage is: [1,2,3]\n\toptional argument", std::vector<float>, std::vector<float>(), CmdLineOption::OptionType::ValueOpt);

REGISTER_CMDLINE_OPT_REQ(TEST_CMD_OPT_STR_REQ, "Required param\n\tmore info..", std::string,"",CmdLineOption::OptionType::ValueOpt);


typename std::enable_if<is_vector< vector<int> >::value,bool>::type abc;

REGISTER_TEST("CmdLineOptions","test 1")
{
	//const bool res = is_vector< vector<int> >::value;
	TEST_EXP(is_vector< vector<int> >::value==true);

	std::enable_if<is_vector< vector<int> >::value,int>::type iVal = 1;
	LOG_TEST_MSG("iVal=="<<iVal);

	CmdLineOptions::getGlblCmdLineOptions().displayUsageMsg();
	CmdLineOptions clOpts(CmdLineOptions::getGlblCmdLineOptions());

	char *argv[7];// = "-a 123 -b 456 -d ZXY";
	argv[0]=(char *)"myExeFName";
	argv[1]=(char *)TEST_CMD_OPT_STR;
	argv[2]=(char *)"testOptData 1";
	argv[3]=(char *)TEST_CMD_OPT_VEC;
	argv[4]=(char *)"[0,1,2,3,4]]";
	//argv[5]=(char *)BAD_CMD_OPT_VEC;
	argv[5]=(char *)TEST_CMD_OPT_STR_REQ;
	argv[6]=(char *)"testOptData 2";

	//argv[3]
	int argc = 7;

	if(!clOpts.parse(argc, argv))
	{
		TEST_SET_FAIL;
		LOG_TEST_MSG("Error reading command line options please fix your command line options and try again!");
	}

	clOpts.displayUsageMsg();

	LOG_TEST_MSG("recreated command line= "<<CmdLineOptions::cmdLineToString(argc,argv));
	//clOpts[]
	//using CmdLineOptions;
	//CmdLineOptions::CmdLineOptionPtr<std::string> testOpt = clOpts[TEST_CMD_OPT_STR];
	CmdLineOptions::OptionBasePtr testOpt = clOpts[TEST_CMD_OPT_STR];
	TEST_EXP(testOpt->isAvailable);
	CmdLineOptionPtr<std::string> testOptT2 = clOpts.getOpt<std::string>(TEST_CMD_OPT_STR);
	//CmdLineOptionPtr<std::string> outputFileOpt = cmdLineOptions.getOpt<std::string>(OUTPUT_FILE_CMD_OPT_STR);
	LOG_TEST_MSG("argv[2]=="<<testOptT2->getVal());
	TEST_EXP(testOptT2->getVal()==argv[2]);

	CmdLineOptionPtr<std::vector<int> > testOptVec = clOpts.getOpt<std::vector<int> >(TEST_CMD_OPT_VEC);
	vector<int> &intVec = testOptVec->value;
	LOG_TEST_MSG("Testing int vector values");
	for(int i=0;i<intVec.size();i++)
	{
	  TEST_EXP(intVec[i]==i);
	}
}

REGISTER_TEST("CmdLineOptions","test 2")
{

	CmdLineOptions::getGlblCmdLineOptions().displayUsageMsg();
	CmdLineOptions clOpts(CmdLineOptions::getGlblCmdLineOptions());

	char *argv[5];
	argv[0]=(char *)"myExeFName";
	argv[1]=(char *)TEST_CMD_OPT_STR;
	argv[2]=(char *)"testOptData 1";
	argv[3]=(char *)TEST_CMD_OPT_STR_REQ;
	argv[4]=(char *)"testOptData 2";
	int argc = 5;
	TEST_EXP(clOpts.parse(argc, argv))

	// reset
	clOpts[TEST_CMD_OPT_STR]->isAvailable = false;
	clOpts[TEST_CMD_OPT_STR_REQ]->isAvailable = false;

	argc = 3;
	TEST_EXP(!clOpts.parse(argc, argv))
}

//} /* namespace lul */
//} /* namespace oslc */
