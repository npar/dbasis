/*
 * StringParser.h
 *
 *  Created on: Sep 25, 2017
 *      Author: admin
 */

#ifndef OSLC_LUL_STRINGPARSER_H_
#define OSLC_LUL_STRINGPARSER_H_


#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <memory>


#include <oslc/lul/Log.h>

//const string StdDelimitersG = "\n\r:";
#define DEFAULT_DELIMITER_STR "\n\r\t: "

namespace oslc {
namespace lul {

class StringParser
{
public:
	StringParser(std::string StdDelimiters_ = DEFAULT_DELIMITER_STR): StdDelimiters(StdDelimiters_)
    {
    }

	virtual ~StringParser() {}

	const char CommentSymbol = '#';
	const std::string StdDelimiters;

	bool isBlankLine(const std::string &textLine)
	{
		// handle empty line
		if(textLine.size()==0)
			return true;
        // else
		// look for next token if any
		size_t tokBegin = getNextTokenPos(textLine);
		if(tokBegin==std::string::npos)
			return true;
		else
			return false;
	}

	bool isCommentLine(const std::string &textLine)
	{
		// handle blank and comment line
		if(textLine.size()>0 && textLine[0]==CommentSymbol)
			return true;
		else
			return false;
	}

	bool loadDataFromFile(const char *srcFile)
	{

		std::unique_ptr<std::ifstream> finPtr(new std::ifstream);
		finPtr->open(srcFile);
		if(!finPtr->good())
		{
			LOG_ERROR("failed to load file: "<<srcFile);
			return false;
		}
		m_sinPtr = std::move(finPtr);
		return true;
	}

	bool loadDataFromString(const char *str)
	{

		std::unique_ptr<std::istringstream> isStream(new std::istringstream(str));
		m_sinPtr = std::move(isStream);
		return true;
	}

	struct TagReader
	{
		size_t getTagText(const std::string& rawStr, size_t beginPos, std::string tag, std::string &tokenStr)
		{
			size_t ir = beginPos;
			size_t last_ir;
			size_t it = 0;
			while(it<tag.size() && ir<rawStr.size())
			{
			  it = 0;
			  last_ir = ir;
			  while(it<tag.size() && ir<rawStr.size())
			  {
				  // if exact match
				  if(rawStr[ir]==tag[it])
				  {
					  ir++;
					  // inc and check match
					  if(++it == tag.size())
						  break;
				  }
				  // if star
				  else
				  if(tag[it]=='*')
				  {
					  // if match next token
					  if(rawStr[ir]==tag[it+1])
					  {
						  //ir++;
						  it++;
					  }
					  else
					  {
						  ir++;
					  }
				  }
				  else // not equal
				  // if no match just move forward and at begin
				  if(it==0)
				  {
				    ir++;
				    last_ir = ir;
				  }
				  else // we were in the middle of a match
				  {
					ir = last_ir + 1; //reset ir
					break;
				  }
			  }
			}
			// if we had a match
			if(it==tag.size())
			{
				tokenStr = rawStr.substr(last_ir,ir-last_ir);
				return ir;
			}
			else
				return std::string::npos;
		}

		bool isClosingTag(const std::string &tag)
		{
			return tag.find('/')!=std::string::npos;
		}

		bool getNextTag(std::string &tag, std::string &data)
		{
			std::string rawTag1;
			size_t beginPosNew = m_beginPos;
			do
			{
			  beginPosNew = getTagText(m_data,beginPosNew,"<*>",rawTag1);
			  if(beginPosNew==std::string::npos)
				return false;
			} while (isClosingTag(rawTag1));
			rawTag1.insert(rawTag1.begin()+1,'/'); // create "</*>"
			std::string rawTag2;
			size_t endPosNew = getTagText(m_data,beginPosNew,rawTag1,rawTag2);
			if(endPosNew==std::string::npos)
				return false;
			//if(rawTag1!=rawTag2)
				//return false;
			tag =  rawTag1.substr(2,rawTag1.size()-3);
			data = m_data.substr(beginPosNew,endPosNew-beginPosNew-rawTag2.size());
			// update tag reader start pos
			m_beginPos = beginPosNew;

	        return true;
		}
       // data
	   size_t m_beginPos;
	   size_t m_endPos;
	   std::string m_data;
	};

	bool initTagReader(TagReader &tr)
	{
		std::string textLine, allText;
		// read all file and cleanup
		while(getline(*m_sinPtr,textLine))
		{
			// handle blank and comment line
			if(isBlankLine(textLine) || isCommentLine(textLine))
				continue;

			allText += textLine;
		}

		tr.m_beginPos = 0;
		tr.m_endPos = allText.size() - 1;
		tr.m_data = allText;
        // look for the first tag
		//if(getNextTag(tag,allText))
		if(!tr.m_data.empty())
		{
			return true;
		}
		else
	      return false;
	}

	bool getNextTokens(std::stringstream &sStream)
	{
		std::string textLine;
		while(getline(*m_sinPtr,textLine))
		{
			// handle blank and comment line
			if(isBlankLine(textLine) || isCommentLine(textLine))
				continue;
			//getNextToken(textLine,)
			// handle data line
			removeDelimiters(textLine);
			sStream.clear();
			sStream.str(textLine);
			return true;
		}

		return false;

        // TODO: only handles int for now...
		//while (fin >> de.m_timeStamp >> de.m_intVal)
		//{
			//m_data.push_back(de);
		//}
	}

	void removeDelimiters(std::string &rawString)
	{
		removeDelimiters(rawString,StdDelimiters);
	}

	void removeDelimiters(std::string &rawString, const std::string &delimiters, const char replaceWith=' ')
	{
		for(size_t i=0;i<rawString.size();i++)
		{
			// if we didnt find the char stop
			if(delimiters.find(rawString[i])!=std::string::npos)
				rawString[i] = replaceWith;
		}
	}

	// !!! can not have default param that is not known at complile time example: delimiters = StdDelimiters
	// https://stackoverflow.com/questions/12903724/default-arguments-have-to-be-bound-at-compiled-time-why
	int discardDelimiters(std::stringstream &sStream)
	{
		return discardDelimiters(sStream,StdDelimiters);
	}

	int discardDelimiters(std::stringstream &sStream, std::string delimiters)
	{
	  //char ch;
	  int count = 0;
	  while(sStream.good())
	  {
		char ch = sStream.peek();
		// if we didnt find the char stop
		if(delimiters.find(ch)==std::string::npos)
			break;
		else
		{
			// eat and discard of delimiter
			sStream>>ch;
			count ++;
		}
	  }
	  return count;
	}

	// delimiters
	static size_t getNextToken(const std::string& rawStr, size_t beginPos, const std::string &delimiters, std::string &tokenStr)
	{
		std::string resStr;
		size_t tokBegin = rawStr.find_first_not_of(delimiters,beginPos);
		if(tokBegin==std::string::npos)
			return std::string::npos;
		size_t tokEnd = rawStr.find_first_of(delimiters,tokBegin);
		// if we didnt find any non token chars, rest of string was all token
		if(tokEnd==std::string::npos)
			tokEnd = rawStr.size();
		// note tokEnd is a delimiter char or eofs
		tokenStr = rawStr.substr(tokBegin,tokEnd-tokBegin);

		return tokEnd;
	}

	static std::vector<std::string> getTokenList(const std::string& str, const std::string &delimiters)
	{
		std::string tokenStr;
		size_t beginPos = 0;
		std::vector<std::string> tokenVec;
		while((beginPos=getNextToken(str,beginPos,delimiters,tokenStr))!=std::string::npos)
		{
			tokenVec.push_back(tokenStr);
		}
		return tokenVec;
	}

	size_t getNextTokenPos(const std::string& textLine)
	{
		size_t tokBegin = textLine.find_first_not_of(DEFAULT_DELIMITER_STR);
		return tokBegin;
	}

	// data
	std::unique_ptr<std::istream> m_sinPtr;
};


} // namespace oslc
} // namespace lul


#endif /* OSLC_LUL_STRINGPARSER_H_ */
