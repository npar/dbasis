/*
 * MemBuffer_test.cpp
 *
 *  Created on: Jan 8, 2018
 *      Author: admin
 */

#include <iostream>
#include <istream>
#include <streambuf>
#include <string>
//#include <strstream>

#include <oslc/lul/MemBuffer.h>

#include <oslc/lul/TestHarness.h>


namespace oslc {
namespace lul {

//REGISTER_TEST("Base Tests","XXXX")
//{
//	//std::abort();
//	//throw 101;
//
//   int x = 0;
//
//   //x = 3/x;;
//
//   TEST_EXP(5>100);
//   return 0;
//
//}

REGISTER_TEST("MemBuffer","test 1")
{
    char buffer[] = "I'm a buffer with embedded nulls\0and line\n feeds";

    MemBuffer sbuf(buffer, buffer + sizeof(buffer));
    std::istream in(&sbuf);
    std::string line;
    while (std::getline(in, line)) {
    	LOG_TEST_MSG("line: " << line << "\n");
    }

    #define TEST_IVAL 12345

    std::ostream out(&sbuf);
    int iVal = TEST_IVAL;
    out<<iVal;

    std::iostream inOut(&sbuf);
    inOut>>iVal;
    TEST_EXP(iVal==TEST_IVAL);
    inOut<<iVal;
    inOut>>iVal;
    TEST_EXP(iVal==TEST_IVAL);

    const int N = 100;
    MemBuffer mb(N);
    TEST_EXP(mb.buffSize()==N);
    std::iostream inOut2(&mb);

    #define TEST_FVAL 12.345
    float fVal = TEST_FVAL;
    inOut2<<fVal;
    fVal = 0.0;
    inOut2>>fVal;
    TEST_EXP(abs(fVal-TEST_FVAL)<0.1);


    IOMemStream ioMemStrm(100);
    ioMemStrm<<int(1)<<'|'<<int(22); // cast to type

    //oMemStrm.
    char ch;
    int x,y;
    ioMemStrm>>x>>ch>>y;
    LOG_INFO(" x is "<<x<<" ch is "<<ch<<" y is "<<y);
    TEST_EXP2(x==1 and ch=='|' and y==22," x is "<<x<<" ch is "<<ch<<" y is "<<y);

}

// overflow test code from: https://en.cppreference.com/w/cpp/io/strstreambuf/overflow
//struct BuffTest : std::strstreambuf
//{
//    int_type overflow(int_type c)
//    {
//        std::cout << "Before overflow(): size of the put area is " << epptr()-pbase()
//                  << " with " << epptr()-pptr() << " write positions available\n";
//        int_type rc = std::strstreambuf::overflow(c);
//        std::cout << "After overflow(): size of the put area is " << epptr()-pbase()
//                  << " with " << epptr()-pptr() << " write positions available\n";
//        return rc;
//    }
//};

REGISTER_TEST("MemBuffer","test 2")
{

//	BuffTest sbuff;
//	std::iostream stream(&sbuff);
//
//	stream << "Sufficiently long string to overflow the initial allocation, at least "
//	           << " on some systems.";

	const int N = 10;
    IOMemStream ioMemStrm(N);
    for(int i=0;i<N*2;i++)
    {
    	//TEST_EXP2(ioMemStrm.good(),"Stream error: "<<i<<" : "<<ioMemStrm.flags());
        ioMemStrm<<i<<'|';

    }
	TEST_EXP2(ioMemStrm.good(),"Stream error: "<<ioMemStrm.flags());


    int val=0,prevVal=-1;
    char ch;
    //TODO: check why ioMemStrm.eof()) not working
    while(ioMemStrm.good())
    {
    	ioMemStrm>>val>>ch;
    	if(!ioMemStrm.good())
    	{
            TEST_EXP2(prevVal==(2*N-1),"val/prevVal are "<<val<<":"<<prevVal<<" ch is"<<ch);
    		break;
    	}
        prevVal = val;
    }

    TEST_EXP(prevVal==(2*N-1));


}

} /* namespace lul */
} /* namespace oslc */
