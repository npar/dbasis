/*
 * DynLib_test_dll.h
 *
 *  Created on: Oct 18, 2020
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_DYNLIB_TEST_DLL_H_
#define SRC_OSLC_LUL_DYNLIB_TEST_DLL_H_

#include <oslc/lul/DynLib.h>

class MyPluginBase: public oslc::lul::IPlugin
{
public:
	//using IPlugin::IPlugin;
	MyPluginBase(): IPlugin("testP",1010) {}
	virtual int testMemberFunc(int x) { return x+0; }

};

typedef int(*MyExpoFuncPtrType)(int);

#endif /* SRC_OSLC_LUL_DYNLIB_TEST_DLL_H_ */
