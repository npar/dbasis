/*
 * TaskMap.h
 *
 *  Created on: Dec 1, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_TASKMAP_H_
#define SRC_OSLC_LUL_TASKMAP_H_

#include <functional>
#include <map>

namespace oslc {
namespace lul {

template<class Key, class FuncSig>
class TaskMap
{
public:
	TaskMap()  {}

	typedef std::function <FuncSig> FuncType;

	typedef std::map<Key, FuncType> EvalTaskMap;

	bool registerTask(Key type, FuncType func)
	{
		m_data.emplace(std::pair<Key,FuncType>(type, func));
        return true;
	}

	EvalTaskMap &getTaskMap() { return m_data; }

	FuncType &getTask(Key type) { return m_data[type]; }

	FuncType &operator[](Key type) { return getTask(type); }

	static TaskMap &get()
	{
		static TaskMap theOne;
		return theOne;
	}

	~TaskMap() {}
private:
	// data
	EvalTaskMap m_data;

};

#define REGISTER_TASK_MAP(name,keyType,funcSig) typedef oslc::lul::TaskMap<keyType,funcSig> name

#define REGISTER_TASK_MAP_TASK(taskMapType,type,task)\
static volatile bool MACRO_CONCAT(global_option_,__COUNTER__) = taskMapType::get().registerTask(type,task)


} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_TASKMAP_H_ */
