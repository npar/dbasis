/*
 * Time.h
 *
 *  Created on: May 11, 2018
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_TIME_H_
#define SRC_OSLC_LUL_TIME_H_

#include <string>
#include <chrono>
#include <thread>
#include <map>


namespace oslc {
namespace lul {

class Time {
public:
	Time();
	~Time();
};

class DateTime
{
public:
	DateTime(time_t timeT = -1): m_timeT(timeT) {}

	DateTime(const DateTime &dt)
	{
		m_timeT = dt.m_timeT;
	}

	time_t update() { return m_timeT = time(NULL); }

	static DateTime now()
	{
		DateTime dt;
		dt.update();
		return dt;
	}

	const char *format(const char *format)
	{
		struct tm * timeinfo = localtime (&m_timeT);
		strftime (m_buffer,MaxBuffSize,format,timeinfo);
		return (const char *)&m_buffer[0];
	}
private:
	// data
	static const unsigned int MaxBuffSize = 128;
	char m_buffer[MaxBuffSize];
	time_t m_timeT;
};

template<class T>
class XTimer
{
public:
	XTimer() {}
	XTimer(const T &start, const T &stop): m_start(start), m_stop(stop) {}
protected:
	T m_start;
	T m_stop;
};

struct CPUTime
{
	static double diffSec(clock_t start, clock_t stop)
	{
		return ((double)(stop - start))/ CLOCKS_PER_SEC;
	}
};

class CPUTimer: XTimer<clock_t>
{
public:
	CPUTimer(): XTimer(0,0) {}
	void start()
	{
		m_start = clock();
	}

	void stop()
	{
		m_stop = clock();
	}

	double elapsedSec()
	{
		return  CPUTime::diffSec(m_start,clock());
	}

	double totalSec()
	{
		return  CPUTime::diffSec(m_start,m_stop);
	}
};

// used not to pollute global name space
namespace _
{
  namespace stdc = std::chrono;
  using stdch = stdc::high_resolution_clock;
} // namespace _

using TimePointHiRes = _::stdch::time_point;

typedef long int MilliSec;

struct WallTime
{

	static TimePointHiRes now() { return  _::stdch::now(); }

	// try to sleep milli amount and report actual time slept
	static MilliSec sleepMilli(MilliSec milli)
	{
		TimePointHiRes timeStamp = now();
		std::this_thread::sleep_for (std::chrono::milliseconds(milli));
		return diffMilli(timeStamp);
	}

	static double diffSec(TimePointHiRes start, TimePointHiRes stop = now())
	{
		_::stdc::duration<double> time_span = _::stdc::duration_cast<_::stdc::duration<double>>(stop-start);
		return time_span.count();
	}

	static MilliSec diffMilli(TimePointHiRes start, TimePointHiRes stop = now())
	{
		auto time_span = _::stdc::duration_cast<_::stdc::milliseconds>(stop-start);
		return time_span.count();
	}

	template<class F>
	static bool bussyWait(MilliSec maxWait, MilliSec interval, F evalFunc)
	{
		// wait for queue to be ready to send
		bool ready = evalFunc();
		while(!ready && maxWait>0)
		{
			maxWait -= oslc::lul::WallTime::sleepMilli(interval);
			ready = evalFunc();
		}
		// return success or failure
		return ready;
	}
};

class WallTimer: XTimer<TimePointHiRes>
{
public:
	WallTimer() {} //: XTime(0,0) {} -> no need initialize TimePointHiRes

	void start()
	{
		m_start = _::stdch::now();
	}

	void stop()
	{
		m_stop = _::stdch::now();
	}

	MilliSec elapsedMilli()
	{
		return  WallTime::diffMilli(m_start,_::stdch::now());
	}

	double elapsedSec()
	{
		return  WallTime::diffSec(m_start,_::stdch::now());
	}

	double totalSec()
	{
		return  WallTime::diffSec(m_start,m_stop);
	}
};

class TagWallTimer: XTimer<TimePointHiRes>
{
public:
	typedef std::map<std::string,TimePointHiRes> TagMap;

	TagWallTimer(const std::string &startTag="")
    {
		if(!startTag.empty())
		  start(startTag);
		else
		  start();
    }
	void start() { m_start = WallTime::now(); }
	void start(const std::string &startTag) { start(); m_tags[startTag] = m_start;}
	void reset() { m_tags.clear(); }
	void tag(const std::string &tag)
	{
		m_tags[tag] = WallTime::now();
	}
	double diffSec(const std::string &tagBegin, const std::string &tagEnd)
	{
		return WallTime::diffSec(m_tags[tagBegin],m_tags[tagEnd]);
	}
	double diffSec(const std::string &tagBegin, TimePointHiRes endTimePoint = WallTime::now())
	{
		// Note: we are using an ordered map, elements are ordered!
		return WallTime::diffSec(m_tags[tagBegin],endTimePoint);
	}
	double elapsedSecFromStart(const std::string &tagEnd)
	{
		// Note: we are using an ordered map, elements are ordered!
		return WallTime::diffSec(m_start,m_tags[tagEnd]);
	}
	const TimePointHiRes &getTag(const std::string &tag)
	{
		return m_tags[tag];
	}
	const TimePointHiRes &operator[](const std::string &tag)
	{
		return getTag(tag);
	}

	TagMap &getTagMap() { return m_tags; }

protected:
	TagMap m_tags;
};

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_TIME_H_ */
