/*
 * Pipeline.h
 *
 *  Created on: Jun 20, 2019
 *      Author: admin
 */

#ifndef SRC_OSLC_LUL_PIPELINE_H_
#define SRC_OSLC_LUL_PIPELINE_H_

#include <vector>
#include <functional>

#include <oslc/lul/Log.h>

namespace oslc {
namespace lul {


struct PipelineState;

class  PipelineInterface
{
public:
	virtual std::string getStageName(int index) = 0;
	virtual int getStageIndex(std::string stageName) = 0;
	virtual ~PipelineInterface() {}
};

struct PipelineState
{
	enum Op { RedoStage, NextStage, PrevStage, SkipNextStages, JumptoStage, HaltPipeline, PassComplete };

	PipelineState() :  pipeLineInterface(0) { reset(); } //, index(0), nextOp(NextStage), nextIndex(-1) {}
	void reset() {  index = 0; prevIndex = nextIndex = -1; nextOp = NextStage; }
	void setNextOp(Op nextOp_, int nextIndex_=-1) { nextOp = nextOp_; nextIndex = nextIndex_; }
	void setNextOp(Op nextOp_, std::string stageName)
	{
	  nextOp = nextOp_;
	  nextIndex = getStageIndex(stageName);
	}
	int getStageIndex(std::string stageName) { return pipeLineInterface->getStageIndex(stageName); }
	std::string getStageName(int index_=-1)
	{
		// if invalid index set to our index
		if(index_<0)
			index_=index;
		return pipeLineInterface->getStageName(index_);
	}
	// data
	PipelineInterface *pipeLineInterface;
	int index;
    Op nextOp;
    int nextIndex;
    int prevIndex;
};

enum PipelineFlags { None = 0, AutoResetState = 0x1, AutoSetNextStage=0x2 };

template<class S=PipelineState>
class  Pipeline: public PipelineInterface
{
public:

	static_assert(
		        std::is_base_of<PipelineState, S>::value,
		        "S must be a descendant of PipelineState"
		    );

	typedef std::function<void(S &state)> MemFptr;
	typedef std::shared_ptr<S> StateSPtr;

	Pipeline &setFlag(PipelineFlags f)
	{
		m_flags |= f;
		// allow chaining
		return *this;
	}

	Pipeline(StateSPtr stateSPtr = std::make_shared<S>()): m_stateSPtr(stateSPtr), m_flags(AutoResetState)
	{
		m_stateSPtr->pipeLineInterface = this;
	}

	void registerStage(MemFptr fptr, std::string name)
	{
		m_stages.push_back(fptr);
		if(name.size())
			m_stageMap[name] = m_stages.size() - 1;
	}

	int stageNameToIndex(std::string name) { return  m_stageMap[name]; }

	///////////////////////
	// PipelineInterface
	///////////////////////
	//TODO: consider changing this to map as well if used heavily. search inefficient...
	virtual std::string getStageName(int index)
	{
       for(auto &stage: m_stageMap)
    	   if(stage.second==index)
    		   return stage.first;
       throw std::runtime_error("getStageName called with invalid index");
	}

	virtual int getStageIndex(std::string stageName)
	{
		return stageNameToIndex(stageName);
	}
	///////////////////////

	Pipeline &addStage(MemFptr fptr, std::string name)
	{
		registerStage(fptr,name);
		// allow chaining
		return *this;
	}

	template<class Func, class Obj>
	Pipeline &addStage(Func fptr, Obj &obj, std::string name)
	{
		auto flamda = std::bind(fptr,std::ref(obj),std::placeholders::_1);
		registerStage(flamda,name);
		return *this;
	}

	StateSPtr getState()
	{
		return m_stateSPtr;
	}

	typename S::Op runPass()
	{
		S &state = *m_stateSPtr.get();
		// check auto reset flag
		if(m_flags & AutoResetState)
		  state.reset();
		//int &i = state.index;
		while(state.index<m_stages.size())
		{
			// TODO: before running a stage set the next op to default.
			// this way stage function does not need to set state.
			// do we want to change that? set flags to control this?
			// right now you must reset it manually!
			//state.nextOp = S::Op::NextStage;
			if(m_flags & AutoSetNextStage)
			  state.setNextOp(PipelineState::NextStage);
			// call stage handler
			m_stages[state.index](state);
			// update previous index
			state.prevIndex = state.index;
            // switch on next op
			switch(state.nextOp)
			{
			case S::Op::RedoStage:
				break;
			case S::Op::NextStage:
				state.index++;
				break;
			case S::Op::PrevStage:
				(state.index>0) ? state.index-- : state.index=0;
				break;
			case S::Op::JumptoStage:
				state.index = state.nextIndex;
				//state.nextIndex = -1; // careful if we reset this we should also reset next state
				break;
			case S::Op::SkipNextStages:
				return S::Op::SkipNextStages;
				break;
			case S::Op::HaltPipeline:
				return S::Op::HaltPipeline;
				break;
			default:
				LOG_ERROR("Unknown pipeline state!");
				return S::Op::HaltPipeline;
				break;
			}
		}
		return S::Op::PassComplete;
	}

protected:
	std::vector<MemFptr> m_stages;
	std::map<std::string,int> m_stageMap;
	StateSPtr m_stateSPtr;
	int m_flags;
};

} /* namespace lul */
} /* namespace oslc */

#endif /* SRC_OSLC_LUL_PIPELINE_H_ */
