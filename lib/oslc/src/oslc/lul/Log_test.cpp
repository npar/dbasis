/*
 * log.cpp
 *
 *  Created on: May 9, 2018
 *      Author: admin
 */

#include <oslc/lul/Log.h>
#include <oslc/lul/TestHarness.h>
using namespace oslc::lul;

namespace oslc {
namespace lul {


REGISTER_TEST("Log","test 1")
{
  MsgLog testLog(std::clog,MsgLog::InfoLevel);
  testLog.setDest(std::cerr);
  testLog.setOutputFormat(MsgLog::OutputFormat::Terminal);
  testLog.setLogLevel(MsgLog::Warning);
  testLog.log(__FILE__,__LINE__,MsgLog::Info)<<"test Info"<<"\n";
  testLog.log(__FILE__,__LINE__,MsgLog::Warning)<<"test Warning"<<"\n";
  testLog.log(__FILE__,__LINE__,MsgLog::Error)<<"test Error"<<"\n";

  // test sys log

  LOG_SET_LEVEL(MsgLog::None);
  LOG_ERROR("Test none level");

  LOG_SET_LEVEL(MsgLog::DebugLevel);

  LOG_DEBUG("Test log functions");
  LOG_INFO("Test log functions");
  LOG_WARNING("Test log functions");
  LOG_WARN("Test log functions"); // test warn shortcut
  LOG_ERROR("Test log functions");


  LOG(MsgLog::Debug,"test log debug");
  LOG(MsgLog::Info,"test log Info");
  LOG(MsgLog::Warning,"test log Warning");
  LOG(MsgLog::Error,"test log Error");

  for(int i=0;i<18;i+=2)
  {
    LOG_IF(MsgLog::Debug,"test log if i>5 Info i="<<i,i>5);
    LOG_DEBUG_IF("test log if i>5 Info i="<<i,i>5);

    LOG_IF(MsgLog::Info,"test log if i>6 Info i="<<i,i>6);
    LOG_INFO_IF("test log if i>6 Info i="<<i,i>6);

    LOG_IF(MsgLog::Warning,"test log if i>7 Warning i="<<i,i>7);
    LOG_WARN_IF("test log if i>7 Warning i="<<i,i>7);
    LOG_WARNING_IF("test log if i>7 Warning i="<<i,i>7);

    LOG_IF(MsgLog::Error,"test log if i>8 Error i="<<i,i>8);
    LOG_ERROR_IF("test log if i>8 Error i="<<i,i>8);

  }

}

REGISTER_TEST("Log","test 2")
{
  // test multiple threads
  auto loggerFunc = []()
					 {
						 for(int i=0;i<10;i++)
						   LOG_INFO("Hello from thread "<<std::this_thread::get_id());
					 };

  std::thread t1(loggerFunc);
  std::thread t2(loggerFunc);
  t1.join();
  t2.join();

}

REGISTER_TEST("Log","test 3")
{
	MsgLog testLog(std::clog,MsgLog::InfoLevel);
	testLog.setDest(std::cerr);
	testLog.setOutputFormat(MsgLog::OutputFormat::Terminal);
	int i=1;
	while(i<MsgLog::UserDefinedBase * 2 + 1)
	{
	  int logLevel = i;//MsgLog::UserDefinedBase << i;
	  testLog.setLogLevel(logLevel);
	  testLog.registerType(i,std::string("CustomLogMsg")+ std::to_string(i));
	  testLog.log(__FILE__,__LINE__,MsgLog::Debug)<<"test Info"<<"\n";
	  testLog.log(__FILE__,__LINE__,MsgLog::Info)<<"test Info"<<"\n";
	  testLog.log(__FILE__,__LINE__,MsgLog::Warning)<<"test Warning"<<"\n";
	  testLog.log(__FILE__,__LINE__,MsgLog::Error)<<"test Error"<<"\n";
	  testLog.log(__FILE__,__LINE__,(MsgLog::Type)logLevel)<<"test custom logLevel"<<logLevel<<"\n";
	  //i = (i<MsgLog::Debug) ? i + 1 : i + 2;
	  i<<=1;
	}
}


} /* namespace lul */
} /* namespace oslc */

