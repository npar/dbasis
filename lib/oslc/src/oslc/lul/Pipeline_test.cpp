/*
 * Pipeline_test.cpp
 *
 *  Created on: Jun 20, 2019
 *      Author: admin
 */

#include<string>
using namespace std;

#include <functional>

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/Pipeline.h>

namespace oslc {
namespace lul {

struct NewState: public PipelineState
{
	NewState() { myData= 121; }
	int myData;
};

class TestClass1
{
public:
	void testFunc(PipelineState &state)
	{
		LOG_INFO("in TestClass1::testFunc");
		state.setNextOp(PipelineState::NextStage);
	}

	void testFuncNS(NewState &state)
	{
		LOG_INFO("in TestClass1::testFuncNS");
		state.setNextOp(PipelineState::NextStage);
	}

};

class TestClass2
{
public:
	TestClass2(): counter(0) {}
	void testFunc(PipelineState &state)
	{
		//std::string stageName = state.getStageName();
		LOG_INFO("in TestClass2::testFunc " << "stageName = " << state.getStageName() << " " << this);
		if(counter++>=2)
		  state.setNextOp(PipelineState::NextStage);
		else
		if(state.nextOp==PipelineState::NextStage)
		  state.setNextOp(PipelineState::RedoStage);
		else
		if(state.nextOp==PipelineState::RedoStage)
		{
		  int jumptoIndex = (state.index>0) ? state.index-1 : state.index+1;
		  std::string jumptoStageName = state.getStageName(jumptoIndex);
		  state.setNextOp(PipelineState::JumptoStage,jumptoStageName);
		}

	}
	void testFuncNS(NewState &state)
	{
		LOG_INFO("in TestClass2::testFuncNS");
		state.setNextOp(PipelineState::NextStage);
	}

	// data
	int counter;

};

void testFunc(PipelineState &state)
{
	LOG_INFO("in glbl testFunc");
	state.setNextOp(PipelineState::NextStage);
}

void testFuncNS(NewState &state)
{
	LOG_INFO("in glbl testFuncNS");
	state.setNextOp(PipelineState::NextStage);
}

static int bla = 0;

void testFuncNoSetState1(PipelineState &state)
{
	LOG_INFO("in glbl testFuncNoSetState");
	if(bla++==0)
	  state.setNextOp(PipelineState::RedoStage);
}

void testFuncNoSetState2(PipelineState &state)
{
	LOG_INFO("in glbl testFuncNoSetState2");
	if(bla++<3)
	  state.setNextOp(PipelineState::PrevStage);
}

REGISTER_TEST("Pipeline","Test 1")
{

  TestClass1 tc1;
  TestClass2 tc2;

  ///////////////////////////
  // fail static assert
  // error: static assertion failed: S must be a descendant of PipelineState
  //Pipeline<TestClass1> pl;
  ///////////////////////////

  Pipeline<> pl;
  auto flamda = std::bind(&TestClass1::testFunc,std::ref(tc1),std::placeholders::_1);
  pl.addStage(flamda,"stage1");

  auto flamda2 = std::bind(&TestClass2::testFunc,std::ref(tc2),std::placeholders::_1);
  pl.addStage(flamda2,"stage2");


  pl.addStage(&TestClass2::testFunc,tc2,"stage3");

  pl.addStage(testFunc,"stage4");

  pl.addStage([&](PipelineState &state) { tc2.testFunc(state); },"stage5");

  //pl.addStage([&]() { Pipeline::State state; tc2.testFunc(state); } );

  pl.runPass();

//  std::function<void()> fptr = std::bind(&TestClass1::testFunc2,tc1);
//  fptr();
//  fptr = std::bind(&TestClass2::testFunc2,tc2);


}

REGISTER_TEST("Pipeline","Test 2")
{

  TestClass1 tc1;
  TestClass2 tc2;

  //Pipeline<TestClass1> pl(tc1);
  Pipeline<NewState> pl(std::make_shared<NewState>());
  auto flamda = std::bind(&TestClass1::testFunc,std::ref(tc1),std::placeholders::_1);
  pl.addStage(flamda,"stage1");

  auto flamdaNS = std::bind(&TestClass1::testFuncNS,std::ref(tc1),std::placeholders::_1);
  pl.addStage(flamdaNS,"stage1NS");

  auto flamda2 = std::bind(&TestClass2::testFunc,std::ref(tc2),std::placeholders::_1);
  pl.addStage(flamda2,"stage2");

  pl.addStage(&TestClass2::testFunc,tc2,"stage3");

  pl.addStage(testFunc,"stage4");

  pl.addStage([&](PipelineState &state) { tc2.testFunc(state); },"stage5");

  //pl.addStage([&]() { Pipeline::State state; tc2.testFunc(state); } );

//  std::function<void()> fptr = std::bind(&TestClass1::testFunc2,tc1);
//  fptr();
//  fptr = std::bind(&TestClass2::testFunc2,tc2);

}

REGISTER_TEST("Pipeline","Test 3")
{
  Pipeline<> pl;

  pl.addStage(testFuncNoSetState1,"stage1");
  pl.addStage(testFuncNoSetState2,"stage2");

  pl.setFlag(PipelineFlags::AutoResetState).setFlag(PipelineFlags::AutoSetNextStage);

  pl.runPass();


}

} /* namespace lul */
} /* namespace oslc */
