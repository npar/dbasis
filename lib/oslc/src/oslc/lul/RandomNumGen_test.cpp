/*
 * RandomNumGen_test.cpp
 *
 *  Created on: May 25, 2018
 *      Author: admin
 */

#include <oslc/lul/RandomNumGen.h>

#include<oslc/lul/TestHarness.h>

namespace oslc {
namespace lul {

REGISTER_TEST("RandomNumGen", "test 1")
{
  // declare random num generator of integers
  oslc::lul::RNG<int> rng;
  // obtain a seed from the system clock
  rng.seed();
  // set range of numbers
  int minRange = 1;
  int maxRange = 5;
  rng.setRange(minRange,maxRange);
  for(int i=0;i<10;i++)
  {
    int value  = rng();
    LOG_TEST_MSG(value);
    TEST_EXP2(value>=minRange and value<=maxRange,value);
  }

}

REGISTER_TEST("RandomNumGen", "test 2")
{
  // declare random num generator of integers
  oslc::lul::RNG<float> rng;
  // obtain a seed from the system clock
  rng.seed();
  // set range of numbers
  int minRange = 1;
  int maxRange = 5;
  rng.setRange(minRange,maxRange);
  for(int i=0;i<10;i++)
  {
    float value  = rng();
    LOG_TEST_MSG(value);
    TEST_EXP2(value>=minRange and value<=maxRange,value);
  }

}

REGISTER_TEST("RandomNumGen", "test 3")
{
  // declare random num generator of integers
  oslc::lul::RNG<int> rng;
  // obtain a seed from the system clock
  rng.seed();
  // set range of numbers
  int minRange = 1;
  int maxRange = 5;
  for(int i=0;i<10;i++)
  {
    int value  = rng(minRange,maxRange);
    LOG_TEST_MSG(value);
    TEST_EXP2(value>=minRange and value<=maxRange,value);
  }

}

} /* namespace lul */
} /* namespace oslc */




