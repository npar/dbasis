/*
 * Option.h
 *
 *  Created on: Dec 3, 2017
 *      Author: admin
 */

#ifndef OSLC_LUL_OPTION_H_
#define OSLC_LUL_OPTION_H_

#include <map>
#include <sstream>
#include <string>
#include <memory>

#include <oslc/lul/StringParser.h>
#include <oslc/lul/Log.h>

#include <oslc/lul/Misc.h>


namespace oslc {
namespace lul {


class OptionBase
{
public:
    typedef std::shared_ptr<OptionBase> Ptr;

	virtual OptionBase::Ptr clone() { return NULL; }//= 0;
	virtual bool setValueFromString(std::string strVal) { return false; } //= 0;
	//virtual bool setValueFromString(const char *strVal) = 0;
	virtual ~OptionBase() {}
};

class DefaultOptionData: public OptionBase
{
public:
	DefaultOptionData(std::string name,  std::string desc): name(name), desc(desc) {}
	DefaultOptionData(const DefaultOptionData &other)
    {
	  name = other.name;
	  desc = other.desc;
    }
	// data
	std::string name;
	std::string desc;
};

template< class T, class S>
class Option : public S // S -> OptionBase
{

static_assert(
	        std::is_base_of<OptionBase, S>::value,
	        "S must be a descendant of OptionBase"
	    );

public:
	  Option(const Option &opt): S(opt), value(opt.value)
	  {
	  }

	  Option(const S &s,  T value):
		  S(s), value(value)
	  {
	  }

	  virtual typename OptionBase::Ptr clone() { return (typename OptionBase::Ptr)new Option<T,S>(*this); }

	  T getVal()
	  {
		  return value;
	  }

	  bool setValueFromString(std::string strVal)
	  {
		  return setValueFromString<T>(strVal);
	  }

	  template<class Q>
	  typename std::enable_if<is_string<Q>::value,bool>::type
	  setValueFromString(std::string strVal)
	  {
		  value = strVal;
		  return true;
	  }

	  //
	  // use SFINAE to find out if string stream can handle string to value conversion
	  //
	  template<typename Q>
	  auto toValue(std::stringstream &strStream, Q &q, bool)
	  -> decltype(strStream >> q, bool())
	  {
		  strStream>>value;
		  return !strStream.fail();
	  }
	  template<typename Q>
	  auto toValue(std::stringstream &strStream, Q &q, int)
	  -> bool
	  {
		  return false;
	  }
	  ////////////////////////////////////

	  template<class Q>
	  typename std::enable_if<!is_vector<Q>::value && !is_string<Q>::value,bool>::type
	  setValueFromString(std::string strVal)
	  {
		  std::stringstream strStream(strVal);
		  return toValue(strStream,value,true);
	  }

	  template<class Q>
	  typename std::enable_if<is_vector<Q>::value,bool>::type
	  setValueFromString(std::string strVal)
	  {
			std::stringstream strStream(strVal);
			char token;
			while(strStream>>token && !strStream.eof())
			{
			  if(!isdigit(token))
				  continue;
			  else
			  {
				  strStream.putback(token);
				  int num;
				  if(strStream>>num)
					  value.push_back(num);
			  }
			}
			return !strStream.bad();
	  }

	  void setVal(T value_)
	  {
		  value = value_;
	  }

	  virtual ~Option() {}

	  // data
	  T value;
};

template<class S>
class OptionContainer
{
	static_assert(
		        std::is_base_of<OptionBase, S>::value,
		        "S must be a descendant of OptionBase"
		    );
public:
      typedef typename std::shared_ptr<S> OptionBasePtr;

	  typedef std::map<std::string, OptionBasePtr> OptionMap;

	  template<class T> using OptionPtr = std::shared_ptr<Option<T,S> >;

	  bool addOption(std::string name, OptionBasePtr optBasePtr)
	  {
		  m_options.insert(typename OptionMap::value_type(name,optBasePtr));
		  return true;
	  }

	  bool addOptionSynonym(std::string name, std::string synonym)
	  {
		  OptionBasePtr optBasePtr = m_options[name];
		  if(optBasePtr==NULL)
		  {
			  return false;
		  }
		  else
		  {
			  m_options.insert(typename OptionMap::value_type(synonym,optBasePtr));
			  return true;
		  }
	  }

	  typedef std::map< OptionBasePtr,std::vector<std::string> > SynonymGroups;

	  bool getSynonymKeyGroups(SynonymGroups &sg)
      {
		  for(typename OptionMap::const_iterator itr = m_options.begin(); itr!=m_options.end(); itr++)
		  {
			  sg[itr->second].push_back(itr->first);
		  }

		  return true;
      }

	  // validate that option name is valid and option type matches requested type
	  // Note: we use RTTI to validate types match.
	  //       Problem example: avoid a case where option value type(T) is a string but requested value type(T) is an integer
	  template <class T> OptionPtr<T> tryGetOptPtr(std::string name)
	  {
		  try
		  {
		  OptionBasePtr optBasePtr = m_options[name];
		  // first check that option exists
		  if(optBasePtr==NULL)
			  throw std::invalid_argument("Unknown configuration option: " + name );
		  // validate that cast is legal using RTTI
		  OptionPtr<T> optPtr = std::dynamic_pointer_cast<Option<T,S> > (optBasePtr);
		  // g++ complains when comparing to NULL instead of nullptr
		  if(optPtr==nullptr)
			  throw std::invalid_argument("Illegal configuration option type: " + name  + " is not of type " + typeid(T).name());
		  return optPtr;
		  }
		  catch(...)
		  {
	    	  LOG_ERROR("tryGetOptPtr exception: "<<name);
	    	  throw; // re throw error
	      }
	  }

	  bool isValidOption(std::string name)
	  {
		  OptionBasePtr optBasePtr = m_options[name];
		  return optBasePtr!=NULL;
	  }

      template <class T> OptionPtr<T> getOpt(std::string name)
	  {
    	  OptionPtr<T> optPtr= tryGetOptPtr<T>(name);
		  return optPtr;
	  }

	  template <class T> T getVal(std::string name)
	  {
		  return getOpt<T>(name)->value;
	  }

	  template <class T> void setVal(std::string name, T value)
	  {
		  OptionPtr<T> optPtr= tryGetOptPtr<T>(name);
		  optPtr->value = value;
	  }

	  void copyFrom(const OptionContainer &src)
	  {
		  for(typename OptionMap::const_iterator itr = src.m_options.begin(); itr!=src.m_options.end(); itr++)
		  {
			  OptionBasePtr optBasePtr = std::dynamic_pointer_cast<S>(itr->second->clone());
			  m_options.insert(typename OptionMap::value_type(itr->first,optBasePtr));
		  }
	  }

	  bool loadFromFile(const char *srcFile)
	  {
          using namespace oslc::lul;
          using namespace std;

			StringParser strParser;
			if(!strParser.loadDataFromFile(srcFile))
				return false;

			stringstream sStream;
			while(strParser.getNextTokens(sStream))
			{
				//string varType;
				//sStream>>varType;
				string varName;
				sStream>>varName;
				//if(varType=="int")
				//addOption(varType)

				//Option<T> *optPtr= tryGetOptPtr<T>(name);

				OptionBasePtr optBasePtr = m_options[varName];
			    if(optBasePtr==NULL)
			    {
		    	  LOG_ERROR("Bad option name: "<<varName);
		    	  break;
			    }

				string varVal;
				// we want to read spaces as well i.e. the whole line up to new line
				//sStream>>varVal;
				std::getline(sStream,varVal);

			    if(!optBasePtr->setValueFromString(varVal))
			    {
		    	  LOG_ERROR("Bad option value: "<<varName<<" : "<<varVal);
		    	  break;
			    }
			}

			return true;
	  }

	 // the map container
	 OptionMap m_options;
};

} /* namespace lul */
} /* namespace oslc */

#endif /* OSLC_LUL_OPTION_H_ */
