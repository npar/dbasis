/*
 * Time_test.cpp
 *
 *  Created on: May 11, 2018
 *      Author: admin
 */

#include<string>
using namespace std;

#include <oslc/lul/TestHarness.h>

#include <oslc/lul/Time.h>

namespace oslc {
namespace lul {

REGISTER_TEST("Time","Test 1")
{

	LOG_WARN("add tests here");
}

REGISTER_TEST("Time","Test 2")
{
    //TagTimer<WallTimer,TimePointHiRes> tagT;
    //TagTimer<WallTimer> tagT;
    TagWallTimer tagT;


    tagT.tag("P1");
    cout<<"123"<<endl;
    tagT.tag("P!");
    tagT.tag("P3");
    //LOG_INFO("time between points: "<<tagT.diffSec("\0","P1"));
    LOG_INFO("time between points: "<<tagT.diffSec("P1","P!"));
    LOG_INFO("time between points: "<<tagT.diffSec("P!","P3"));
    LOG_INFO("time from beginning: "<<tagT.elapsedSecFromStart("P1"));
    LOG_INFO("time from beginning: "<<tagT.elapsedSecFromStart("P!"));
    LOG_INFO("time from beginning: "<<tagT.elapsedSecFromStart("P3"));

    LOG_INFO("time between now and tag: "<<tagT.diffSec("P1"));
    LOG_INFO("time between time point and tag: "<<tagT.diffSec("P1",tagT["P3"]));
    LOG_INFO("time between time point and tag: "<<tagT.diffSec("P3",tagT["P1"]));

}

} /* namespace lul */
} /* namespace oslc */
