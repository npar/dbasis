/**
   Test cases for RS

   Copyright Vera-Licona Research Group (C) 2016
   Author: Andrew Gainer-Dewar, Ph.D. <andrew.gainer.dewar@gmail.com>

   This file is part of MHSGenerationAlgorithms.

   MHSGenerationAlgorithms is free software: you can redistribute it
   and/or modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   MHSGenerationAlgorithms is distributed in the hope that it will be
   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
**/

#include "catch.hpp"
#include "rs.hpp"

TEST_CASE ("RS: Simple hypergraph, full enumeration") {
    agdmhs::RSAlgorithm alg (0, 0);

    agdmhs::Hypergraph H ("example-input.dat");
    agdmhs::Hypergraph T = alg.transversal(H);

    REQUIRE(T.num_verts() == 6);
    REQUIRE(T.num_edges() == 5);
}

TEST_CASE ("RS: Simple hypergraph, cutoff enumeration") {
    agdmhs::RSAlgorithm alg (0, 1);

    agdmhs::Hypergraph H ("example-input.dat");
    agdmhs::Hypergraph T = alg.transversal(H);

    REQUIRE(T.num_verts() == 6);
    REQUIRE(T.num_edges() == 0);
}

TEST_CASE ("RS: hypergraph with int int vec constructor") {
    agdmhs::RSAlgorithm alg (0, 0);

    std::vector<std::vector<int>> edgeVec;
    edgeVec.push_back(std::vector<int>({1,2,3}));
    edgeVec.push_back(std::vector<int>({1,2,3,4}));
    edgeVec.push_back(std::vector<int>({1,3,4}));

    agdmhs::Hypergraph H (edgeVec);
    agdmhs::Hypergraph T = alg.transversal(H);

    REQUIRE(T.num_verts() == 5);
    REQUIRE(T.num_edges() == edgeVec.size());

    T.write_to_stream(std::cout);
    std::cout<<T;
}
