/*
 * test-mhsa.cpp
 *
 *  Created on: Nov 10, 2018
 *      Author: admin
 */

#include "catch.hpp"

#include "mhsa.h"

TEST_CASE ("mhsa: external lib use") {

    std::vector<std::vector<int>> edgeVec;
    edgeVec.push_back(std::vector<int>({1,2,3}));
    edgeVec.push_back(std::vector<int>({1,2,3,4}));
    edgeVec.push_back(std::vector<int>({1,3,4}));

    edgeVec = ::runMinHitSetAlgo(edgeVec,"rs");


    REQUIRE(edgeVec.size() == 3);
}



