/*
 * mhsa.cpp
 *
 *  Created on: Nov 10, 2018
 *      Author: admin
 */

#include "rs.hpp"
#include "mmcs.hpp"
#include "bm.hpp"

#include "mhsa.h"

#include <thread>
#include <memory>

//std::
typedef std::shared_ptr<agdmhs::MHSAlgorithm> MHSAlgoSPtr;

MHSAlgoSPtr createAlgo(const std::string &algoType, const size_t HWThreadCount, unsigned cutOffSize)
{
	if(algoType=="rs")
	  return std::make_shared<agdmhs::RSAlgorithm>(HWThreadCount, cutOffSize);
	else
	if(algoType=="mmcs")
	  return std::make_shared<agdmhs::MMCSAlgorithm>(HWThreadCount, cutOffSize);
	if(algoType=="bm")
	  return std::make_shared<agdmhs::ParBMAlgorithm>(HWThreadCount);
	//if(algoType=="shd")
	 // return new agdmhs::SHDAlgorithm;
	else
	{
		throw std::runtime_error(algoType + " is not a supported minimal hitting set algorithm!");
		//return nullptr;
	}
}


HyperGraphPortableData runMinHitSetAlgo(const HyperGraphPortableData &data, std::string algoType, unsigned cutOffSize)
{
	const size_t HWThreadCount = std::thread::hardware_concurrency();

	MHSAlgoSPtr algoPtr = createAlgo(algoType,HWThreadCount,cutOffSize);

    //agdmhs::RSAlgorithm alg (HWThreadCount, 0);
    //agdmhs::MMCSAlgorithm alg (HWThreadCount, 0);
    //agdmhs::ParBMAlgorithm alg(HWThreadCount);

    //std::vector<std::vector<int>> edgeVec;
    //edgeVec.push_back(std::vector<int>({1,2,3}));
    //edgeVec.push_back(std::vector<int>({1,2,3,4}));
    //edgeVec.push_back(std::vector<int>({1,3,4}));

    agdmhs::Hypergraph H (data);
    agdmhs::Hypergraph T = algoPtr->transversal(H);

    //T.write_to_stream(std::cout);
    //std::cout<<T;
    HyperGraphPortableData hg;
    T.write_to_vec(hg);
    return hg;
}


