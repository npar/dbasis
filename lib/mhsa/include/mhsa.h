/*
 * mhsa.h
 *
 *  Created on: Nov 10, 2018
 *      Author: admin
 */

#ifndef INCLUDE_MHSA_H_
#define INCLUDE_MHSA_H_

#include<vector>

typedef std::vector<std::vector<int>> HyperGraphPortableData;

HyperGraphPortableData runMinHitSetAlgo(const HyperGraphPortableData &data, std::string algoType, unsigned cutOffSize = 0);

#endif /* INCLUDE_MHSA_H_ */
