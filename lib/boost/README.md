# Boost dependency notes
  1. The mhsa library depends on the boost library ([see mhsa lib](../mhsa/README.md))
  2. This folder contains the files needed to dynamically link to the boost library (see lib folder)
  3. To rebuild libmhsa from source make sure to set the BOOST_ROOT environment variable to a valid boost installation (see mhsa Makefile for more details)
  