Using input file: input.txt\Small_set_1_6x7
Using minSupport: 0
0.00 Reducing table.
0.00 Equivalent columns for reduced table in original table starting from 1
1 2 4 5 6 7 
 Equivalent columns in original table starting from 1, blank means null set 
3<=> 1 
case 2: The row: 6 is deleted, it is identical to 1 row 
0.00 Creating row comparison table.
0.00 Creating column comparison table.
0.00 Creating up and down arrows.
0.00 std::vector<Implication> FindDBasis 
0.00 Table::getFullBinaryBasis 
0.00 Table::getFullBinaryBasis done.
0.00 Table::getDFullNonBinaryBasis FindDBasis 
0.00 Calling shd.
Producing hypergraph with a parameter siz: 5.
0.00 Returned from shd.
0.00 readDualToImplication start.
0.00 Counting data points.
0.00 Count finished. Datapoints 3. Sets 1.
0.00 Read back 0 data points.
0.00 readDualToImplication finish.
0.00 Implication size: 1 Starting reduction.
0.00 Reduction finished.
0.00 Calling shd.
Producing hypergraph with a parameter siz: 4.
0.00 Returned from shd.
0.00 readDualToImplication start.
0.00 Counting data points.
0.00 Count finished. Datapoints 2. Sets 1.
0.00 Read back 0 data points.
0.00 readDualToImplication finish.
0.00 Implication size: 1 Starting reduction.
0.00 Reduction finished.
0.00 diff s d0
0.00 Table::getDFullNonBinaryBasis FindDBasis done
0.00 std::vector<Implication> FindDBasis done. 
0.00 0;1 -> 5 ; support = 3; RealSupport = 4; rows = 1, 2, 4, 6, 
0.00 1;2 -> 5 ; support = 2; RealSupport = 2; rows = 2, 5, 
0.00 2;4 -> 6 ; support = 3; RealSupport = 3; rows = 3, 4, 5, 
0.00 3;2 -> 7 ; support = 2; RealSupport = 2; rows = 2, 5, 
0.00 4;4 5 7 -> 2 ; support = 1; RealSupport = 1; rows = 5, 
0.00 5;2 6 -> 4 ; support = 1; RealSupport = 1; rows = 5, 
Total Support(column->it's total support)
1 -> 0
2 -> 0
3 -> 0
4 -> 0
5 -> 0
6 -> 0
7 -> 0
Closing output file