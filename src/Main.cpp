#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstdlib>

#include "Table.h"
#include "Implication.h"
#include "SetOperations.h"

#include "DataExchange.h"

// utility functions
#include "Util.h"

// internal application testing
#include "Tests.h"

// printer filter
#include "Printer.h"

// !!! configuration and command line options
#include <oslc/lul/CmdLineOptions.h>
#include <oslc/lul/ConfigOptions.h>
#include <oslc/lul/Version.h>
#include <oslc/lul/MathUtil.h>
using namespace oslc::lul;
////////
#include "Configuration.h"
////////
// for random numbers
#include <chrono>
#include <random>

// mpi
#include <oslc/mpi/MPI.h>
using namespace oslc::mpi;
//
//// serialization misc
//#include <oslc/lul/MemBuffer.h>
using namespace oslc::lul;
//using namespace oslc::ipc;

#include "QueueManager.h"



// Not in use after version 0.0.4b
//void withConfidence(std::vector<std::vector<char> > matrix, int minSupport, int tableRequestedColumn, bool reduceMode, ostream &outputStream)
//{
//    vector< vector<int> > allResults = vector< vector<int> >();
//
//    // !!!  11.6.17 Oren temp fix: need to fix loop -> size should be a parameter????
//    int IterCount = matrix.size();
//    //for (int i=-1;i<2;i++) {
//    for (int i=-1;i<IterCount;i++) {
//        vector< vector<char> > m2 = matrix;
//        //Erase by setting all to 0
//        if (i > -1) {
//            for (int j=0;j<m2[i].size();j++) m2[i][j] = '0';
//        }
//        // create table with min support set
//        // !!!  11.7.17 Oren bug fix: m2 is not used?
//        //Table test2(matrix,minSupport);
//        Table test2(m2,minSupport,tableRequestedColumn,reduceMode);
//        vector<Implication> currImplications = test2.FindDBasis();
//        vector< vector<int> > newResults = test2.getNewResults(allResults, currImplications);
//        if (i == -1) allResults = newResults;
//
//        //print
//        if (newResults.size() > 0)
//        {
//        	outputStream << "New Rules for " << i << "\n";
//        	for (int s=0;s<newResults.size();s++)
//        	{
//        		for (int t=0;t<newResults[s].size()-1;t++)
//        			if (newResults[s][t] == -1)
//        				outputStream << "-> ";
//        			else
//        				outputStream << newResults[s][t] << " ";
//        		outputStream << "support = " << newResults[s][newResults[s].size()-1]<< "\n";
//        		// !!! !!! 10.28.17 oren change: do not buffer results for now to help debug crashes
//        		outputStream.flush();
//        	}
//        	//endOf print
//        }
//        else
//        	outputStream << "No new Rules for " << i << "\n";
//
//    }
//
//    //myfile.close();
//}

inline void printHeader(std::string txt, int filter = Printer::Allways )
{
	PRINT_RAW(filter,"***"<<endl);
	PRINT_RAW(filter,"*** "<<txt<<endl);
	PRINT_RAW(filter,"***"<<endl);
}

bool withConfidenceV2(std::vector<std::vector<char> > matrix, int minSupport, int tableRequestedColumn, bool reduceMode, const vector<int> &removeRowList, DataExchange &de, ostream &outputStream)
{
    //vector< vector<int> > allResults = vector< vector<int> >();

    //vector< vector<char> > m2 = matrix;
    //Erase by setting all to 0
    for(int i=0;i<removeRowList.size();i++)
    {
    	int rowToDel = removeRowList[i];
    	for (int j=0;j<matrix[rowToDel].size();j++)
    		matrix[rowToDel][j] = '0';
    }
    try // 1.9.17 oren ->table constructor can fail !!!
    {
    // create table with min support set
    Table tbl(matrix,minSupport,tableRequestedColumn,reduceMode);
    //Implications currImplications = tbl.FindDBasis();
    de.imps = tbl.FindDBasis();
    // convert implications to original table coordinates
    //tbl.mapToOriginal(currImplications);
    tbl.mapToOriginal(de.imps);
    de.equivMap = tbl.getEquivalentColumns();
    //return currImplications;
    return true;
    }
    catch(...)
	{
    	// return empty implication set
    	//return vector<Implication>();
    	return false;
	}
    //vector< vector<int> > newResults = test2.getNewResults(allResults, currImplications);
   //if (i == -1) allResults = newResults;
}

typedef vector<vector<int> > CombinationTable;
//static int
//math::Int2DVector vect2D = math::genCombinationList(5,3);


void generateUniqueCombinations(CombinationTable &permTbl, vector<int> numList, int combSize, int combCount)
{
	// construct a trivial random generator engine from a time-based seed:
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator (seed);

	//std::uniform_int_distribution<int> distribution(0,numList.size()-1);
	// check if we are asked for combSize bigger then total amount of numbers
	if(combSize>numList.size())
	  combSize = numList.size();
	// check if we are asked for more combinations than available numbers or no combination count
	if( (combCount==0) || (combSize * combCount) > numList.size())
	  combCount = numList.size() / combSize + ( (numList.size() % combSize)>0 ? 1 : 0 );

	permTbl.resize(combCount);
	for(int i=0;i<combCount;i++)
	{
		// handle last comb group size -> might be smaller then requested
		if(combSize>numList.size())
		  combSize=numList.size();
		//combSize = (combSize<=numList.size()) ? combSize : numList.size();
		permTbl[i].resize(combSize);
		for(int j=0;j<combSize;j++)
		{
			int index = generator() % numList.size();//distribution(generator);
			permTbl[i][j] = numList[index];
			numList.erase(numList.begin()+index);
		}
	}
}

void generateCombinations(CombinationTable &permTbl, vector<int> numList, int combSize, int combCount)
{
	// construct a trivial random generator engine from a time-based seed:
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator (seed);

	//std::uniform_int_distribution<int> distribution(0,numList.size()-1);
	// check if we are asked for combSize bigger then total amount of numbers
	if(combSize>numList.size())
	  combSize = numList.size();

	// generate all combinations (N/GS)
    math::Int2DVector allComb = math::genCombinationList(numList.size(),combSize);

	// check if we are asked for more combinations than available numbers or no combination count
	if( (combCount==0) || combCount > allComb.size())
	  combCount = allComb.size();

	permTbl.resize(combCount);
	for(int i=0;i<combCount;i++)
	{
		permTbl[i].resize(combSize);
		int combIndex = generator() % allComb.size();//distribution(generator);
		for(int j=0;j<combSize;j++)
		{
			permTbl[i][j] = numList [ allComb[combIndex][j] ];
		}
		allComb.erase(allComb.begin()+combIndex);
	}
}

#define WARN_COMB_EXPLOSION 1024*1024

int generateCombinationsV2(CombinationTable &permTbl, vector<int> numList, int combSize, int combCount)
{
	// construct a trivial random generator engine from a time-based seed:
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator (seed);

	//std::uniform_int_distribution<int> distribution(0,numList.size()-1);
	// check if we are asked for combSize bigger then total amount of numbers
	if(combSize>numList.size())
	  combSize = numList.size();

	// generate all combinations (N/GS)
    //math::Int2DVector allComb = math::genCombinationList(numList.size(),combSize);
	unsigned long long numOfPossibleComb = math::comb(numList.size(),combSize);
	// sanity check
	//if(numOfPossibleComb>WARN_COMB_EXPLOSION)

	// check if we are asked for more combinations than available numbers or no combination count
	if( (combCount==0) || combCount > numOfPossibleComb)
	  combCount = numOfPossibleComb;

	// count the actual amount of times we had to try in order to produce all the unique combinations
	int tryCount = 0;

	permTbl.resize(combCount);
	int i=0;
	while(i<combCount)
	{
		tryCount++;
		// make sure we have enough size in vec
		permTbl[i].resize(combSize);
		//vector<bool> pickVec(combSize,0);
		// make copy of original group
		vector<int> pickNumList = numList;
		// create a random combination
		for(int j=0;j<combSize;j++)
		{
			int pickIndex = generator() % pickNumList.size();//distribution(generator);
	        permTbl[i][j] = pickNumList[pickIndex];
	        pickNumList.erase(pickNumList.begin()+pickIndex);
		}
		// check if unique
		int j=0;
		while(j<i)
		{
			int foundCount = 0;
			for(int k=0;k<combSize;k++)
				if(std::find(permTbl[j].begin(),permTbl[j].end(),permTbl[i][k])==permTbl[j].end())
					break; // not found, we can check next comb group
				else
					foundCount++;
			// check if all elements found i.e. it is not unique
			if(foundCount==combSize)
				break;
            // next round
			j++;
		}
		// check if all clear
		if(j==i)
			i++;
	}

	return tryCount;
}

// max communication buffer size
#define COMM_BUFFER_SIZE 1024*1024*4

//MPI mpi(MPI::ThreadSupport::Multiple);
// the one and only MPI object
std::unique_ptr<MPI> glblMpiPtr;
//////////////////////////////



void printRulesHelper(std::string msg, const vector<int>& removeRowList, ostream& outputStream)
{
	//outputStream<<"***"<<endl;
	outputStream<<"*** "<<msg;
	size_t size =  removeRowList.size();
	//for(const auto & value: removeRowList)
	for(int i=0;i<size-1;i++)
	{
		// output row number and add 1 to convert from zero based matrix to standard form
		//outputStream << value + 1 << ",";
		outputStream << removeRowList[i] + 1 << ", ";
	}
	// print last one
	if(size)
	  outputStream << removeRowList[removeRowList.size()-1] + 1;
	else
	  outputStream << "???";

	outputStream<<" ***"<<"\n";
	//outputStream<<"***"<<endl;
}

void clientSide(const std::vector<std::vector<char> > &matrix, int minSupport, int tableRequestedColumn, bool reduceMode, const CombinationTable &removeRowTable, ostream &outputStream, bool aggregateRules)
{
	// parallel version
	//MPI mpi(MPI::ThreadSupport::Single);
	MPI::Comm comm;
	//int size = comm.size();
	int rank = comm.rank();
	//std::string hostName = mpi.getHostName();
	//LOG_INFO("host name="<<hostName<<" Size="<<size<<" Rank="<<rank);
	// declare the message exchange queue
	DBasisMsgQueue msgQ(COMM_BUFFER_SIZE,false);
	// start queue
	msgQ.start();


	outputStream<<"Starting client rank "<<rank<<"\n";

    while(msgQ.isActive())
	{
	  // check if we have available messages
	  if(!msgQ.queueInSize())
	  {
	      oslc::lul::WallTime::sleepMilli(10);
		  //std::this_thread::yield();
		  continue;
	  }
	  // remove message from the queue
	  oslc::ipc::Message<DataExchange> msg;
	  msgQ.dequeueIn(msg);
	  outputStream<<rank<<" Received data from "<<msg.src<<"\n";
      // print info
  	  printRulesHelper("Remove row list: ",msg.getData().removeRowList,outputStream);
      // run request
      withConfidenceV2(matrix,minSupport,tableRequestedColumn,reduceMode,msg.getData().removeRowList,msg.editData(),outputStream);
      //Implications &currImplications = de.imps;
      //EquivalenceMap &equivMap = de.equivMap;
      // return results
      //int tag = rank;// use quesry id??
      //comm.send(memStrm.getMemBuff().buff(),COMM_BUFFER_SIZE,MPI_CHAR,0,tag);
      outputStream<<rank<<" Sending reply to "<<msg.src<<"\n";
      msg.dest = msg.src;
      msgQ.enqueueOut(msg);
	}

	outputStream<<"Stopping client rank "<<rank<<"\n";

    // must call join or detach to avoid exception
    msgQ.stop();
}

void serverSide(const std::vector<std::vector<char> > &matrix, int minSupport, int tableRequestedColumn, bool reduceMode, const CombinationTable &removeRowTable, ostream &outputStream, bool aggregateRules)
{
	//waitForDebugger();

	// parallel version
	//MPI mpi(MPI::ThreadSupport::Single);
	MPI::Comm comm;
	//int numOfParallelProcesses = comm.size() - 1;

	// declare the message exchange queue
	DBasisMsgQueue msgQ(COMM_BUFFER_SIZE,true);
	// start queue
	msgQ.start();

	//int rank = comm.rank();
	//std::string hostName = mpi.getHostName();
	//LOG_INFO("host name="<<hostName<<" Size="<<size<<" Rank="<<rank);

	//waitForDebugger();


    // create original table - no internal reduction, this will be used as base -> 1.25.18 -> reduce hack
    Table orgTbl(matrix,minSupport,tableRequestedColumn,reduceMode,false);
    // create aggregated table with min support set
    Table totalTbl(matrix,minSupport,tableRequestedColumn,reduceMode);
    //vector<Implication> origImplications = totalTbl.FindDBasis();
    Implications allImplications = totalTbl.FindDBasis();

    const EquivalenceMap &orgTblEquivalences = totalTbl.getEquivalentColumns();
    outputStream <<"*** Begin print equivalences (original table) ***"<<std::endl;
    totalTbl.prettyprintEquivalences(orgTblEquivalences);
    outputStream <<"*** End print equivalences ***"<<std::endl;

    // aggregate all equivalences in aggregation mode
    EquivalenceMultiMap allEquialences;
    // if we are in aggregateRules mode
    if(aggregateRules)
    	allEquialences.insert(orgTblEquivalences.begin(),orgTblEquivalences.end());

    // print the combined list of implications
    outputStream<<"***"<<endl;
    outputStream<<"*** Printing all implications (original table)"<<endl;
    outputStream<<"***"<<endl;
    totalTbl.prettyprintImplications(allImplications);
    ////////////////////////////////////////////
    // convert implications to original table coordinates
    totalTbl.mapToOriginal(allImplications);

    //vector< vector<int> > allResults;
    //allResults = totalTbl.getNewResults(allResults, origImplications);
    //int idealQeueSize = (numOfParallelProcesses > removeRowTable.size()) ? removeRowTable.size() : numOfParallelProcesses;
    //int pendingCount = 0;
    int i = 0;
    // now for each permutation of rows test and aggregate
    while(true)
    {
		oslc::ipc::Message<DataExchange> msg;

		//const vector<int> &removeRowList = removeRowTable[i];

		// try to fill the queue with messages
    	while(i<removeRowTable.size())
    	{
    		// while queue not full, queue messages for parallel processing
    		msg.editData().removeRowList = removeRowTable[i];
    		msg.dest = oslc::ipc::Message<DataExchange>::AnyAddress;
    		if(msgQ.tryEnqueueOut(msg,0))
    		{
    			printRulesHelper("Server sending remove row list: ",msg.getData().removeRowList,outputStream);
    			i++;
    			continue;
    		}
    		else
    			break;
    	}

    	// check if we have anymore pending messages
    	// if input queue is empty and no more pending messages then we are done
    	if(msgQ.queueInSize()==0 && msgQ.pendingMessages()==0)
    	{
    		PRINT(Printer::Allways, "parallel process complete - all messages accounted for...");
  	        LOG_INFO("Sending abort to clients ...");
    	    msgQ.sendAbort();
    	    //std::this_thread::sleep_for (std::chrono::seconds(1));
  	        //LOG_INFO("Waiting for clients to terminate...");
    	    //while(msgQ.pendingMessages())
    	    //{
    	    oslc::lul::WallTime::sleepMilli(100);
    	    //}
  	        LOG_INFO("Stopping queue...");
    	    msgQ.stop();
    	    LOG_INFO("Abort process");
    		break;
    	}

    	// if we are here, then we still have message/s to process, wait for input queue to receive a message
    	while(msgQ.queueInSize()==0)
    		std::this_thread::yield();

    	PRINT(Printer::Verbose, "queueInSize=="<<msgQ.queueInSize());
    	// if we are here we have some incoming replies
    	//while(msgQ.queueInSize()>0)
    	//{
    	// remove message from the queue
    	msgQ.dequeueIn(msg);
    	//pendingCount--;
    	PRINT(Printer::Verbose, "Server received data from "<<msg.src);
    	//}
    	//if(msg.src==0)
    	  //waitForDebugger();

    	DataExchange &de = msg.editData();
    	printRulesHelper("Remove row list: ",de.removeRowList,outputStream);

    	//Implications currImplications = withConfidenceV2(matrix,minSupport,tableRequestedColumn,reduceMode,removeRowList,outputStream);
    	//DataExchange de;
    	//->withConfidenceV2(matrix,minSupport,tableRequestedColumn,reduceMode,de.removeRowList,de,outputStream);
    	Implications &currImplications = de.imps;
    	EquivalenceMap &equivMap = de.equivMap;
    	// print equivalences
    	PRINT_RAW(Printer::Allways,"*** Begin print new equivalences ***"<<std::endl);
    	totalTbl.prettyprintEquivalences(equivMap);
    	PRINT_RAW(Printer::Allways,"*** End print new equivalences ***"<<std::endl);
    	// select/filter the new implications
    	PRINT_RAW(Printer::Allways,"Begin select new implications"<<std::endl);
    	Implications newImps;
    	allImplications.selectNewFrom(currImplications,newImps);
    	PRINT_RAW(Printer::Allways,"End select new implications"<<std::endl);
    	// if we did not find any new implications
    	//if(currImplications.isEmpty() || currImplications<allImplications)
    	if(newImps.isEmpty())
    	{
    		printRulesHelper("No new rules for removed row list: ",de.removeRowList,outputStream);
    		continue;
    	}
    	// else
    	// we have new implications
    	// print rules
    	std::stringstream strStream;
    	strStream<<"Found "<<newImps.size()<<" new rules for removed row list: ";
    	//printRulesHelper("New rules for removed row list: ");
    	printRulesHelper(strStream.str(),de.removeRowList,outputStream);
    	//totalTbl.prettyprintImplications(currImplications);
    	totalTbl.prettyprintImplications(newImps,false);
    	// aggregate rules
    	//allImplications += currImplications;
    	// if we are in aggregateRules mode
    	if(aggregateRules)
    	{
    		// aggregate equivalences
    		//allEquialences.insert(equivMap.begin(),equivMap.end());
    		insertIfUnique(allEquialences,equivMap);

    		//!!! kira wanted to work with simple aggregation mode first
    		//		// remove the cycles from new implications
    		//	    timestamp_print(); std::cout <<"Begin cycles"<<std::endl;
    		//		EquivalenceMap newEquivs;
    		//		allImplications.handleCycles(newImps,newEquivs);
    		//	    timestamp_print(); std::cout <<"End cycles"<<std::endl;

    		allImplications.add(newImps);
    		// 4.13.18 -> print all implications to make it easier to analyze results
    		printHeader("Printing implications aggregated so far");
    		//totalTbl.prettyprintImplications(allImplications,false);
    		orgTbl.prettyprintImplications(allImplications,false);
    	}
    }
    // record critical points in time
    PRINT(Printer::Verbose,"***** Time stamp *****");
    // eof record
    // if we are in aggregateRules mode
    if(aggregateRules)
    {

    	// print the combined list of equivalences
    	printHeader("Printing all equivalences");
    	orgTbl.prettyprintEquivalences(allEquialences);

    	// print the combined list of implications
    	printHeader("Printing all implications");
    	//totalTbl.prettyprintImplications(allImplications,false);
    	orgTbl.prettyprintImplications(allImplications,false);
    	//  outputStream<<"***"<<endl;
    	//  outputStream<<"*** Reducing all implications"<<endl;
    	//  outputStream<<"***"<<endl;
    	//totalTbl.reduce(tableRequestedColumn,allImplications);// ????
    	orgTbl.reduce(tableRequestedColumn,allImplications);
    	printHeader("Printing all implications reduced");
    	//totalTbl.prettyprintImplications(allImplications,false);
    	orgTbl.prettyprintImplications(allImplications,false);
    }
}

void withConfidenceCombinations(const std::vector<std::vector<char> > &matrix, int minSupport, int tableRequestedColumn, bool reduceMode, const CombinationTable &removeRowTable, ostream &outputStream, bool aggregateRules)
{
  // create original table - no internal reduction, this will be used as base -> 1.25.18 -> reduce hack
  Table orgTbl(matrix,minSupport,tableRequestedColumn,reduceMode,false);
  // create aggregated table with min support set
  Table totalTbl(matrix,minSupport,tableRequestedColumn,reduceMode);
  //vector<Implication> origImplications = totalTbl.FindDBasis();
  Implications allImplications = totalTbl.FindDBasis();

  const EquivalenceMap &orgTblEquivalences = totalTbl.getEquivalentColumns();
  PRINT_RAW(Printer::Allways,"*** Begin print equivalences (original table) ***"<<std::endl);
  totalTbl.prettyprintEquivalences(orgTblEquivalences);
  PRINT_RAW(Printer::Allways,"*** End print equivalences ***"<<std::endl);

  // aggregate all equivalences in aggregation mode
  EquivalenceMultiMap allEquialences;
  // if we are in aggregateRules mode
  if(aggregateRules)
    allEquialences.insert(orgTblEquivalences.begin(),orgTblEquivalences.end());

  // print the combined list of implications
  printHeader("Printing all implications (original table)");
  totalTbl.prettyprintImplications(allImplications);
  ////////////////////////////////////////////
  // convert implications to original table coordinates
  totalTbl.mapToOriginal(allImplications);

  //vector< vector<int> > allResults;
  //allResults = totalTbl.getNewResults(allResults, origImplications);

  // now for each permutation of rows test and aggregate
  for(int i=0;i<removeRowTable.size();i++)
  {
	  const vector<int> &removeRowList = removeRowTable[i];
	  auto printRulesHelper = [&](std::string msg)
	  	{
		  //outputStream<<"***"<<endl;
		  outputStream<<"*** "<<msg;
		  //for(const auto & value: removeRowList)
		  for(int i=0;i<removeRowList.size()-1;i++)
		  {
			  // output row number and add 1 to convert from zero based matrix to standard form
			  //outputStream << value + 1 << ",";
			  outputStream << removeRowList[i] + 1 << ", ";
		  }
		  // print last one
		  outputStream << removeRowList[removeRowList.size()-1] + 1;
		  outputStream<<" ***"<<"\n";
		  //outputStream<<"***"<<endl;
	  	};

	  printRulesHelper("Remove row list: ");

	  //Implications currImplications = withConfidenceV2(matrix,minSupport,tableRequestedColumn,reduceMode,removeRowList,outputStream);
	  DataExchange de;
	  withConfidenceV2(matrix,minSupport,tableRequestedColumn,reduceMode,removeRowList,de,outputStream);
	  Implications &currImplications = de.imps;
	  EquivalenceMap &equivMap = de.equivMap;
	  // print equivalences
	  outputStream <<"*** Begin print new equivalences ***"<<std::endl;
	  totalTbl.prettyprintEquivalences(equivMap);
	  outputStream <<"*** End print new equivalences ***"<<std::endl;
	  // select/filter the new implications
	  PRINT(Printer::Allways,"Begin select new implications");
	  Implications newImps;
	  allImplications.selectNewFrom(currImplications,newImps);
	  PRINT(Printer::Allways,"End select new implications");
	  // if we did not find any new implications
	  //if(currImplications.isEmpty() || currImplications<allImplications)
	  if(newImps.isEmpty())
	  {
		printRulesHelper("No new rules for removed row list: ");
        continue;
	  }
	  // else
	  // we have new implications
	  // print rules
	  std::stringstream strStream;
	  strStream<<"Found "<<newImps.size()<<" new rules for removed row list: ";
	  //printRulesHelper("New rules for removed row list: ");
	  printRulesHelper(strStream.str());
	  //totalTbl.prettyprintImplications(currImplications);
	  totalTbl.prettyprintImplications(newImps,false);
	  // aggregate rules
	  //allImplications += currImplications;
	  // if we are in aggregateRules mode
	  if(aggregateRules)
	  {
		// aggregate equivalences
		//allEquialences.insert(equivMap.begin(),equivMap.end());
		insertIfUnique(allEquialences,equivMap);

		//!!! kira wanted to work with simple aggregation mode first
//		// remove the cycles from new implications
//	    timestamp_print(); std::cout <<"Begin cycles"<<std::endl;
//		EquivalenceMap newEquivs;
//		allImplications.handleCycles(newImps,newEquivs);
//	    timestamp_print(); std::cout <<"End cycles"<<std::endl;

	    allImplications.add(newImps);
	    // 4.13.18 -> print all implications to make it easier to analyze results
	    // print the combined list of implications
		printHeader("Printing implications aggregated so far");
		//totalTbl.prettyprintImplications(allImplications,false);
		orgTbl.prettyprintImplications(allImplications,false);
	  }
  }
  // if we are in aggregateRules mode
  if(aggregateRules)
  {

	  // print the combined list of equivalences
	  printHeader("Printing all equivalences");
	  orgTbl.prettyprintEquivalences(allEquialences);

	  // print the combined list of implications
	  printHeader("Printing all implications");
	  //totalTbl.prettyprintImplications(allImplications,false);
	  orgTbl.prettyprintImplications(allImplications,false);
	  //  outputStream<<"***"<<endl;
	  //  outputStream<<"*** Reducing all implications"<<endl;
	  //  outputStream<<"***"<<endl;
	  //totalTbl.reduce(tableRequestedColumn,allImplications);// ????
	  orgTbl.reduce(tableRequestedColumn,allImplications);
	  printHeader("Printing all implications reduced");
	  //totalTbl.prettyprintImplications(allImplications,false);
	  orgTbl.prettyprintImplications(allImplications,false);
  }

}


void withConfidenceCombinationsParallel(const std::vector<std::vector<char> > &matrix, int minSupport, int tableRequestedColumn, bool reduceMode, const CombinationTable &removeRowTable, ostream &outputStream, bool aggregateRules)
{
	  //MPI mpi(MPI::ThreadSupport::Multiple);
	  //MPI mpi(MPI::ThreadSupport::Serialized);
	  MPI::Comm comm;
	  int size = comm.size();
	  int rank = comm.rank();
	  std::string hostName = glblMpiPtr->getHostName();
	  PRINT(Printer::Verbose,"Host name="<<hostName<<" Size="<<size<<" Rank="<<rank);
	  // run as server or client
	  if(rank==0)
	  {
		  ::serverSide(matrix,minSupport,tableRequestedColumn,reduceMode,removeRowTable,outputStream,aggregateRules);
	  }
	  else
	  {
		  ::clientSide(matrix,minSupport,tableRequestedColumn,reduceMode,removeRowTable,outputStream,aggregateRules);
		  //TODO: release resources?
	  }
}

//Todo: inline with conf thunk function switch between single and parallel...


int main(int argc, char* argv[])
{
	oslc::lul::CPUTimer cpuTimer;
	oslc::lul::WallTimer wallTimer;

	cpuTimer.start();
	wallTimer.start();

	DBasisHDAVersionInfo *verInfo = GET_VERSION_INFO(DBasisHDA);

	//CmdLineOptions::getGlblCmdLineOptions().displayUsageMsg();
	CmdLineOptions cmdLineOptions(CmdLineOptions::getGlblCmdLineOptions());

	if(argc==1 || !cmdLineOptions.parse(argc, argv))
	{
		PRINT(Printer::Allways,"Error reading command line options please fix your command line options and try again!");
		cmdLineOptions.displayUsageMsg();
		return 1;
	}

	//
	// One time configuration initialization
	// TODO: replace cmdline code with configuration class. auto read all values!
	DBHDAConfig::get(&cmdLineOptions);

	// set output verbosity options
	int verbosity = cmdLineOptions.getOpt<int>(VERBOSITY_LEVEL_CMD_OPT_STR)->value;
	if(verbosity==1)
	   Printer::getSysPrinter().setFilter(Printer::Summary);
	else
	if(verbosity==2)
	   Printer::getSysPrinter().setFilter(Printer::Verbose | Printer::Summary);
	else
	{
		PRINT(Printer::Allways,"Error bad output verbosity level "<<verbosity);
		return 1;
	}
	//PRINT(Printer::Verbose,"test");

	CmdLineOptionPtr<bool> versionOpt = cmdLineOptions.getOpt<bool>(VERSION_CMD_OPT_STR);

	if(versionOpt->isAvailable)
	{
		PRINT(Printer::Allways,verInfo->getStdVersionString());
		return 0;
	}

	CmdLineOptionPtr<bool> testOpt = cmdLineOptions.getOpt<bool>(TEST_CMD_OPT_STR);

	if(testOpt->isAvailable)
	{
		PRINT(Printer::Allways,"Running tests...");
		runTests();
		return 0;
	}

	// the one and only MPI object
	//std::unique_ptr<MPI> mpiPtr;
    //////////////////////////////
	// check parallel mode
	int execUnits = 1;
	int execId = 0;
	CmdLineOptionPtr<bool> multiProcess = cmdLineOptions.getOpt<bool>(MULTIPLE_PROCESSES_CMD_OPT_STR);
	if(multiProcess->isAvailable)
	{
		// initialize mpi library
		glblMpiPtr = std::make_unique<MPI>(MPI::ThreadSupport::Multiple);
		MPI::Comm comm;
		execUnits = comm.size();
		execId = comm.rank();
		PRINT(Printer::Allways,"Running in parallel mode ["<<execId<<":"<<execUnits<<"]");
		//MPI mpi(MPI::ThreadSupport::Multiple);
		//return 0;
	}
	bool multiProcessMode = multiProcess->isAvailable;

	CmdLineOptionPtr<bool> oneRowDelOpt = cmdLineOptions.getOpt<bool>(ORD_CMD_OPT_STR);
	if(oneRowDelOpt->isAvailable)
	{
		PRINT(Printer::Allways,"Running in one row deletion mode...");
		//return 0;
	}
	bool oneRowDelMode = oneRowDelOpt->isAvailable;

	CmdLineOptionPtr<bool> reduceCmdLineOpt = cmdLineOptions.getOpt<bool>(REDUCE_CMD_OPT_STR);
	if(reduceCmdLineOpt->isAvailable)
	{
		PRINT(Printer::Allways,"Running in reduce mode...");
		//return 0;
	}
	bool reduceMode = reduceCmdLineOpt->isAvailable;

	CmdLineOptionPtr<std::string> outputFileOpt = cmdLineOptions.getOpt<std::string>(OUTPUT_FILE_CMD_OPT_STR);
	if(outputFileOpt->isAvailable)
	{
		//waitForDebugger();
	    std::string outputFileName = outputFileOpt->getVal();
	    //!!! in paralle mode we need to handle output diffrently !!!
	    if(multiProcessMode)
	    {
	    	MPI::Comm com;
	    	std::stringstream strm;
	    	int hostId = com.rank();
	    	std::string hostName = glblMpiPtr->getHostName();
	    	strm<<hostName<<"-"<<hostId<<"-"<<outputFileName;
	    	outputFileName = strm.str();
	    }

	    PRINT(Printer::Allways,"Using output file: "<<outputFileName);
		freopen (outputFileName.data(),"w",stdout);
		// issue #27 -> store original cmd line for future reference
		PRINT(Printer::Allways,"Using command line: "<<CmdLineOptions::cmdLineToString(argc,argv));
	}

	CmdLineOptionPtr<std::string> inputFileOpt = cmdLineOptions.getOpt<std::string>(INPUT_FILE_CMD_OPT_STR);
	if(!inputFileOpt->isAvailable)
	{
		PRINT(Printer::Allways,"Error missing input file!");
		return 1;
	}

    std::string inputFileName = inputFileOpt->getVal();
    PRINT(Printer::Allways,"Using input file: "<<inputFileName);

	int tableRequestedColumn;
	CmdLineOptionPtr<int> columnOpt = cmdLineOptions.getOpt<int>(COLUMN_CMD_OPT_STR);
	if(columnOpt->isAvailable)
	{
		int userRequestedColumn = columnOpt->getVal();
		PRINT(Printer::Allways,"Using Matrix column: "<<userRequestedColumn);
  	    // handle zero base
		if(DBHDAConfig::get().isZeroBasedMode())
		  tableRequestedColumn = userRequestedColumn;
		else
		  //!!! since the code expects a C zero based column number we need to translate it by subtracting 1
		  tableRequestedColumn = userRequestedColumn - 1;
	}
	else
		tableRequestedColumn = -1;

	int minSupport;
	CmdLineOptionPtr<int> minSupportOpt = cmdLineOptions.getOpt<int>(MIN_SUPPORT_CMD_OPT_STR);
	if(minSupportOpt->isAvailable)
	{
		minSupport = minSupportOpt->getVal();
	}
	else // set to default value if not supplied by user
		minSupport = SUPPORT_LOWER_BOUND_DEFAULT;

    // always print the minimum support in use!
	PRINT(Printer::Allways,"Using minSupport: "<<minSupport);

	// always print if using zero base
	PRINT(Printer::Allways,"Using zero base: "<<std::boolalpha<<DBHDAConfig::get().isZeroBasedMode());

	// always print mp/mt
	if(multiProcessMode)
	{
	  PRINT(Printer::Allways,"Using parallel process mode: "<<std::boolalpha<<true<<" ["<<execId<<":"<<execUnits<<"]");
	}
	else
	{
	  PRINT(Printer::Allways,"Using parallel process mode: "<<std::boolalpha<<multiProcessMode);
	}
	bool multiThreadMode = cmdLineOptions.getOpt<bool>(MULTIPLE_THREADS_CMD_OPT_STR)->isAvailable;
    PRINT(Printer::Allways,"Using parallel thread mode: "<<std::boolalpha<<multiThreadMode<<" [Max="<<thread::hardware_concurrency()<<"]");

	// print code base version
	PRINT(Printer::Allways,"Using code base: "<<verInfo->getStdVersionString());

	///////////
	CmdLineOptionPtr<vector<int> > combGrpOpt = cmdLineOptions.getOpt<vector<int> >(COMB_GROUP_CMD_OPT_STR);
    //vector<int> v = permGrpOpt->value;

	///////////////
    //std::vector<std::vector<char> > * m = readTable(inputFileName);
    std::vector<std::vector<char> > *matrixPtr = readTable(inputFileName);
    // if in combination group mode
    if(combGrpOpt->isAvailable)
    {
    	// first generate combinations according to list
    	CombinationTable removeRowTable;
    	vector<int> numList = combGrpOpt->value;
    	// 3.4.18 -> Justin request for all option #29
    	if(numList.empty())
    	{
    	  int rowNum = 0;
    	  numList.resize(matrixPtr->size());
          std::for_each(numList.begin(),numList.end(),[&rowNum](int &val) { val = rowNum++; });
    	}
    	else
    	{
      	  // handle zero base
		  //!!! since the code expects a C zero based row numbers we need to translate it by subtracting 1
   		  if(!DBHDAConfig::get().isZeroBasedMode())
            std::for_each(numList.begin(),numList.end(),[](int &val) { val--; });
    	}
        // validate input
    	int combSize = cmdLineOptions.getOpt<int>(COMB_GROUP_SIZE_CMD_OPT_STR)->value;
    	if(combSize==0)
    	{
    		PRINT(Printer::Allways,"Error missing combination group size option: "<<COMB_GROUP_SIZE_CMD_OPT_STR);
    		return 1;
    	}
        int combCount = cmdLineOptions.getOpt<int>(COMB_GROUP_COUNT_CMD_OPT_STR)->value;
    	if(combCount==0)
    	{
    		PRINT(Printer::Allways,"Note missing combination group count option: "<<COMB_GROUP_COUNT_CMD_OPT_STR<<". Using default count");
    		//return 1;
    	}
    	// 4.23.18 -> allow for optional aggregation
        bool aggregate = cmdLineOptions.getOpt<bool>(AGGREGATE_CMD_OPT_STR)->value;

    	//generateCombinations(removeRowTable,numList,combSize,combCount);
    	int res = generateCombinationsV2(removeRowTable,numList,combSize,combCount);
    	PRINT(Printer::Allways,"Using combinations(requested(N:cgs:cgc)-> generated(cgsXcgc)[debug try count] "<<numList.size()<<":"<<combSize<<":"<<combCount<<" -> "<<combSize<<"X"<<removeRowTable.size()<<"["<<res<<"]");
    	PRINT_RAW(Printer::Allways,"\n---> RunningWithRowDeletionCombinations\n");
    	// call combinations with all rows, ordered
    	try
    	{
    	  if(multiProcessMode)
        	  withConfidenceCombinationsParallel(*matrixPtr,minSupport,tableRequestedColumn,reduceMode,removeRowTable,cout,aggregate);
    	  else
    	      withConfidenceCombinations(*matrixPtr,minSupport,tableRequestedColumn,reduceMode,removeRowTable,cout,aggregate);
    	}
    	catch (...)
    	{
    	  // handle crash issue #23 - do nothing for now - can happen when initial column request fails in table constructor
    	  // TODO: move Table constructor exception.
    	  // need to refactor old Table constructor code. reduce and other functions should not happen there.
    	  // not a good idea to abort in the constructor !!!
    	}
    	PRINT_RAW(Printer::Allways,"\n---> EOF RunningWithRowDeletionCombinations\n");

    }
    else
    // if we are not in one row delete mode run reduce and FindDBasis once
    if(!oneRowDelMode)
    {
    	Table test(*matrixPtr,minSupport,tableRequestedColumn,reduceMode);
    	std::vector<Implication> implications = test.FindDBasis();
    	test.prettyprintImplications(implications);
    }
    else // in one row delete mode
    {
    	// RunningWithRowDeletions
    	PRINT_RAW(Printer::Allways,"\n---> RunningWithOneRowDeletions\n");
    	// !!! oren change 1.21.18 -> move to new way of doing things
    	//withConfidence(*matrixPtr,minSupport,tableRequestedColumn,reduceMode,cout);
    	CombinationTable removeRowTable(matrixPtr->size());
    	//std::iota(
    	// generate all row numbers
    	int i= 0;
    	for(auto &x: removeRowTable)
    		x.push_back(i++);
    	// call combinations with all rows, ordered
    	try
    	{
      	  if(multiProcessMode)
        	withConfidenceCombinationsParallel(*matrixPtr,minSupport,tableRequestedColumn,reduceMode,removeRowTable,cout,false);
      	  else
    	    withConfidenceCombinations(*matrixPtr,minSupport,tableRequestedColumn,reduceMode,removeRowTable,cout,false);
    	}
    	catch (...)
    	{
    	  // handle crash issue #23 - do nothing for now - can happen when initial column request fails in table constructor
    	  // TODO: move Table constructor exception.
    	  // need to refactor old Table constructor code. reduce and other functions should not happen there.
    	  // not a good idea to abort in the constructor !!!
    	}
    	//endOf RunningWithRowDeletions
    	PRINT_RAW(Printer::Allways,"\n---> EOF RunningWithOneRowDeletions\n");
    }

    // print program stats
    wallTimer.stop(); cpuTimer.stop();
    PRINT(Printer::Allways,"**** Begin Timing Information  ****");
    //cout<<"timestamp=";timestamp_print();cout<<"\n";
    PRINT(Printer::Allways,"cpuTimer="<<cpuTimer.elapsedSec());
    PRINT(Printer::Allways,"wallTimer="<<wallTimer.elapsedSec());
    PRINT(Printer::Allways,"**** End Timing Information  ****");

    delete matrixPtr;

	if(outputFileOpt->isAvailable)
	{
		PRINT(Printer::Allways,"Closing output file");
		fclose (stdout);
	}
}
