/*
 * Printer.h
 *
 *  Created on: Apr 19, 2020
 *      Author: admin
 */

#ifndef PRINTER_H
#define PRINTER_H

#include <iostream>
#include <iomanip>
#include <string>

#include <mutex>

#include <oslc/lul/Time.h>

class IOSStateSaver
{
public:
	IOSStateSaver(std::ios& iostrm): m_temp(NULL), m_org(iostrm)
    {
		// -> note this seems like an overkill, we will save each manually instead
		// https://stackoverflow.com/questions/1513625/c-how-to-reset-the-output-stream-manipulator-flags
		//m_temp.copyfmt(iostrm);
		m_temp.flags(m_org.flags());
		// NOTE: width does not seem to be sticky, should we bother saving it? ->
		// https://stackoverflow.com/questions/1532640/which-iomanip-manipulators-are-sticky
		m_temp.width(m_org.width());
		m_temp.precision(m_org.precision());
		m_temp.tie(m_org.tie());
		m_temp.fill(m_org.fill());

    }
	~IOSStateSaver()
	{
		// -> note this seems like an overkill, we will restore each manually instead
		//m_org.copyfmt(m_temp);
		m_org.flags(m_temp.flags());
		m_org.width(m_temp.width());
		m_org.precision(m_temp.precision());
		m_org.tie(m_temp.tie());
		m_org.fill(m_temp.fill());
	}
protected:
	std::ios m_temp;
	std::ios &m_org;
};

class Printer
{
public:
	enum CommonFilters
	{
		Never = 0,
		Summary = 0x1,
		Detail1 = 0x2, // | Summary,
		Detail2 = 0x4, // | Detail1,
		Detail3 = 0x8, // | Detail2,
		Verbose = 0x10, //| Detail3,
		Allways = 0xffffffff,
	};
	class DummyStream : public std::ostream, public std::streambuf
	{
	public:
		DummyStream() : std::ostream(this) {}
		template <class T>
		DummyStream &operator<<(const T &data)
		{
			return *this;
		}
	};

	Printer(std::ostream &output, int filter)
	{
		m_filter = filter;
		m_outStrm = &output;
		//setOutputFormat();
		m_wallTimer.start();
	}

	//void saveOutputFormat() {  }

//	void setOutputFormat()
//	{
//		//*m_outStrm<<std::fixed<<std::setw(8)<<std::setfill('0');
//		m_outStrm->flags(std::ios_base::fixed);
//		m_outStrm->fill('0');
//		m_outStrm->width(4);
//	}

	Printer &printTimeStamp(int filter)
	{
		//IOSStateSaver s(*m_outStrm);
		//setOutputFormat();
		//int prec = m_outStrm->precision();
		//<<std::setprecision(6)
    	if(m_filter & filter)
		  *m_outStrm<<"["<<std::fixed<<std::setprecision(3)<<std::setw(10)<<std::setfill('0')<<m_wallTimer.elapsedSec()<<"]";

		return *this;
	}

	std::ostream & print(int filter)
	{
    	if(m_filter & filter)
    	{
    		//if(timeStamp)
    		//  printTimeStamp();
    		//TODO: consider doing this only when flags changed? check if an if statement will hurt performance. This operation just sets flags.
    		//setOutputFormat();
    		return *m_outStrm;
    	}
    	else
    	{
    		return m_dummyStream;
    	}
	}

	static Printer &getSysPrinter()
	{
		static Printer theOne(std::cout,Allways);
		return theOne;
	}

	//std::ostream & print() { return print(ALL); }

	void setDest(std::ostream &dest)   {  m_outStrm = &dest;  }

	void setFilter(int filter) { m_filter = filter; }

	std::mutex &getLock() { return m_mutex; }

	virtual ~Printer() {}
protected:
	int m_filter;
	std::ostream *m_outStrm;
    DummyStream  m_dummyStream;
	oslc::lul::WallTimer m_wallTimer;
	std::mutex m_mutex;
};

// print timestamp + newline
#define PRINT_BASE(prnInst,filter,x) \
{ \
  std::lock_guard<std::mutex> autoGuard(prnInst.getLock()); \
  prnInst.printTimeStamp(filter).print(filter)<<x<<"\n"; \
}

// print raw -> no time stamp, no new line
#define PRINT_BASE_RAW(prnInst,filter,x) \
{ \
  std::lock_guard<std::mutex> autoGuard(prnInst.getLock()); \
  prnInst.print(filter)<<x; \
}

// print raw ->  no new line
#define PRINT_BASE_RAW_WTS(prnInst,filter,x) \
{ \
  std::lock_guard<std::mutex> autoGuard(prnInst.getLock()); \
  prnInst.printTimeStamp(filter).prnInst.print(filter)<<x; \
}

#define PRINT(filter,x)  PRINT_BASE(Printer::getSysPrinter(),filter,x)

//#define PRINT_ALLWAYS(x)  PRINT_BASE(Printer::getSysPrinter(),Printer::All,true,x)

#define PRINT_RAW(filter,x)  PRINT_BASE_RAW(Printer::getSysPrinter(),filter,x)

#define PRINT_RAW_WTS(filter,x)  PRINT_BASE_RAW_WTS(Printer::getSysPrinter(),filter,x)

#define PRINT_TS(filter) PRINT_RAW_WTS(filter,"")

#endif /* PRINTER_H */
