/*
 * QueueManager.h
 *
 *  Created on: Jun 28, 2018
 *      Author: admin
 */

#ifndef QUEUEMANAGER_H_
#define QUEUEMANAGER_H_

#include<queue>
#include "DataExchange.h"

// mpi
#include <oslc/mpi/MPI.h>
//using namespace oslc::mpi;

// serialization misc
#include <oslc/lul/MemBuffer.h>
//using namespace oslc::lul;

#include <oslc/mpi/MPIMsgQueue.h>

typedef oslc::mpi::MPIMsgQueue<DataExchange> DBasisMsgQueue;

//
//class QueueManager
//{
//public:
//  QueueManager() : m_memStrm(COMM_BUFFER_SIZE)
//  {
//	  // TODO Auto-generated constructor stub
//
//  }
//
//  void enqueueIn(const DataExchange &de)
//  {
//	  m_dataIn.push(de);
//  }
//
//  void enqueueOut(const DataExchange &de)
//  {
//	  m_dataOut.push(de);
//  }
//
//  const DataExchange &dequeueIn()
//  {
//	  DataExchange de = m_dataIn.front();
//	  m_dataIn.pop();
//	  return de;
//  }
//
//  const DataExchange &dequeueOut()
//  {
//	  DataExchange de = m_dataOut.front();
//	  m_dataOut.pop();
//	  return de;
//  }
//
//  // comm misc
//  bool tryReadData(DataExchange &de)
//  {
//	  // read data from mpi layer
//	  MPI_Status status;
//	  m_comm.receive(m_memStrm.getMemBuff().buff(),COMM_BUFFER_SIZE,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,&status);
//	  LOG_INFO(rank<<" Received data from: "<<status.MPI_SOURCE<<":"<<status.MPI_TAG);
//	  //if(buff==0)
//	  //  break;
//	  // serialize data
//	  IBinarySerializer<IOMemStream> serI(m_memStrm);
//	  serI>>de;
//	  return true;
//  }
//
//  bool tryWriteData(const DataExchange &de)
//  {
//	  // write data to mpi layer
//	  //MPI_Status status;
//	  int tag = m_comm.rank();
//	  // serialize data
//	  OBinarySerializer<IOMemStream> serI(m_memStrm);
//	  serI<<de;
//	  m_comm.send(m_memStrm.getMemBuff().buff(),COMM_BUFFER_SIZE,MPI_CHAR,0,tag);
//	  return true;
//  }
//
//  //
//  // manage queue in/out
//  //
//  static void run(QueueManager *queueMgr)
//  {
//	  // while we have messages to send out
//	  //while(m_dataIn.size())
//	  // send them out
//	  while(queueMgr->m_dataOut.size())
//
//	  using namespace oslc::lul;
//	  //MPI mpi(MPI::ThreadSupport::Single);
//	  MPI::Comm comm;
//	  int size = comm.size();
//	  int rank = comm.rank();
//	  //std::string hostName = mpi.getHostName();
//	  //LOG_INFO("host name="<<hostName<<" Size="<<size<<" Rank="<<rank);
//
//	  if(rank==0)
//	  {
//	  }
//	  else
//	  {
//		  DataExchange de;
//
//
//
//	  }
//
//
//  }
//
//  ~QueueManager()
//  {
//	  // TODO Auto-generated destructor stub
//  }
//
//private:
//	std::queue<DataExchange> m_dataOut;
//	std::queue<DataExchange> m_dataIn;
//    MPI::Comm m_comm;
//    const unsigned int COMM_BUFFER_SIZE = 100000;
//    IOMemStream m_memStrm;
//
//};


#endif /* QUEUEMANAGER_H_ */
