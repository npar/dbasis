/*
 * Configuration.cpp
 *
 *  Created on: Nov 19, 2017
 *      Author: admin
 */

#include "Configuration.h"

#include <oslc/lul/CmdLineOptions.h>

#include <oslc/lul/Version.h>

//
// Version information for application
//

// TODO: bump version numbers here when appropriate or automate (using cmake/make and git version numbers etc.)
DEFINE_VERSION_INFO(DBasisHDA,0,2,01,"b");


using namespace oslc::lul;

//
// configuration options
//
// TODO: setup external configuration file - values below are just place holders
REGISTER_CONFIG_OPT("testInt","This is an int test",int,0);
REGISTER_CONFIG_OPT("testFloat","This is a float test",float,7.7);
REGISTER_CONFIG_OPT("testString","This is a string test",std::string,"default value string");

//
// command line options
//

INIT_CMD_LINE("DBasisHDA", "Usage is DBasisHDA <-i inputFile> [<-o outputFile> [<-c column>]");
REGISTER_CMDLINE_OPT(INPUT_FILE_CMD_OPT_STR,  "Input file path\n\tthis is a required argument",  std::string, "", CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_OPT(OUTPUT_FILE_CMD_OPT_STR, "Output file path\n\toptional argument", std::string, "", CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_OPT(COLUMN_CMD_OPT_STR, "A column to work on\n\toptional argument",  int, 0, CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_OPT(ORD_CMD_OPT_STR,   "Run in one row deletion mode\n\toptional argument",    bool, false, CmdLineOption::OptionType::BinaryOpt);
REGISTER_CMDLINE_OPT(REDUCE_CMD_OPT_STR, "Run in reduce mode\n\treduce after call to shd", bool, false, CmdLineOption::OptionType::BinaryOpt);
REGISTER_CMDLINE_OPT(TEST_CMD_OPT_STR, "Run test harness\n\tmore info...", bool, false, CmdLineOption::OptionType::BinaryOpt);
//TODO: !!! use SUPPORT_LOWER_BOUND_DEFAULT from table.h?
REGISTER_CMDLINE_OPT(MIN_SUPPORT_CMD_OPT_STR, "Min support to work on\n\toptional argument", int, 1, CmdLineOption::OptionType::ValueOpt);
// combination group options
REGISTER_CMDLINE_OPT(COMB_GROUP_CMD_OPT_STR, "Combination group. Usage is: " COMB_GROUP_CMD_OPT_STR " [1,2,3]\n\toptional argument", std::vector<int>, std::vector<int>(), CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_OPT(COMB_GROUP_SIZE_CMD_OPT_STR, "Combination group size\n\toptional argument", int, 0, CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_OPT(COMB_GROUP_COUNT_CMD_OPT_STR, "Combination group count\n\toptional argument", int, 0, CmdLineOption::OptionType::ValueOpt);
// version information
REGISTER_CMDLINE_OPT(VERSION_CMD_OPT_STR, "Display version information", bool, false, CmdLineOption::OptionType::BinaryOpt);
REGISTER_CMDLINE_SYNONYM(VERSION_CMD_OPT_STR, "-v");
// zero based io
REGISTER_CMDLINE_OPT(ZERO_BASED_CMD_OPT_STR, "Run in zero base mode\n\tby default all column and row numbers are one based\n\tif this option is specified, all input/output column numbers will be zero based", bool, false, CmdLineOption::OptionType::BinaryOpt);
// aggregate flag - determine if MRD algorithms should aggregate
REGISTER_CMDLINE_OPT(AGGREGATE_CMD_OPT_STR, "Determine if MRD algorithm should aggregate. If not specified, by default the value is true.\n\tSetting aggregation to false disables aggregation. Usage is:\n\tDisable: -agg 0\n\tEnable: -agg 1", bool, true, CmdLineOption::OptionType::ValueOpt);
// parallel execution - multiple processes
REGISTER_CMDLINE_OPT(MULTIPLE_PROCESSES_CMD_OPT_STR, "parallel process execution mode (multiple processes potentially across multiple computers (uses MPI))", bool, false, CmdLineOption::OptionType::BinaryOpt);
// parallel execution - multiple threads
REGISTER_CMDLINE_OPT(MULTIPLE_THREADS_CMD_OPT_STR, "parallel thread execution mode (multiple threads on a single computer)", bool, false, CmdLineOption::OptionType::BinaryOpt);

//Minimal hitting set params
REGISTER_CMDLINE_OPT(MHS_ALGO_CMD_OPT_STR, "Minimal hitting set algorithm. Usage is: " MHS_ALGO_CMD_OPT_STR  " [rs,mmcs,bm] \n\toptional argument", std::string, "rs", CmdLineOption::OptionType::ValueOpt);
REGISTER_CMDLINE_OPT(MHS_ALGO_CUTOFF_SIZE_CMD_OPT_STR, "Minimal hitting set algorithm cut off size. Can be used with the rs and mmcs algorithms.\n\tUsage is: " MHS_ALGO_CMD_OPT_STR " [rs,mmcs] " MHS_ALGO_CUTOFF_SIZE_CMD_OPT_STR " [1..N]" " \n\toptional argument", unsigned, 0, CmdLineOption::OptionType::ValueOpt);

REGISTER_CMDLINE_OPT(VERBOSITY_LEVEL_CMD_OPT_STR, "Verbosity level of output displayed by program.\n\tUsage is: " VERBOSITY_LEVEL_CMD_OPT_STR  " [1..N]\n\tValid values are: 1 => Summary (default level), 2 => Verbose", int, 1, CmdLineOption::OptionType::ValueOpt);



