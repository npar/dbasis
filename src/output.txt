0.00 Reducing table.
0.00 Equivalent columns for reduced table in original table starting from 1
1 2 3 4 5 7 9 10 11 12 13 14 15 16 17 18 19 20 21 22 
 Equivalent columns in original table starting from 1, blank means null set 
6<=> 
8<=> 7 
0.00 Creating row comparison table.
0.00 Creating column comparison table.
0.00 Creating up and down arrows.
0.00 std::vector<Implication> FindDBasis 
0.00 Table::getFullBinaryBasis 
0.00 Table::getFullBinaryBasis done.
0.00 Table::getDFullNonBinaryBasis FindDBasis 
0.00 Calling shd.
Producing hypergraph with a parameter siz: 5.
0.00 Returned from shd.
0.00 readDualToImplication start.
0.00 Counting data points.
0.00 Count finished. Datapoints 38. Sets 19.
0.00 Read back 0 data points.
0.00 readDualToImplication finish.
0.00 Implication size: 19 Starting reduction.
0.00 Reduction finished.
0.00 diff s d0
0.00 Table::getDFullNonBinaryBasis FindDBasis done
0.00 std::vector<Implication> FindDBasis done. 
0.00 0;2 -> 1 ; support = 6; RealSupport = 6; rows = 2, 3, 4, 7, 8, 9, 
0.00 1;12 -> 4 ; support = 4; RealSupport = 4; rows = 3, 5, 6, 7, 
0.00 2;4 -> 7 ; support = 6; RealSupport = 6; rows = 3, 4, 5, 6, 7, 8, 
0.00 3;5 -> 7 ; support = 4; RealSupport = 4; rows = 5, 6, 7, 10, 
0.00 4;12 -> 7 ; support = 4; RealSupport = 4; rows = 3, 5, 6, 7, 
0.00 5;19 -> 7 ; support = 3; RealSupport = 3; rows = 4, 5, 9, 
0.00 6;3 -> 10 ; support = 4; RealSupport = 4; rows = 2, 3, 5, 7, 
0.00 7;9 -> 10 ; support = 4; RealSupport = 4; rows = 2, 3, 5, 8, 
0.00 8;9 -> 11 ; support = 4; RealSupport = 4; rows = 2, 3, 5, 8, 
0.00 9;5 -> 16 ; support = 4; RealSupport = 4; rows = 5, 6, 7, 10, 
0.00 10;17 -> 16 ; support = 4; RealSupport = 4; rows = 1, 5, 8, 9, 
0.00 11;18 -> 16 ; support = 6; RealSupport = 6; rows = 1, 2, 5, 7, 8, 9, 
0.00 12;17 -> 18 ; support = 4; RealSupport = 4; rows = 1, 5, 8, 9, 
0.00 13;1 4 -> 22 ; support = 4; RealSupport = 4; rows = 3, 4, 7, 8, 
0.00 15;1 10 -> 22 ; support = 5; RealSupport = 5; rows = 1, 2, 3, 7, 8, 
0.00 16;1 11 -> 22 ; support = 4; RealSupport = 4; rows = 1, 2, 3, 8, 
0.00 17;1 13 -> 22 ; support = 4; RealSupport = 4; rows = 1, 3, 4, 8, 
0.00 18;1 14 -> 22 ; support = 4; RealSupport = 4; rows = 1, 4, 7, 8, 
0.00 19;1 15 -> 22 ; support = 3; RealSupport = 3; rows = 2, 4, 8, 
0.00 20;1 20 -> 22 ; support = 5; RealSupport = 5; rows = 1, 2, 4, 7, 8, 
0.00 21;1 21 -> 22 ; support = 3; RealSupport = 3; rows = 1, 4, 8, 
0.00 22;20 10 -> 22 ; support = 4; RealSupport = 4; rows = 1, 2, 7, 8, 
0.00 23;20 13 -> 22 ; support = 3; RealSupport = 3; rows = 1, 4, 8, 
0.00 24;20 14 -> 22 ; support = 4; RealSupport = 4; rows = 1, 4, 7, 8, 
0.00 25;20 18 -> 22 ; support = 4; RealSupport = 4; rows = 1, 2, 7, 8, 
0.00 28;21 13 -> 22 ; support = 4; RealSupport = 4; rows = 1, 4, 8, 10, 
0.00 29;21 14 -> 22 ; support = 4; RealSupport = 4; rows = 1, 4, 8, 10, 
Total Support(column->it's total support)
1 -> 16
2 -> 0
3 -> 0
4 -> 2
5 -> 0
6 -> 0
7 -> 0
8 -> 0
9 -> 0
10 -> 4.5
11 -> 2
12 -> 0
13 -> 5.5
14 -> 6
15 -> 1.5
16 -> 0
17 -> 0
18 -> 2
19 -> 0
20 -> 10
21 -> 5.5
22 -> 27.5
