/*
 * DataExchange.h
 *
 *  Created on: May 13, 2018
 *      Author: admin
 */

#ifndef DATAEXCHANGE_H_
#define DATAEXCHANGE_H_

#include "Implication.h"
#include <oslc/lul/Serializer.h>

//
// DataExchange - will be used for data exchange between parallel modules
//
struct DataExchange
{
	std::vector<int> removeRowList;
	EquivalenceMap equivMap;
	Implications imps;

	// serialization
	template<class Serializer>
	void serialize(Serializer &ser, const unsigned int version)
	{
		ser & removeRowList;
		ser & equivMap;
		ser & imps;
//		for(auto val: equivMap)
//		{
//			ser & val.first;
//			ser & val.second;
//		}

	}
};



#endif /* DATAEXCHANGE_H_ */
