#all: Implication.cpp Main.cpp SetOperations.cpp shd.h Table.h Implication.h Makefile SetOperations.h Table.cpp timestamp.cpp !!! oren ???
#	g++ -O3 -I../shd31 *.cpp ../shd31/shd.o -o DBasisHDA

#	g++ -Og -I../shd31 ^$ ../shd31/shd.o -o DBasisHDA # !!! !!! 10.28.17 oren change: compile for best debugging experience

#########################################
# !!! 11.10.17 oren -> improve make file
#########################################
# Notes: 
#   Use make DEBUG=0 for release version
#   Use make DEBUG=1 for debug version


DEL_CMD  := rm -r #f
COPY_CMD   := cp #f
COMPRESS_CMD := tar -hczvf 
MAKE_DIR := mkdir -p

BUILD_DIR   := .
DISTRIB_DIR := $(BUILD_DIR)/distrib

# !!! oren changes
#BOOST_ROOT = ../boost
# boost install dir can now be supplied in the environment
ifeq ($(BOOST_ROOT),)
  #BOOST_ROOT = ../boost
  $(info BOOST_ROOT was not specified, using default boost install) 
else
  $(info BOOST_ROOT was specified, using custom BOOST_ROOT => $(BOOST_ROOT)) 
  INCLUDES += -I$(BOOST_ROOT)/include 
  LIBS += -L$(BOOST_ROOT)/lib
  LIBS += -Wl,-rpath-link=$(BOOST_ROOT)/lib
endif

# Boost libraries
LIBS += -lboost_program_options
LIBS += -lboost_system
LIBS += -lboost_log

# note $ORIGIN will allow dyn lib search in executable dir
LIBS += -loslc -lmhsa -Wl,-rpath='$$ORIGIN' -lgomp

ifeq ($(DEBUG), 1)
  $(info Make running in debug mode)
  OBJ_TYPE := Debug
  OBJ_DIR  := $(BUILD_DIR)/$(OBJ_TYPE)
#  LDFLAGS  := -Loslc/Debug -pthread -L/home/admin/projects/npar/Minimal-Hitting-Set-Algorithms 
  LDFLAGS  += -Llib/oslc/Debug -pthread -Llib/mhsa 
#  LIBS     := -loslc -lmhsa
  CXXFLAGS := -std=c++11 -O0 -g3 -Wall -fmessage-length=0 -Wno-sign-compare
else
  $(info Make running in release mode)
  OBJ_TYPE := Release
  OBJ_DIR  := $(BUILD_DIR)/$(OBJ_TYPE)
#  LDFLAGS  := -Loslc/Release -pthread -L/home/admin/projects/npar/Minimal-Hitting-Set-Algorithms 
  LDFLAGS  += -Llib/oslc/Release -pthread -Llib/mhsa 
#  LIBS     := -loslc -lmhsa
  CXXFLAGS := -std=c++11 -O3 -g3 -Wall -fmessage-length=0 -Wno-sign-compare
endif

# dependency libs
MHSA_DYN_LIB:= lib/mhsa/libmhsa.so

# needed for openmp support!
# CXXFLAGS += -fopenmp

#
# Parallel implementation build
#
ifeq ($(PARALLEL_IMP),0)
  $(warning *** Making a non-parallel build ***)
  $(warning *** !!! Not recommended/useful unless testing the build system !!!  ***)
  $(warning *** ****  ***)
else
  CXXFLAGS += $(shell mpicc -showme:compile)
  LDFLAGS  += $(shell mpicc -showme:link)
  $(info *** Making a parallel build ***)
endif

CXX      := g++
#CXXFLAGS := -std=c++11 -O0 -g3 -Wall -fmessage-length=0 -Wno-sign-compare
#LDFLAGS  := #-L/usr/lib 
#OBJ_DIR  := $(BUILD_DIR)/Debug
APP_DIR  := $(OBJ_DIR)
TARGET   := DBasisHDA
INCLUDE  := -I"lib/oslc/src" -I"lib" -I"lib/shd31"
SRCS     :=                      \
   $(wildcard src/*.cpp) #\
#   $(wildcard ...) \

OBJS := $(SRCS:%.cpp=$(OBJ_DIR)/%.o) lib/shd31/shd.o 	

#handle dependencies
DEPS := $(OBJS:%.o=%.d)
DEP_FLAGS:= -MMD -MP

# make default target
#.DEFAULT_GOAL := init

init: 
	$(MAKE) oslcLib
	$(MAKE) $(APP_DIR)/$(TARGET)

# note -> $(USER_DEFINES) can be used to pass defines to make -> g++. example: make ... USER_DEFINES="-DTEST_FLAG=xyz"

$(OBJ_DIR)/%.o: %.cpp
	$(MAKE_DIR) $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(DEP_FLAGS) $(USER_DEFINES) -o $@ -c $<

$(APP_DIR)/$(TARGET): $(OBJS)
	$(MAKE_DIR) $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $(APP_DIR)/$(TARGET) $^ $(LIBS) $(LDFLAGS)
	$(COPY_CMD) $(MHSA_DYN_LIB) $(OBJ_DIR)

# !!! Note - shd uses non standard conversions that are frawned upon in modern compilers.
#            It produces excessive amounts of warnings that should someday be addressed.  
#            For now, we need to add -Wnarrowing to compile successfully 
lib/shd31/shd.o: lib/shd31/shd.c
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $< -Wno-narrowing -D_FILE2_LOAD_FROM_MEMORY_

#################################
# build library dependencies
#oslc/Debug/oslc.a: 
#	cd oslc && $(MAKE) lib
oslcLib: lib/oslc
	$(MAKE) -C $< lib mpi

mhsaLib: lib/mhsa
	$(MAKE) -C $< all

#################################

#all: oslcLib $(APP_DIR)/$(TARGET)

# solve parallel dependencies - make targets by order (internaly they can be parallel)
all:
	$(MAKE) oslcLib
	$(MAKE) mhsaLib
	$(MAKE) $(APP_DIR)/$(TARGET)

#ifneq("$(wildcard $(APP_DIR))","")
#ifneq ("$(OBJ_DIR)","$(APP_DIR)")
clean:
ifneq ("$(OBJ_DIR)","$(APP_DIR)")
	$(DEL_CMD) $(OBJ_DIR)
	$(DEL_CMD) $(APP_DIR)
else
#clean:
	$(DEL_CMD) $(OBJ_DIR)
endif

cleanAll: clean
	$(MAKE) -C lib/oslc clean
	$(MAKE) -C lib/mhsa clean

# list of files needed for distribution
DISTRIB_FILES += Debug/$(TARGET) Release/$(TARGET) $(BUILD_DIR)/README.md scripts/* data/*
DISTRIB_FILES += $(MHSA_DYN_LIB)
# boost dependencies
BOOST_DEP_FILES  = libboost_program_options.so libboost_system.so libboost_log.so 
# yet more dependencies...
BOOST_DEP_FILES += libboost_atomic.so libboost_chrono.so libboost_thread.so libboost_date_time.so libboost_filesystem.so libboost_regex.so
BOOST_VERSION_SUFFIX = 1.61.0
DISTRIB_FILES += $(BOOST_DEP_FILES:%=$(BOOST_LIB_PATH)/%.$(BOOST_VERSION_SUFFIX))
# copy boost unicode dependencies
DISTRIB_FILES += $(BOOST_LIB_PATH)/libicudata.so.50 $(BOOST_LIB_PATH)/libicui18n.so.50 $(BOOST_LIB_PATH)/libicuuc.so.50


distrib: $(DISTRIB_FILES) 
	#$(COPY_CMD) $(APP_DIR)/$(TARGET) $(DISTRIB_DIR)
	## list of files to compress and distribute
	#$(COMPRESS_CMD) $(DISTRIB_DIR)/DBasisDist.tar.gz Debug/$(TARGET) Release/$(TARGET) $(BUILD_DIR)/README.md
	$(COMPRESS_CMD) $(DISTRIB_DIR)/DBasisDist.tar.gz $^

docker: 
	git archive --prefix=dbasis/ -o containers/dbasis.tar.gz HEAD
	docker build -t npar/dbasis -f containers/Dockerfile.ubuntu containers
        #$(DEL_CMD) dbasis.tar.gz
        

.PHONY: all clean cleanAll oslcLib mhsaLib distrib docker

-include $(DEPS)


