While these aren't necessarily scripts in the traditional sense, they're a required part of the program for now.
You have to run the following commands from the command line that you use to run the Dbasis program:
source scripts/openMpiEnv
source scripts/DBasisDepEnv

these commands will set up the dependency environments to allow the program to run.
