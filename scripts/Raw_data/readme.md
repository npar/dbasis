data_setup - Converts raw data into something more usable for our program
take, for instance, a matrix, M:
0 1 -1
1 0 1
0 0 1

the first step divides this matrix into two seperate matrices:
M_over:         M_under:
a b c           a b c
0 1 0           0 0 1
1 0 1           0 0 0
0 0 1           0 0 0

add in other data -- since we had multiple files, each of which help different information, this was used to capture that information and append it onto the binary matrices produced from data_setup. Also worth noting, even though there was no script provided the over and underexpressed genes were combined by simply concatenating the M_over and M_under matrices together and keeping track of the index where they were joined:
M_tot:
a  b  c  ~a ~b ~c
0  1  0  0  0  1
1  0  1  0  0  0
0  0  1  0  0  0
