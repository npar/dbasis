
def put_survival_groups():
    f = open("patients.csv", "r")
    holder = []
    survival = f.readline() #read in the survival and censoring data
    censoring = f.readline() #same here
    survival = str(survival)[:len(survival)-1].split(",")
    censoring = str(censoring)[:len(censoring)-1].split(",")
    for days,censoring in zip(survival,censoring): #This implements the logic in the July 16, 2018 email from Dr. Adaricheva
        days = int(days)
        censoring = int(censoring)
        x = len(holder)
        if (censoring == 0):
            if (days > 2000):
                holder.append("1,1,0,0")
            elif (days <= 2000 and days > 1300):
                holder.append("0,1,0,0")
            elif (days > 850 and days <= 1300):
                holder.append("0,0,1,0")
            elif (days <= 850):
                holder.append("0,0,1,1")
        else: #if there's a one in the censoring column
            if (days > 2000):
                holder.append("1,1,0,0")
            elif (days > 1300 and days <= 2000):
                holder.append("0,1,0,0")
            elif (days > 850 and days <= 1300):
                holder.append("0,0,0,0")
            elif (days <= 850):
                holder.append("0,0,0,0")
    line1 = [] #eventually to be output: 4 lines of survival coding
    line2 = []
    line3 = []
    line4 = []

    i = 0
    j = 0
    while j < len(holder):
        line1.append(holder[j][i])
        i += 2
        line2.append(holder[j][i])
        i += 2
        line3.append(holder[j][i])
        i += 2
        line4.append(holder[j][i])
        j += 1
        i = 0

    f = open("categories.csv","a+")
    f.write(str(line1))
    f.write('\n')
    f.write(str(line2))
    f.write('\n')
    f.write(str(line3))
    f.write('\n')
    f.write(str(line4))
    f.close()
i = input("Want to run survival groups? 1 if yes: ")
if i:
    put_survival_groups()

def read_recurrence():
    f = open("recurrence.csv", "r")
    line = f.readline()
    line = line[:len(line)-1]
    line = line.split(',')
    line2 = []
    for i in line:
        if i == 'NA':
            line2.append(0)
        else:
            line2.append(1)
    f = open("express2.csv","a+")
    f.write(str(line2))
    f.close()
i = input("Want to read and write the recurrence values? 1 if yes: ")
if i:
    read_recurrence()
