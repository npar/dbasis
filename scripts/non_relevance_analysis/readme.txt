These scripts are for looking at properties of the matrix output/input that might be important but that aren't necesarilly related to relevance.


antecedents -- prints out the distribution of rule sizes. For instance, [10,15,8,2,0,0,0,1] -> 10 rules of size 1, 2 of size 15....
Rule_frequencies.py -- prints number of rules that each row supports
Ranker.py -- ranks rows for likelihood of being a blocker -- each ranker has a different calculation/way of weighting the potential blocking rows.
density.py -- prints density of matrices
column averages.py -- returns averages (Read, density) of each column of a csv
cluster.py -- given an output of the program run on the medical data, prints out a line iff only under OR overexpressed genes show up. Under/overexpressed genes? since genes were traditionally split so the first N cols would be overexpressed and the next M to a certain number would be underexpressed, this code offers to check two cases. First, if you're checking to see if all are less than a value, you look between M and then N and see if any rules include those columns, if they include those columns then it's not only rows before a certain column. The same is similarly done for the other case.
Sup.py -- finds number of rules whose support is above some threshold
