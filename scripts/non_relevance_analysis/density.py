import os

def density(name):
    f = open(str(name)+".txt","r")
    one = int(f.readline())
    two = int(f.readline())
    one_count = 0
    zero_count = 0
    for line in f:
        for char in line:
            if (char == "0" or char == 0):
                zero_count = zero_count + 1
            if(char == "1" or char == 1):
                one_count = one_count + 1
    print(100*float(one_count)/(one*two))
    return(100*float(one_count)/(one*two))
