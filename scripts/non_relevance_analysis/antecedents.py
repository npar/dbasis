fname = "<EXAMPLE STRING>"
f = open(fname,"r")


rule_nums = [0 for i in range(0, 30)]
for line in f:
    if "support =" in line:
        tline = line.find(";")
        tline = line[tline+1:line.find("->")-1]
        tline = tline.split()
        tline = map(int,tline)

        rule_nums[len(tline)] += 1

print(rule_nums)
