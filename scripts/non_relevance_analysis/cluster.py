def all_less_than(a, sep, end):
    for i in a:
        if i > sep and i < end:
            return False
    return True

def all_greater_than(a, sep):
    for i in a:
        if i < sep:
            return False
    return True

over = True
over = False

f = open("t0_out.txt","r")
for line in f:
    if "support =" in line:
        tline = line.find(";")
        tline = line[tline+1:line.find("->")-1]
        tline = tline.split()
        tline = map(int,tline)
        
        if(all_less_than(tline, 444, 888)):
            print("all less than some range")
        if(all_greater_than(tline, 444)):
            print("all greater than some range")
