def ranker(filename): #Rank(blocker) = sum ( confidence * (1/N)  * (Avg.tsup(new rules)/||new rules||)**2)/|new rules|))
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += confidence * (float(1)/float(N[q-1])) * ((tsup_to_be_averaged/(float(i)+1*(i==0)))**2)/i
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks


def ranker2(filename): #Rank(blocker) = sum ( confidence* (Avg.tsup(new rules)/||new rules||)**2)
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += confidence * ((tsup_to_be_averaged/(float(i)+1*(i==0)))**2)
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks

def ranker3(filename): #Rank(blocker) = sum ( confidence * Avg.tsup(new rules) ^ 2/||new rules||)
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += confidence * ((tsup_to_be_averaged**2/(float(i)+1*(i==0))))
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks


def ranker4(filename): #Rank(blocker) = sum ( confidence * 1/N * Avg.tsup(new rules) ^ 2/||new rules||)
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += confidence * (float(1)/float(N[q-1])) * tsup_to_be_averaged**2/(float(i)+1*(i==0))
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks

def ranker5(filename): #Rank(blocker) = sum ( confidence * Avg.tsup(new rules) ^ 2)
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += tsup_to_be_averaged
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks

def ranker6(filename): #Rank(blocker) = sum ( confidence * Avg.tsup(new rules) ^ 2)
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += float(tsup_to_be_averaged)*i
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks

def ranker7(filename): #Rank(blocker) = sum ( confidence * (Avg.tsup(new rules) ^ 2)/N)
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += float(tsup_to_be_averaged)**2*1.0/N[q-1]
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks

def ranker8(filename): #Rank(blocker) = sum ( avg_tsup(new rules))
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0
                tsup_to_be_averaged = 0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)                           #rows
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= len(y)
                for q in x:
                    ranks[q-1] += float(1.0/float(i)) * tsup_to_be_averaged
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks

def ranker9(filename): #Rank(blocker) = sum ( t_sup * number of new rules)
    ranks = [0 for i in range(0,22)]
    N = [0 for i in range(0,22)]
    confidence = float(19)/float(22)
    inp = open(filename + ".txt","r")
    inp2 = open(filename+".txt","r")
    while True: #workaround
        line = inp.readline()
        if not line:
            return ranks
        else:
            z = inp2.readline() #keeps second copy of input at same line as original
            if not z:
                return ranks
            if("removed row list:" in line):
                num_of_new_rules = 0.0
                tsup_to_be_averaged = 0.0
                x = line                                    #finds
                x = (x[x.find(": ")+2:line.find(" ***")])   #the
                x = x.split(", ")                           #removed
                x = map(int, x)
                for i in x:
                    N[i-1] += 1                             #adjusts N parameters
                i = 0.0
                while("Total Support(column->it's total support)" not in z): #gets avg tsup + number of new rules
                    y = inp2.readline()
                    inp.readline()
                    z = y
                    i = i + 1.0
                    y = y[y.find("rows = ")+7:len(y)-3]
                    y = y.split(", ")
                    y = map(int, x)
                    tsup_to_be_averaged+= float(len(y))
                for q in x:
                    ranks[q-1] += float(i)*tsup_to_be_averaged
    print("ranks are shifted. Return value[0] corresponds to row 1, value[1] -> row 2, etc...")
    return ranks


from operator import itemgetter
x,y,z,a,b,c,d,e,f = ranker("out_extracted_rules"),ranker2("out_extracted_rules"),ranker3("out_extracted_rules"),ranker4("out_extracted_rules"),ranker5("out_extracted_rules"),ranker6("out_extracted_rules"),ranker7("out_extracted_rules"),ranker8("out_extracted_rules"),ranker9("out_extracted_rules")


holder,q = [], 1
print("last four columns are the arithmetic avg. and the geometric mean, the arithmetic avg without ranker1, geometric mean without ranker1")
for i,j,k,l,m,n,o,p,r in zip(x,y,z,a,b,c,d,e,f):
    holder.append([q,i,j,k,l,m,n,o,p,r,(i*j*k*l*m*n*o*p*r)**(1.0/9.0),(j*k*l*m*n*o*p*r)**(1.0/8.0)])
    q+=1

print('')

print("ordered by geomtric mean with the ordered ranking")
holder.sort(key = itemgetter(11),reverse=True)
for i in holder:
	print i

print('')
print("ordered by geomtric mean without the ordered ranking")
holder.sort(key = itemgetter(10),reverse=True)
for i in holder:
	print i
#check confidence
	#support is computed, say support = 8, and the rule
#
#1-> 2, support = 8
#8/#times 1 occurs

print('')
print()
holder.sort(key = itemgetter(9),reverse=True)
for i in holder:
	print i
