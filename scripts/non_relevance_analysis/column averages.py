import csv
from collections import Counter
import ast
import operator as op

operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul, ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor, ast.USub: op.neg}
def eval_expr(expr):
    return eval_(ast.parse(expr, mode = 'eval').body)

def eval_(node):
    if isinstance(node, ast.Num):
        return node.n
    elif isinstance(node, ast.BinOp):
        return operators[type(node.op)](eval_(node.left),eval_(node.right))
    elif isinstance(node, ast.UnaryOp):
        return operators[type(node.op)](eval_(node.operand))

    
def average_column (csv_filepath):
    column_totals = Counter()
    with open(csv_filepath,"rb") as f:
        reader = csv.reader(f)
        row_count = 0.0
        for row in reader:
            for column_idx, column_value in enumerate(row):
                try:
                    n = float(eval_expr(column_value[1:]))
                    column_totals[column_idx] += n
                except ValueError:
                    print ("error, column val for error:" + column_value)
                    break
            row_count+=1

    column_indexes = column_totals.keys()
    averages = [column_totals[idx]/row_count for idx in column_indexes]
    return averages
    

    """
densities = ["14","21","27","33","39","46","52","58","61","68","71","77","83","89","96"]

for density in densities:
    with open("aggregated csvs/"+str(density)+".csv", "wb") as csvfile:
        writer = csv.writer(csvfile)
        for i in range(1,101):
            writer.writerow([i]+average_column(str(density)+"/csv/"+str(i)+".csv") + [sum(average_column(str(density)+"/csv/"+str(i)+".csv")[0:4])/float(4)] + [sum(average_column(str(density)+"/csv/"+str(i)+".csv")[4:])/float(27)])
            """
