#give a file name (output of program or matrix)
from Call_and_run import run
from switch_last import switch_last
from test_relevance import *
from extract_MRD import *
import itertools
x = [i for i in range(1,51)]
y = []
for i in range(1,51):
    for j in range(1+i,51):
        y.append([i,j])
info_holder = []
for j in range(0,1224):
    name_of_inp = "16/"+str(j)+'.txt'
    one_if_matrix = 1
    MRD_tru = 0

    if MRD_tru:
        MRD = y[j]
        print(MRD)

    if(one_if_matrix):
        run(name_of_inp, 50, MRD)
        column = 50
        switch = 0
        if (switch == 0):
            switch_last(name_of_inp)
            name_of_out = name_of_inp[:name_of_inp.find(".")] + "_switched.txt"
            run(name_of_out, str(int(column)+1), MRD, 1)
        name_of_inp_for_rel = name_of_inp[:name_of_inp.find(".")] + "_out.txt"
        name_of_out_for_rel = name_of_out[:name_of_out.find(".")] + "_out_switched.txt"
        info_holder.append(extract_MRD(name_of_inp_for_rel, name_of_out_for_rel, test = 1))

with open("121.csv", "wb") as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(info_holder)
"""
At this pont, should have:
    name_of_inp
    name_of_out
    MRD rows
"""
