import subprocess
import os
import shlex
from subprocess import Popen, PIPE
import csv

def extract(name_of_file, name_of_switched_file, size = 908):
    f = open(name_of_file, "r") #load in the files that you're going to observe
    f2 = open(name_of_switched_file,"r")
    overarching_info_holder1 = []
    overarching_info_holder = []
    info_holder = [] #info holder for this set of files

    reached_end = False  #no well-defined stopping points, so this hacks it together
    reduced = True
    for line in f: #at this point we know that there's at least one rule
        if("support = " in line):
            reduced = False
        if("column->it's total support" in line): #it's reached the "end" of the rules
            reached_end = True
        if(reached_end and not("column" in line) and not("support" in line) ): #iff we've reached the end of the rules it's time to look for total support
            info_holder.append(line[line.find("->")+3:len(line)-1])

    print(reduced)
    if(len(info_holder)==0): ##if we miss that a column was reduced, this catches
        info_holder = [0 for q in range(0,size)]
    
    if(reduced == True): #if we've found that the column IS reduced
        x = 0
        f3 = open(name_of_file,"r")
        w = 0
        print(f3)
        for line in f3:
            if(w>=2):
                if(line[len(line)-2] == 1 or line[len(line)-2] == "1" ):
                    x = x + 1
            w = w+1
        f3.close()
        f4 = open(name_of_switched_file,"r")
        for line in f4:
            if(str(size)+"<=>" in line):
                if(line[6:len(line)-2]==''):
                    y = str(x)+"/" + "1" + "\n the denominator WAS 0, however a 1 was added"
                else:
                    X = map(int, line[6:len(line)-2].split(" "))
                    print(X)
        if(line[6:len(line)-2] != ''):
            X = map(int, line[6:len(line)-2].split(" "))
            y = str(x)+"/" + str((len(X))) + "1"*(len(X)==0) + (len(X)==0)*"\n the denominator WAS 0, however a 1 was added"
        f5 = open(str(size)+"_was_reduced_.txt","w")
        f5.write(str(y))
        f5.close()
        f4.close()
        f3.close()
        return
                      
    second_info_holder = []
    reached_end = False 
    for line in f2:
        if("Begin" in line):
            break
        if("column->it's total support" in line):
            reached_end = True
        if(reached_end and not("support" in line)):
            second_info_holder.append(line[line.find("->")+3:len(line)-1])           
    if (len(second_info_holder)==0):
        second_info_holder =[0 for i in range(0,size)]
    f.close()
    f2.close()
    if(len(second_info_holder) > 1 and len(info_holder) > 1):
       for i,x in zip(info_holder, second_info_holder):
           overarching_info_holder1.append("="+str(i) + "/(" + str(x) + "+" + str(1)+")")
    overarching_info_holder.append(overarching_info_holder1)

    print(overarching_info_holder)
    
    with open(name_of_file[:len(name_of_file)-4]+".csv", "wb") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerows(overarching_info_holder)

extract('recur.txt','not_recur.txt')
