import os
import numpy as np
import random
def randbin(M,N,P):
    return np.random.choice([0, 1], size = (M, N), p = [P, 1-P])
    
outdir = "10x15matrices/" Self explanatory
num_files = 20

if not os.path.exists(outdir):
    os.makedirs(outdir)
i = 0
prob = .754
rows, cols = 76, 456


while (i < num_files):
    if(i % 10 != 0): #skipped only because sometimes a filename with 10 gave errors
        M = rows #cols
        N = cols #rows
        f = open(outdir+str(i)+"n.txt","w")
        f.write(str(M))
        f.write("\n")
        f.write(str(N))
        f.write("\n")
        x = randbin(M,N,prob)
        j = 0
        for line in x:
                y = list(line)
                f.write(str(y))
                f.write("\n")
        f.close()
    i = i + 1

i = 0
while (i < num_files):
    if(i % 10 != 0):
        f = open(outdir+str(i)+"n.txt","r")
        f2 = open(outdir+str(i)+".txt","w")
        for line in f:
            f2.write(line.replace("[","").replace("]","").replace(",",""))
        f.close()
        os.remove(outdir+str(i)+"n.txt")
        f2.close()
    i = i + 1
