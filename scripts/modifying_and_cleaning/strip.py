fname = "reduced_params_set_2.txt"
f = open(fname,"r")
import string
printable = set(string.printable)
f2 = open(fname += "_done","w")


for line in f:
    hold = ""
    for char in line:
        if char not in ["'",'"',"[","]",",","  "]:
            hold = hold + char
        else:
            hold = hold + " "
    x = (" ".join(hold.split()))
    x = filter(lambda X: X in printable, x)
    f2.write(x.encode('ascii'))
    f2.write("\n")
f2.close()
f.close()

