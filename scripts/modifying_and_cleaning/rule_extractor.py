import random
def is_number(s): #Needed because the info in matrices is stored as text, not float/int
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except(TypeError, ValueError):
        pass

def rule_extraction(filename):
    out_array = []
    with open(filename+".txt","r") as inp:
        for line in inp:
            if("removed row list:" in line):
                out_array.append("\n")
                out_array.append("done section")
                out_array.append("\n")
                out_array.append(line)
            if("->" in line and "support" in line):
                out_array.append(line[line.find(";")+1:])
            if("*** Printing all implications reduced" in line):
                break
    f2 = open(filename+"_extracted_rules.txt","w")
    for i in out_array:
        f2.write(i)
