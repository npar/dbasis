This folder contains programs for modifying and cleaning the datasets before they are run with the DBasis program.

custom_rule_insertion - replaces the first N characters of the first M lines with a custom set of values for all files in a specified folder, useful for testing how certain columns interact with rule outputs.
invert everything - takes everything in a given file and inverts it while maintaining the number of rows and columns. Invert, in this sense, means all 1's -> 0's and vice versa.
invert everything but last N - given some string index at which to stop, it inverts everything before that point in every row in a matrix.
noise_adder - given some file name and some probability, it adds gaussian, non-cryptographically secure, noise to the matrix by converting every value to the opposite based on the probability given.
rand - creates a given number of matrices whose elements are either 1 or 0 with a given probability. Useful for testing the way the program behaves on random data and contrasting that with non-rand data.
rule extractor - given a certain file output extracts all rules from the file
switch all last in folder - exactly what it sounds like, switches the last character in each row for each matrix in a folder.
strip - useful when converting .csv to .txt or stripping out exrtraneous characters in data
switch_last - appends onto the end of each column the inverted value, done because, traditionally, the last column was the target column.
