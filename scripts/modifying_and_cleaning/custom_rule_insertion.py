
import os
f_name = "<EXAMPLE STRING>" #folder__name
row_size = 16 #num of rows 
num_inserted_cols = 4
for subdir,dirs,files in os.walk(f_name):
    for file in files:
        f = open(f_name+"/" + str(file), "r")
        f2 = open(f_name+"/inserted_" + str(file),"w")
        f2.write(f.readline())
        f2.write(f.readline().strip()+num_inserted_cols)
        
        ##begin custom rules
        line = f.readline()
        f2.write("0 1 1 1 " + line[2*num_inserted_rules:])
        line = f.readline()
        f2.write("1 0 1 1 " + line[2*num_inserted_rules:])
        line = f.readline()
        f2.write("1 1 0 1 " + line[2*num_inserted_rules:])
        line = f.readline()
        f2.write("1 1 1 0 " + line[2*num_inserted_rules:])
        #end custom rules
        
        f.close()
        f2.close()
