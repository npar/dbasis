import os
import fileinput
def switch_last(filename):
    f = open(filename,"rU")
    i = 0
    f2 = open(filename[:filename.find(".")]+"_switched"+filename[filename.find("."):],"w")
    f2.write(f.readline())
    line = f.readline()
    f2.write(str(int(int(line[:4]) + 1)))
    for line in f:
        if(len(line) < 5):
            return
        line = line.split()
        line = (line + [1 if line[len(line)-1] == '0' else 0])
        write = ""
        for i in line:
            write = write + str(i) + " "
        write = write[:len(write)-1]
        f2.write('\n')
        f2.write(write)
    f.close()
    f2.close()

