import os
import fileinput

basedir = "16/"

for subdir,dirs,files in os.walk(basedir):
    for file in files:
        f = open(basedir+str(file),"r")
        f2 = open(basedir+"switched_"+ str(file),"w")
        f2.write(f.readline())
        f2.write(f.readline())
        for line in f:
            if (True):
                for character in range(0, len(line)):
                    if (character != len(line)-2):
                        f2.write(line[character])
                    elif(line[character] == "0"):
                        f2.write("1")
                    elif(line[character] == "1"):
                        f2.write("0")
                    else:
                        f2.write(line[character])
            else:
                f2.write(line)
        f.close()
        f2.close()
