import random
def is_number(s): #Needed because the info in matrices is stored as text, not float/int
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except(TypeError, ValueError):
        pass

def noise(file_name, probability_of_switching, output = ''):
    f = open(file_name, "r")
    if(output==''):
        f2 = open(file_name[:len(file_name)-4] + "_prob_switch~"+str(probability_of_switching)+"%.txt", "w")
    else:
        f2 = open(output+".txt","w")
    i = 0
    for line in f: #every line in f
        if(i >= 2): #he first 2 lines aren't changed
            x = ''
            for j in range(0,len(line)):
                if(is_number(line[j])): #only checks/generates iff line[k] == num
                    new_rand = random.randint(1, 100)
                    if(new_rand <= probability_of_switching):
                        x = x+"1"*(line[j]!="1")+"0"*(line[j]!="0")
                    else:
                        x = x+str(line[j])
                else:
                   x = x + str(line[j])
            f2.write(str(x))
        else:
            f2.write(line)
        i = i + 1
