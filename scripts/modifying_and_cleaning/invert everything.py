def is_number(s): #Needed because the info in matrices is stored as text, not float/int
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except(TypeError, ValueError):
        pass
    
def invert(filename):
    if (".txt" not in filename):
        filename = filename + ".txt"
    f = open(filename,"r")
    f_new = open("inverted_"+filename,"w")

    f_new.write(f.readline())
    f_new.write(f.readline())
    for line in f:
        i = 0
        for character in line:
            if(is_number(character)):
                if (character == "0" or character == 0):
                    f_new.write("1")
                else:
                    f_new.write("0")
            else:
                f_new.write(character)
    f.close()
    f_new.close()
