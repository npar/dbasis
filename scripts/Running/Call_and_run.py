import subprocess
import os
import shlex
from subprocess import Popen, PIPE

def run(name, last_column = 32, MRD = "", finalout = 0, mins = 1):
    if (finalout == 0):
        if(MRD == ""):
                arg = "./DBasisHDA" + "-mins " + str(mins) + " -i " + name + " -o " + name[:name.find(".")] + "_out.txt" + " -c " + str(last_column) + " -mins 1"
        else:
                arg = "./DBasisHDA" + "-mins " + str(mins) + " -i " + name + " -o " + name[:name.find(".")] + "_out.txt" + " -c " + str(last_column) + " -cg " + str(MRD) + " -cgc 1 " + "-cgs " + str(len(MRD)) + " -mins 1"
        print(arg)

    else:
        if(MRD == ""):
            arg = "./DBasisHDA" + "-mins " + str(mins) + " -i " + name + " -o " + name[:name.find(".")] + "_out_switched.txt"  + " -c " + str(last_column) + " -mins 1"
        else:
            arg = "./DBasisHDA" + "-mins " + str(mins) + " -i " + name + " -o " + name[:name.find(".")] + "_out_switched.txt " + " -c " + str(last_column) + " -cg " + str(MRD) + " -cgc 1 " + "-cgs " + str(len(MRD)) + " -mins 1"
        print(arg)
    args = shlex.split(arg)
    subprocess.call(args)
