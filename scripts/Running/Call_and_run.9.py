import subprocess
import os
import shlex
from subprocess import Popen, PIPE

rootdir = "76x462matrices/"
out = "76x462matrices/finished_matrices/"
if(not os.path.isdir(out)):
    os.makedirs(out)
def run(f_name, minsup, mhss):
    i = rootdir + f_name
    o = out+f_name+"_mins"+ str(minsup)+ "_mhss"+ str(mhss)
    mins = str(minsup)
    arg = "Release/DBasisHDA" + " -i " + i + " -o " + o + " -mins " + mins +" " + "-c 461" + " -mhss " + str(mhss)
    print(arg)
    args = shlex.split(arg)
    subprocess.call(args)
