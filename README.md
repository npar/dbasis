# DBasisHDA #
### Short Description ###
Algorithm produces implicational equivalent of a closure system defined on the columns of a binary table. Input is given by a table of size nxm and a string of zeros and ones of length mn, where n is interpreted as the number of rows and m as the number columns of input table. Output is the set of formulas uvz-> b, where u,v,..,z and b are markings of columns, such that in every row of the given table,  ones in all columns u,v,..,z imply one in column b. Particular set of implications obtained in this algorithm is the D-basis of a closure system, defined in References [1]. Algorithm behind the code is described in [2]. Algorithm employs hypergraph dualization code implementation described in [3]. Actual set of implications retrieved by the algorithm may exceed the D-basis, as an example in [2] indicates, so one needs to run with option *reduce mode* in order to obtain exact D-basis. New function available with options *combination group* aims to compute the association rules of high confidence by removing particular rows of the table [5]. The option *mins* allows to filter implications with the given threshold of support.

### References ###

[1] Adaricheva K., Nation J.B. and Rand R., Ordered direct implicational basis of a finite closure system, Discrete Applied Mathematics, 161 (2013), pp. 707-723.

[2] Adaricheva K. and Nation J.B., Discovery of the D-basis in binary tables based on hypergraph dualization, Theoretical Computer Science, v.658 (2017), Part B, 307-315.

[3] Murakami K. and Uno T., Efficient algorithms for dualizing large scale hypergraphs, Disc. Appl. Math. 170 (2014), 83--94.

[4] Fredman M. and Khachiyan L., On the complexity of dualization of monotone disjunctive normal forms, J. Algorithms 21 (1996), 618--628.

[5] Segal, O., Cabot-Miller, J., Adaricheva, K., Nation, J. B., & Sharafudinov, A. (2018). The Bases of Association Rules of High Confidence. arXiv preprint arXiv:1808.01703.

### Details/complexity of the algorithm ###

Algorithm starts from table reduction that does not alter concept lattice corresponding to original table. Then the D-relation is computed for the columns of the reduced table. Based on the D-relation, a hypergraph is produced for each specified column. This hypergraph is dualized in SHD module written by Murakami-Uno. The output of dualized hypergraph provides the antecedents of the implications with the specified column in conclusion. Algorithm is repeated for each column of the table, and combined outputs give the total D-basis. Only a sector of the D-basis for the specified column may be produced. The complexity of the part of the algorithm leading to formation of a column specific hypergraph is quadratic in the size of the table. Hypergraph dualization is of at most sub-exponential complexity in the size of the hypergraph and its dual, based on result in References [4].
 Code details/authors
Initial implementation is done in C++ by undergraduates J. Blumenkopf and T. Moldwin (Yeshiva College, New York) in 2013, with assistance of T. Uno. SHD module is written in C by T. Uno and K. Murakami. Further development and debugging in 2013-16 is done by Dr. M. Sterling, A. Sailanbayev and A. Sharafudinov from Nazarbayev University (Kazakhstan). Github was maintained by T. Moldwin and A. Amanbekkyzy. Since 2017 the development and testing is done by Dr. O. Segal and J. Cabot-Miller. 
Since Nov 2018 dbasis supports parallel minimal hitting set generation based on code from the [minimal hitting set generation library](https://github.com/VeraLiconaResearchGroup/Minimal-Hitting-Set-Algorithms) by Vera-Licona Research Group. 

### External project dependencies

[OpenMPI](https://www.open-mpi.org/)

[C++ MHS generation algorithms](https://github.com/VeraLiconaResearchGroup/Minimal-Hitting-Set-Algorithms)

[Boost C++ Libraries](https://www.boost.org/LICENSE_1_0.txt)

Setting up build/test environment using docker containers
----------------
The easiest way to get started is to use a prebuilt environment inside a docker container.


To get started this way:

1. Make sure you have [docker](https://docs.docker.com/get-docker/) installed on your machine 

2. Run the following commands to build and run the container:

    ```
    cd [project root]
    make docker
    docker run -it --rm npar/dbasis
    ```
3. Use the Dbasis software inside the container*:

	```
    cd dbasis
    # run using a single processor (non-parallel mode)
    Release/DBasisHDA -mins 1 -i data/golden-tests/Large_set_10x22.txt
    # run using mpi on multiple processes/machines (parallel mode)
    mpirun --allow-run-as-root -np 8 Release/DBasisHDA -i data/golden-tests/Large_set_10x22.txt -mins 0 -cg all -cgs 2 -cgc 40 -agg 1 -c 4 -mp
	```
	
* The mpirun parameter '--allow-run-as-root' is needed when runing mpi processes as root user. Root is the default docker user.
  For more info see [OpenMPI docs](https://www.open-mpi.org/doc/v3.1/man1/mpirun.1.php) and [docker docs](https://docs.docker.com/)


Setting up build/test environment from scratch
----------------
### Boost C++ Libraries 
[C++ MHS generation algorithms](https://github.com/VeraLiconaResearchGroup/Minimal-Hitting-Set-Algorithms) depends on several boost libraries.

1. To build/install from source on a linux based system*:

  ```
  wget https://sourceforge.net/projects/boost/files/boost/1.61.0/boost_1_61_0.tar.gz
  tar -vxf boost_1_61_0.tar.gz 
  cd boost_1_61_0/
  ./bootstrap.sh --prefix=/usr/local --show-libraries
  ```

* Alternatively, use a precompiled boost package. On a Debian based system, for example, you can install the libraries by running the following command:

  ```
  sudo apt-get install libboost-program-options-dev libboost-log-develop
  ```

### OpenMPI 

The application uses [OpenMPI](https://www.open-mpi.org/) for parallel distribution of workloads across multiple processes on a single machine and/or processes on multiple machines.

The current code version has been developed and tested using OpenMPI version 3.1.

To build/install OpenMPI from source on a linux based system:

```
wget https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-3.1.0.tar.gz 
tar -xvf openmpi-3.1.0.tar.gz
cd openmpi-3.1.0 
./configure --prefix=/usr/local
make -j $(nproc) && make install

```

Build instructions
------------------

### Setup build environment variables
```
source  [project root]/scripts/env/openMpiEnv
```

### Compile the code

Run make inside the project root folder ([project root]).

Upon successful build this will produce: ./Debug/DBasisHDA or ./Release/DBasisHDA depending on debug/release mode build chosen (see notes below). 

Notes:

1. To build in debug mode use the following command:

    ``` 
    make DEBUG=1 -j $(nproc)
    ```

2. To build in release mode (optimizations on, difficult to debug, but produces faster code) use the following command:

    ```
    make DEBUG=0 -j $(nproc)
    ```
   
3. To clean object files from previous build add clean to one of the previous commands. 
   for example, to clean debug build folder:

    ```
    make DEBUG=1 clean 
    ```

Run instructions
----------------
1. cd to project root:

    ```
    cd [project root]
    ```

2. Setup run environment: 

    ``` 
    source  [project root]/scripts/env/DBasisDepEnv 
    ```

3. To run the executable use: 

    ```
    ./Debug/DBasisHDA or ./Release/DBasisHDA with the command line options specified below.
    ```

Command line options
--------------------

Usage is DBasisHDA <-i inputFile> [<-o outputFile> [<-c column>]

Options:

  -i
  
	Input file path
	this is a required argument
	
  -o
  
	Output file path
	optional argument
	
  -c
  
	A column to work on
	optional argument
	
  -ord
  
	Run in one row deletion mode
	optional argument
	
  -r
  
	Run in reduce mode
	reduce after call to shd
	
  -test
  
	Run test harness
	more info...
	
  -mins
  
	Min support to work on
	optional argument
	
  -cg
  
	Combination group. Usage is: -cg [1,2,3]
	optional argument
	
  -cgs
  
	Combination group size
	optional argument
	
  -cgc
  
	Combination group count
	optional argument
	
  --version,  -v
  
	Display version information
	
  -zb
  
	Run in zero base mode
	by default all column and row numbers are one based
	if this option is specified, all input/output column numbers will be zero based
	
  -agg
  
	Determine if MRD algorithm should aggregate. If not specified, by default the value is true.
	Setting aggregation to false disables aggregation. Usage is:
	Disable: -agg 0
	Enable: -agg 1
	
  -mp
  
	parallel process execution mode (multiple processes potentially across multiple computers (uses MPI))
	
  -mt
  
	parallel thread execution mode (multiple threads on a single computer)
	
  -mhsa
  
	Minimal hitting set algorithm. Usage is: -mhsa [rs,mmcs,bm] 
	optional argument
	
  -mhss
  
	Minimal hitting set algorithm cut off size. Can be used with the rs and mmcs algorithms.
	Usage is: -mhsa [rs,mmcs] -mhss [1..N] 
	optional argument
  
  -vl
  
	Verbosity level of output displayed by program.
	Usage is: -vl [1..N]
	Valid values are: 1 => Summary (default level), 2 => Verbose

  
  
Usage Examples
----------------

Run with input file data/golden-tests/Large_set_10x22.txt and min support 1:

```
./Release/DBasisHDA -mins 1 -i data/golden-tests/Large_set_10x22.txt
```

Run with input file data/golden-tests/Large_set_10x22.txt, min support 1 and reduction enabled:

```
./Release/DBasisHDA -mins 1 -i data/golden-tests/Large_set_10x22.txt -r
```

Run with input file data/golden-tests/Large_set_10x22.txt, min support 1 and focus on coloumn 1:

```
./Release/DBasisHDA -c 1 -mins 3 -i data/golden-tests/Large_set_10x22.txt
```

Run with input file data/golden-tests/Large_set_10x22.txt, output file Large_set_10x22-1.out and min support 1:

```
./Release/DBasisHDA -mins 1 -i data/golden-tests/Large_set_10x22.txt -o Large_set_10x22-1.out
```

Parallel vs non-parallel runs:

Parallel run:

```
mpirun -np 8 Release/DBasisHDA -i data/golden-tests/Large_set_10x22.txt -mins 0 -cg all -cgs 2 -cgc 40 -agg 1 -c 4 -mp -o out-par.txt
```

Non parallel run:

```
Release/DBasisHDA -i data/golden-tests/Large_set_10x22.txt -mins 0 -cg all -cgs 2 -cgc 40 -agg 1 -c 4 -o out-np.txt
```

Selecting the type of minimal hitting set algorithm:

```
Release/DBasisHDA -i data/golden-tests/Large_set_10x22.txt -c 22 -mins 4 -mhsa rs -mt -mhss 3

Release/DBasisHDA -i data/golden-tests/Large_set_10x22.txt -c 22 -mins 4 -mhsa mmcs -mt -mhss 3

Release/DBasisHDA -i data/golden-tests/Large_set_10x22.txt -mins 5 -cg all -cgs 3 -cgc 1 -agg 1 -c 22 -mhsa rs -mt
```






